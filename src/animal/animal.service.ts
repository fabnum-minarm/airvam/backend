import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { AnimalDto } from './dto/animal.dto';
import { AirvamLogger } from "../logger/airvam.logger";
import { InjectRepository } from "@nestjs/typeorm";
import { In, Repository, SelectQueryBuilder } from "typeorm";
import { Animal } from "./entities/animal.entity";
import { plainToInstance } from "class-transformer";
import { RankDto } from "../rank/dto/rank.dto";
import { RankService } from "../rank/rank.service";
import { PaxService } from "../pax/pax.service";
import { Utils } from "../core/utils";
import * as fs from "fs";
import { LegService } from "../leg/leg.service";
import { LegStatus } from "../leg/enums/leg-status.enum";
import camelcaseKeys = require("camelcase-keys");
import { Worksheet, Workbook } from "exceljs";

const ExcelJS = require('exceljs');
const uuid = require('uuid');

@Injectable()
export class AnimalService {
  private readonly _logger = new AirvamLogger('AnimalService');

  constructor(@InjectRepository(Animal)
              private readonly _animalRepository: Repository<Animal>,
              private _rankService: RankService,
              @Inject(forwardRef(() => LegService))
              private _legService: LegService,
              @Inject(forwardRef(() => PaxService))
              private _paxService: PaxService) {
  }

  /**
   * Récupération des animaux d'un vol
   * @param flightId
   */
  findByFlight(flightId: number): Promise<Animal[]> {
    return this._getAnimalQueryBuilder()
      .where({flight: {id: flightId}})
      .getMany();
  }

  /**
   * Suppression des animaux d'un vol
   * @param flightId
   */
  async deleteByFlight(flightId: number) {
    await this._animalRepository.delete({flight: {id: flightId}});
  }

  /**
   * Parsing d'un fichier excel d'animaux
   * @param flightId
   * @param file
   */
  async parse(flightId: number, file): Promise<AnimalDto[]> {
    let workbook = new ExcelJS.Workbook();
    let animalList: AnimalDto[];
    try {
      await workbook.xlsx.load(file.buffer);
      animalList = this._convertExcelToJson(workbook.worksheets[0]);
    } catch (error) {
      this._logger.error('ERREUR PARSING FICHIER ANIMAL', error);
      throw new HttpException('Le fichier fournit ne peut pas être lu.', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    animalList = await this._addDependencies(flightId, animalList);

    return animalList;
  }

  /**
   * Mise à jour de la liste d'animaux d'un vol
   * Ajout, édition et suppression
   * @param flightId
   * @param animals
   * @param to_delete
   */
  async updateList(flightId: number, animals: Animal[], to_delete: number[]): Promise<Animal[]> {
    const paxs = await this._paxService.findByFlight(flightId);
    animals = animals.map(a => {
      // @ts-ignore
      a.flightId = {id: flightId};
      a.pax = this._affectPax(plainToInstance(AnimalDto, camelcaseKeys(a)), paxs);
      a.lmc = true;
      return a;
    });
    animals = animals.filter(a => !!a.pax);
    const legsToUpdate = [];
    animals.forEach(a => {
      a.pax.legs.forEach(l => {
        if (legsToUpdate.indexOf(l.id) < 0) {
          legsToUpdate.push(l.id);
        }
      });
    });

    const promises = [
      this._animalRepository.save(animals),
      this._animalRepository.delete({id: In(to_delete)})
    ];
    legsToUpdate.forEach(legId => {
      promises.push(this._legService.updateStatus(legId, LegStatus.lmc));
    });
    // @ts-ignore
    await Promise.all(promises);

    return await this._getAnimalQueryBuilder()
      .where({flight: {id: flightId}})
      .getMany();
  }

  /**
   * Export de la liste des animaux d'un vol au format XLSX
   * @param flightId
   */
  exportXls(flightId: number): Promise<fs.ReadStream> {
    return new Promise<fs.ReadStream>(async (resolve, reject) => {
      const workbook = await this._generateWorkbook(flightId);

      const guidForClient = uuid.v1();
      let pathNameWithGuid = `${guidForClient}_result.xlsx`;
      await workbook.xlsx.writeFile(pathNameWithGuid);
      let stream = fs.createReadStream(pathNameWithGuid);
      stream.on("close", () => {
        fs.unlink(pathNameWithGuid, (error) => {
          if (error) {
            throw error;
          }
        });
      });
      resolve(stream);
      return;
    });
  }

  /**
   * Reset du flag LMC pour les animaux d'un leg
   * @param legId
   */
  async resetlmcByLeg(legId: number) {
    const animals = await this._animalRepository.find({where: {pax: {legs: {id: legId}}}});
    if (animals && animals.length > 0) {
      return this._animalRepository.update(animals.map(a => a.id), {lmc: false});
    }
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Converti une feuille excel en entrée vers un tableau d'objets Animal
   * @param worksheet
   */
  private _convertExcelToJson(worksheet: Worksheet): AnimalDto[] {
    const headers = {
      'Name': 'animalName',
      'Matricule': 'animalId',
      'Unit': 'animalUnit',
      'Name_1': 'ownerName',
      'Rank': 'ownerRank',
      'Unit_1': 'ownerUnit',
      'Remarks': 'ownerObservations',
    };
    const animals = [];
    let firstRow = worksheet.getRow(2);
    if (!firstRow.cellCount) return;
    let keys = [];
    for (let i = 1; i <= +firstRow.values.length; i++) {
      if (keys.findIndex(k => k === firstRow.getCell(i).text) >= 0) {
        keys.push(`${firstRow.getCell(i).text}_1`);
      } else {
        keys.push(firstRow.getCell(i).text);
      }
    }

    worksheet.eachRow((row, rowNumber) => {
      if (rowNumber <= 2) return;
      let values = row.values
      let t: any = {};
      for (let i = 0; i < keys.length; i++) {
        const header = Object.keys(headers).find(h =>
          h?.replace(/\W/g, '')?.toLowerCase().trim() === keys[i]?.replace(/\W/g, '')?.toLowerCase().trim()
        );
        if (!!header) {
          t[headers[header]] = values[i + 1];
        }
      }
      animals.push(t);
    });
    return animals;
  }

  /**
   * Rajoute les différents sous-objets à la liste des animmaux
   * @param flightId
   * @param animalList
   * @private
   */
  private async _addDependencies(flightId: number, animalList: AnimalDto[]): Promise<AnimalDto[]> {
    const paxs = await this._paxService.findByFlight(flightId);
    animalList = await Promise.all(animalList.map(async (a) => {
      a.ownerRank = plainToInstance(RankDto, camelcaseKeys(await this._rankService.findOneWithClause({name: a.ownerRank}), {deep: true}));
      a.pax = this._affectPax(a, paxs);
      return a;
    }));
    return animalList;
  }

  /**
   * Affecte un pax à l'animal
   * @param animal
   * @param paxs
   * @private
   */
  private _affectPax(animal: AnimalDto, paxs) {
    return paxs.find(p => {
        const ownerName = Utils.removeDiacritics(animal.ownerName)?.trim().toLowerCase();
        const paxLastName = Utils.removeDiacritics(p.last_name)?.trim().toLowerCase();
        const ownerUnit = Utils.removeDiacritics(animal.ownerUnit)?.trim().toLowerCase();
        const paxUnit = Utils.removeDiacritics(p.unit)?.trim().toLowerCase();
        return ownerName?.includes(paxLastName)
          && ownerUnit === paxUnit
          && animal.ownerRank?.id === p.rank?.id
      }
    );
  }

  /**
   * Retourne l'objet Query Builder générique pour la récupération d'un animal
   * @private
   */
  private _getAnimalQueryBuilder(): SelectQueryBuilder<Animal> {
    return this._animalRepository.createQueryBuilder('animal')
      .select(['animal.id', 'animal.animal_name', 'animal.animal_id', 'animal.animal_unit',
        'animal.owner_name', 'animal.owner_unit', 'animal.owner_observations', 'pax.id'])
      .leftJoinAndSelect('animal.owner_rank', 'owner_rank')
      .leftJoin('animal.pax', 'pax')
  }

  /**
   * Génération d'un fichier excel
   * @param flightId
   * @private
   */
  private async _generateWorkbook(flightId: number): Promise<Workbook> {
    const workbook = new ExcelJS.Workbook();
    const sheet = workbook.addWorksheet('Animaux');
    sheet.addRows(await this._generateWorksheet(flightId));
    this._setPropertiesOnXls(sheet);
    return workbook;
  }

  /**
   * Génération d'une feuille excel avec les données des animaux
   * @param flightId
   * @private
   */
  private async _generateWorksheet(flightId: number) {
    const animals = await this.findByFlight(flightId);
    const rows = [['Animal', '', '', 'Owner'], ['Name *', 'Matricule *', 'Unit *', 'Name *', 'Rank *', 'Unit *', 'Remarks']];
    animals.forEach((animal: Animal) => {
      rows.push(this._generateRow(animal));
    });
    return rows;
  }

  /**
   * Génération d'une ligne pour une feuille excel
   * @param animal
   * @private
   */
  private _generateRow(animal: Animal) {
    return [
      animal.animal_name,
      animal.animal_id,
      animal.animal_unit,
      animal.owner_name,
      animal.owner_rank ? animal.owner_rank.name : '',
      animal.owner_unit,
      animal.owner_observations ? animal.owner_observations : ''
    ]
  }

  /**
   * Edition des propriétés de la feuille excel
   * @param worksheet
   * @private
   */
  private _setPropertiesOnXls(worksheet: Worksheet) {
    worksheet.mergeCells('A1:C1');
    worksheet.mergeCells('D1:G1');
    for (let i = 1; i < 8; i++) {
      worksheet.getColumn(i).width = 18;
    }
  }

}
