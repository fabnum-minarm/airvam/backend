import { Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Flight } from "../../flight/entities/flight.entity";
import { Rank } from "../../rank/entities/rank.entity";
import { Pax } from "../../pax/entities/pax.entity";

@Entity('animal')
export class Animal {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('animal_flightId_index')
  @ManyToOne(type => Flight, flight => flight.animals, {onDelete: 'CASCADE'})
  flight: Flight;

  @Column({nullable: false, length: 255})
  animal_name: string;

  @Column({nullable: false, length: 100})
  animal_id: string;

  @Column({nullable: false, length: 100})
  animal_unit: string;

  @Column({nullable: false, length: 255})
  owner_name: string;

  @Column({nullable: false, length: 100})
  owner_unit: string;

  @ManyToOne(type => Rank, rank => rank.animals)
  owner_rank: Rank;

  @Column({nullable: true, length: 255})
  owner_observations: string;

  @Column({default: false})
  lmc: boolean;

  @ManyToOne(type => Pax, pax => pax.animals, {onDelete: 'CASCADE'})
  @Index('animal_paxId_index')
  pax: Pax;
}
