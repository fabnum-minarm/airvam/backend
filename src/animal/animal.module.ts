import { forwardRef, Module } from '@nestjs/common';
import { AnimalService } from './animal.service';
import { AnimalController } from './animal.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Animal } from "./entities/animal.entity";
import { FlightHistoricModule } from "../flight-historic/flight-historic.module";
import { RankModule } from "../rank/rank.module";
import { PaxModule } from "../pax/pax.module";
import { LegModule } from "../leg/leg.module";
import { FlightModule } from "../flight/flight.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Animal]),
    FlightHistoricModule,
    RankModule,
    forwardRef(() => PaxModule),
    forwardRef(() => LegModule),
    forwardRef(() => FlightModule),
  ],
  controllers: [AnimalController],
  providers: [AnimalService],
  exports: [AnimalService]
})
export class AnimalModule {
}
