import {
  Controller,
  Post,
  Body,
  Param,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Get, Res, HttpException, HttpStatus
} from '@nestjs/common';
import { AnimalService } from './animal.service';
import { AuthGuard } from "@nestjs/passport";
import { FileInterceptor } from "@nestjs/platform-express";
import { ApiBody, ApiConsumes, ApiOperation } from "@nestjs/swagger";
import { FileUploadDto } from "../core/dto/file-upload.dto";
import { plainToInstance } from "class-transformer";
import camelcaseKeys = require("camelcase-keys");
import { AnimalDto } from "./dto/animal.dto";
import { Utils } from "../core/utils";
import { Animal } from "./entities/animal.entity";
import { FlightHistoricService } from "../flight-historic/flight-historic.service";
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import { AirvamLogger } from "../logger/airvam.logger";
import { FlightGuard } from "../core/guards/flight.guard";
import { FlightArchived } from "../core/decorators/flight-archived.decorator";
const snakecaseKeys = require('snakecase-keys');

@Controller('animal')
@UseGuards(AuthGuard('jwt'))
export class AnimalController {
  private readonly _logger = new AirvamLogger('AnimalController');

  constructor(private readonly _animalService: AnimalService,
              private readonly _flightHistoricService: FlightHistoricService) {
  }

  @Get(':flightId')
  @ApiOperation({summary: 'Retourne tout les animaux d\'un vol'})
  @UseGuards(FlightGuard)
  async findByFlight(@Param('flightId') flightId: string): Promise<Animal[]> {
    const animals: Animal[] = await this._animalService.findByFlight(+flightId);
    return plainToInstance(Animal, camelcaseKeys(animals, {deep: true}));
  }

  @Post('parse/:flightId')
  @UseInterceptors(
    FileInterceptor(
      'file',
      {
        fileFilter: Utils.excelFileFilter,
        limits: {
          fileSize: 1e+7
        },
      }
    )
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Liste animaux (excel)',
    type: FileUploadDto,
  })
  @ApiOperation({summary: 'Parsing du fichier excel'})
  parseFile(@Param('flightId') flightId: string,
            @UploadedFile() file): Promise<AnimalDto[]> {
    return this._animalService.parse(+flightId, file);
  }

  @Post('/updateList/:flightId')
  @UseGuards(FlightGuard)
  @FlightArchived(false)
  @ApiOperation({summary: "Mise à jour d'animaux (ajout / édition / suppression)"})
  async updateList(@Param('flightId') flightId: string,
                   @Body() payload: { animals: AnimalDto[], to_delete: number[] },
                   @AuthUser() user: UserDto) {
    // @ts-ignore
    const animalListReturned = await this._animalService.updateList(+flightId, plainToInstance(Animal, snakecaseKeys(payload.animals)), payload.to_delete);
    await this._flightHistoricService.insertByFlightId(+flightId, user.email, `Mise à jour de la liste animaux`);
    return plainToInstance(AnimalDto, camelcaseKeys(animalListReturned, {deep: true}));
  }

  @Get('export/:flightId')
  @ApiOperation({summary: 'Export XLS des animaux'})
  @UseGuards(FlightGuard)
  async exportFile(@Param('flightId') flightId: string,
                   @Res() res): Promise<any> {
    try {
      const streamFile = await this._animalService.exportXls(+flightId);
      res.setHeader("Content-disposition", `attachment;`);
      res.contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      streamFile.pipe(res);
    } catch (err) {
      this._logger.error('', err);
      throw new HttpException(`Une erreur est survenue durant l'export de la liste animaux`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
