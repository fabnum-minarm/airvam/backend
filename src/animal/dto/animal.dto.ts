import { CreateAnimalDto } from './create-animal.dto';
import { IsNotEmpty, IsObject, IsOptional } from "class-validator";
import { PaxDto } from "../../pax/dto/pax.dto";

export class AnimalDto extends CreateAnimalDto {
  @IsOptional()
  id: number;

  @IsObject()
  @IsNotEmpty()
  pax: PaxDto;
}
