import { IsNotEmpty, IsObject, IsOptional, IsString } from "class-validator";
import { FlightDto } from "../../flight/dto/flight.dto";
import { RankDto } from "../../rank/dto/rank.dto";

export class CreateAnimalDto {
  @IsObject()
  @IsOptional()
  flight: FlightDto;

  @IsString()
  @IsNotEmpty()
  animalName: string;

  @IsString()
  @IsNotEmpty()
  animalId: string;

  @IsString()
  @IsNotEmpty()
  animalUnit: string;

  @IsString()
  @IsNotEmpty()
  ownerName: string;

  @IsString()
  @IsNotEmpty()
  ownerUnit: string;

  @IsObject()
  @IsNotEmpty()
  ownerRank: RankDto;

  @IsString()
  @IsOptional()
  ownerObservations: string;
}
