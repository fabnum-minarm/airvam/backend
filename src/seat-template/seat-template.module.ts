import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { SeatTemplateEntity } from "./entities/seat-template.entity";

@Module({
  imports: [TypeOrmModule.forFeature([SeatTemplateEntity])],
  controllers: [],
  providers: [],
  exports: []
})
export class SeatTemplateModule {}
