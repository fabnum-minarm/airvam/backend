import { SeatZone } from "./seat-zone.enum";
import { IsEnum, IsNotEmpty, IsOptional } from "class-validator";

export class SeatTemplateDto {
  @IsOptional()
  id: number;

  @IsEnum(SeatZone)
  @IsNotEmpty()
  zone: SeatZone;

  @IsNotEmpty()
  row: string;

  @IsNotEmpty()
  number: number;
}
