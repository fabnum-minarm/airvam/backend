import { Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn, } from "typeorm";
import { SeatZone } from "../dto/seat-zone.enum";
import { Aircraft } from "../../aircraft/entities/aircraft.entity";

@Entity('seat_template')
export class SeatTemplateEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false})
  zone: SeatZone;

  @Column({nullable: false, length: 2})
  row: string;

  @Column({nullable: false})
  number: number;

  @Column({default: false})
  blocked: boolean;

  @ManyToOne(type => Aircraft, aircraft => aircraft.seats_template)
  @Index('seatTemplate_aircraftId_index')
  aircraft: Aircraft;
}
