import { Injectable } from '@nestjs/common';
import { CreateDocumentDto } from './dto/create-document.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Like, Repository, UpdateResult } from "typeorm";
import { Document } from "./entities/document.entity";

@Injectable()
export class DocumentService {

  constructor(@InjectRepository(Document)
              private readonly _documentsRepository: Repository<Document>) {
  }

  /**
   * Création et sauvegarde d'un document de référence
   * @param createDocumentDto
   * @param file
   */
  async create(createDocumentDto: CreateDocumentDto, file): Promise<Document> {
    const documentToSave = {
      name: file.originalname.trim(),
      category: createDocumentDto.category,
      data: file.buffer
    }
    return this._documentsRepository.save(documentToSave);
  }

  /**
   * Mise à jour d'un document de référence
   * @param id
   * @param document
   */
  async update(id: number, document: Document): Promise<UpdateResult> {
    return this._documentsRepository.update({id: id}, document);
  }

  /**
   * Listing de tous les documents de référence
   */
  findAll(): Promise<Document[]> {
    return this._documentsRepository.find({
      select: ['id', 'name', 'category']
    });
  }

  /**
   * Récupération d'un document de référence
   * @param documentId
   */
  findOne(documentId: number): Promise<Document> {
    return this._documentsRepository.findOneBy({id: documentId});
  }

  /**
   * Suppression d'un document de référence
   * @param id
   */
  async delete(id: number): Promise<DeleteResult> {
    return this._documentsRepository.delete(id);
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._documentsRepository.delete({name: Like('CYTEST%')});
  }
}
