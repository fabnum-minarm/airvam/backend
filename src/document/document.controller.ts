import {
  Controller,
  Get,
  Post,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Body,
  Param,
  Delete,
  StreamableFile,
  Patch,
  HttpException, HttpStatus
} from '@nestjs/common';
import { DocumentService } from './document.service';
import { ApiBody, ApiConsumes, ApiOperation } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";
import { RolesGuard } from "../core/guards/role.guard";
import { Roles } from "../core/decorators/role.decorator";
import { UserRole } from "../user/entities/user.entity";
import { plainToInstance } from "class-transformer";
import { DocumentDto } from "./dto/document.dto";
import camelcaseKeys = require("camelcase-keys");
import { Document } from "./entities/document.entity";
import { FileInterceptor } from "@nestjs/platform-express";
import { FileUploadDto } from "../core/dto/file-upload.dto";
import { CreateDocumentDto } from "./dto/create-document.dto";
import { Readable } from "stream";
import { Dev } from "../core/decorators/dev.decorator";
import { DeleteResult } from "typeorm";
const snakecaseKeys = require('snakecase-keys');

@Controller('document')
export class DocumentController {
  constructor(private readonly _documentService: DocumentService) {
  }

  @Get()
  @ApiOperation({summary: 'Retourne tout les documents'})
  @UseGuards(AuthGuard('jwt'))
  async findAll(): Promise<DocumentDto[]> {
    const documents: Document[] = await this._documentService.findAll();
    return plainToInstance(DocumentDto, camelcaseKeys(documents, {deep: true}));
  }

  @Get(':documentId')
  @ApiOperation({summary: 'Téléchargement d\' un document'})
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Param('documentId') documentId: string): Promise<StreamableFile> {
    const file = await this._documentService.findOne(+documentId);
    if (file) {
      const stream = Readable.from(file.data);
      return new StreamableFile(stream);
    } else {
      throw new HttpException('Ce document n\'existe pas.', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post()
  @UseInterceptors(
    FileInterceptor(
      'file',
      {
        limits: {
          // 5Mb
          fileSize: 5e+6
        },
      }
    )
  )
  @ApiBody({
    description: 'Nouveau document',
    type: FileUploadDto,
  })
  @ApiOperation({summary: 'Créer un document'})
  @ApiConsumes('multipart/form-data')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async create(@UploadedFile() file: Express.Multer.File,
               @Body() createDocument: CreateDocumentDto): Promise<DocumentDto> {
    const documentToReturn: Document = await this._documentService.create(createDocument, file);
    console.log('DOCUMENT SAVED', documentToReturn);
    return plainToInstance(DocumentDto, camelcaseKeys(documentToReturn));
  }

  @Patch(':id')
  @ApiOperation({summary: 'Edition d\'un document'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async update(@Param('id') id: string, @Body() editDocument: CreateDocumentDto): Promise<CreateDocumentDto> {
    await this._documentService.update(+id, plainToInstance(Document, snakecaseKeys(editDocument)));
    const documentToReturn: Document = await this._documentService.findOne(+id);
    return plainToInstance(DocumentDto, camelcaseKeys(documentToReturn));
  }

  @Delete(':id')
  @ApiOperation({summary: 'Supprimer un document'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async deleteMedia(@Param('id') mediaId: number): Promise<void> {
    await this._documentService.delete(mediaId);
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._documentService.removeTestData();
  }
}
