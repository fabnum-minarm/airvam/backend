import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('document')
export class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false, length: 255})
  name: string;

  @Column({nullable: true, length: 255})
  category: string;

  @Column({
    type: 'bytea',
  })
  data: Uint8Array;
}
