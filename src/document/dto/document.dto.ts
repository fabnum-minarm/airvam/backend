import { CreateDocumentDto } from './create-document.dto';
import { IsNotEmpty } from "class-validator";

export class DocumentDto extends CreateDocumentDto {
  @IsNotEmpty()
  id: number;
}
