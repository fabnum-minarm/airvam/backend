import {IsOptional, IsString, MaxLength } from "class-validator";

export class CreateDocumentDto {
  @IsString()
  @MaxLength(255)
  @IsOptional()
  category: string;

  @IsString()
  @MaxLength(255)
  @IsOptional()
  name: string;
}
