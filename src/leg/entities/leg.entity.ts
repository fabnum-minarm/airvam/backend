import { Column, Entity, Index, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Airport } from "../../airport/entities/airport.entity";
import { Flight } from "../../flight/entities/flight.entity";
import { LegStatus } from "../enums/leg-status.enum";
import { Seat } from "../../seat/entities/seat.entity";
import { Pax } from "../../pax/entities/pax.entity";
import { LegDocument } from "../../leg-document/entities/leg-document.entity";
import { PaxManifest } from "../../pax-manifest/entities/pax-manifest.entity";

@Entity('leg')
export class Leg {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(type => Pax, pax => pax.legs, {onDelete: 'CASCADE'})
  paxs: Pax[];

  @OneToMany(type => PaxManifest, pax_lmc => pax_lmc.leg)
  pax_manifest: PaxManifest[];

  @Column({nullable: false, default: LegStatus.to_schedule})
  status: LegStatus;

  @Index('leg_departureId_index')
  @ManyToOne(type => Airport, airport => airport.departures)
  departure: Airport;

  @Index('leg_arrivalId_index')
  @ManyToOne(type => Airport, airport => airport.arrivals)
  arrival: Airport;

  @Column({nullable: false, type: 'timestamp'})
  departure_time: Date;

  @Index('leg_flightId_index')
  @ManyToOne(type => Flight, flight => flight.legs, {onDelete: 'CASCADE'})
  flight: Flight;

  @OneToMany(type => Seat, seat => seat.leg)
  seats: Seat[];

  @OneToOne(type => LegDocument, document => document.leg)
  documents: LegDocument;
}
