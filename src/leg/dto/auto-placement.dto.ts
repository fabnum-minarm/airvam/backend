import { IsOptional } from "class-validator";
import { AutoPlacementModel } from "../enums/auto-placement-model.enum";

export class AutoPlacementDto {
  @IsOptional()
  OA: number;

  @IsOptional()
  OB: number;

  @IsOptional()
  OC: number;

  @IsOptional()
  OD: number;

  @IsOptional()
  autoPlacementModel: AutoPlacementModel;
}
