import { IsDate, IsNotEmpty, IsObject } from "class-validator";
import { Airport } from "../../airport/entities/airport.entity";

export class CreateLegDto {

  @IsObject()
  @IsNotEmpty()
  departure: Airport;

  @IsObject()
  @IsNotEmpty()
  arrival: Airport;

  @IsDate()
  @IsNotEmpty()
  departureTime: Date;
}
