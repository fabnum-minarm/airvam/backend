import { IsArray, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { Pax } from "../../pax/entities/pax.entity";
import { Animal } from "../../animal/entities/animal.entity";
import { Seat } from "../../seat/entities/seat.entity";
import { CreateLegDto } from "./create-leg.dto";

export class LegDto extends CreateLegDto {
  @IsOptional()
  id: number;

  @IsString()
  @IsNotEmpty()
  legStatus: string;

  @IsArray()
  @IsOptional()
  paxs: Pax[];

  @IsArray()
  @IsOptional()
  animals: Animal[];

  @IsArray()
  @IsOptional()
  seats: Seat[];

  @IsOptional()
  archived: boolean;
}
