import { IsNotEmpty } from "class-validator";
import { Unit } from "../../unit/entities/unit.entity";

export class FinalizeDto {
  @IsNotEmpty()
  unit: Unit;

  @IsNotEmpty()
  printColor: boolean;

  @IsNotEmpty()
  onlyLmc: boolean;
}
