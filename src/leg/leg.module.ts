import { forwardRef, Module } from '@nestjs/common';
import { LegService } from './leg.service';
import { LegController } from './leg.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Leg } from "./entities/leg.entity";
import { SeatModule } from "../seat/seat.module";
import { FlightHistoricModule } from "../flight-historic/flight-historic.module";
import { PaxModule } from "../pax/pax.module";
import { LegDocumentModule } from "../leg-document/leg-document.module";
import { UnitModule } from "../unit/unit.module";
import { FlightModule } from "../flight/flight.module";
import { PaxManifestModule } from "../pax-manifest/pax-manifest.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Leg]),
    forwardRef(() => FlightHistoricModule),
    forwardRef(() => SeatModule),
    forwardRef(() => PaxModule),
    forwardRef(() => FlightModule),
    LegDocumentModule,
    UnitModule,
    PaxManifestModule
  ],
  controllers: [LegController],
  providers: [LegService],
  exports: [LegService]
})
export class LegModule {
}
