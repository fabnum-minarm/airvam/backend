import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Leg } from "./entities/leg.entity";
import { Flight } from "../flight/entities/flight.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { In, Repository } from "typeorm";
import { AutoPlacementDto } from "./dto/auto-placement.dto";
import { Seat } from "../seat/entities/seat.entity";
import { SeatService } from "../seat/seat.service";
import { UserDto } from "../user/dto/user.dto";
import { LegStatus } from "./enums/leg-status.enum";
import { PaxService } from "../pax/pax.service";
import { LegDocumentService } from "../leg-document/leg-document.service";
import { AutoPlacementModel } from "./enums/auto-placement-model.enum";
import { FinalizeDto } from "./dto/finalize.dto";
import { UserRole } from "../user/entities/user.entity";
import { UnitService } from "../unit/unit.service";
import { Pax } from "../pax/entities/pax.entity";
import { FlightService } from "../flight/flight.service";
import { LegDocument } from "../leg-document/entities/leg-document.entity";
import { LegDocumentDocuments, LegDocumentDocumentsLmc } from "../leg-document/dto/leg-document.dto";
import { PaxManifestService } from "../pax-manifest/pax-manifest.service";

@Injectable()
export class LegService {
  private _legDocuments = LegDocumentDocuments.concat(LegDocumentDocumentsLmc);

  constructor(@InjectRepository(Leg)
              private readonly _legsRepository: Repository<Leg>,
              @Inject(forwardRef(() => SeatService))
              private _seatService: SeatService,
              @Inject(forwardRef(() => PaxService))
              private _paxService: PaxService,
              private _legDocumentService: LegDocumentService,
              private _unitService: UnitService,
              @Inject(forwardRef(() => FlightService))
              private _flightService: FlightService,
              private _paxManifestService: PaxManifestService) {
  }

  /**
   * Récupération d'un objet leg allégé
   * @param legId
   */
  findOneLight(legId: number): Promise<Leg> {
    return this._legsRepository.findOne({
      where: {id: legId},
      relations: [
        'flight',
        'departure',
        'arrival'
      ]
    })
  }

  /**
   * Récupération d'un objet leg avec toutes ses relations
   * @param legId
   */
  findOneFull(legId: number): Promise<Leg> {
    return this._legsRepository.createQueryBuilder('leg')
      .select()
      .leftJoinAndSelect('leg.departure', 'departure')
      .leftJoinAndSelect('leg.arrival', 'arrival')
      .leftJoinAndSelect('leg.flight', 'flight')
      .leftJoinAndSelect('leg.seats', 'seats')
      .leftJoinAndSelect('flight.aircraft', 'aircraft')
      .leftJoinAndSelect('flight.regulator_user', 'regulator_user')
      .leftJoinAndSelect('flight.legs', 'legs')
      .leftJoinAndSelect('legs.departure', 'departures')
      .leftJoinAndSelect('legs.arrival', 'arrivals')
      .where({id: legId})
      .getOne();
  }

  /**
   * Récupération des legs d'un vol
   * @param flightId
   */
  findByFlight(flightId: number): Promise<Leg[]> {
    return this._legsRepository.find({
      where: {flight: {id: flightId}},
      relations: ['departure', 'arrival'],
      order: {'id': 'ASC'}
    });
  }

  /**
   * Récupération d'un leg par rapport à un id de siège
   * @param seatId
   * @param withPaxs
   */
  findBySeat(seatId: number, withPaxs = false): Promise<Leg> {
    let query = this._legsRepository.createQueryBuilder('leg')
      .select()
      .leftJoinAndSelect('leg.flight', 'flight')
      .leftJoinAndSelect('leg.seats', 'seats')
      .leftJoinAndSelect('seats.pax', 'pax')
      .leftJoin('leg.seats', 'seatSelected')
      .where('seatSelected.id = :seatId', {seatId: seatId});

    if (withPaxs) {
      query = query.leftJoinAndSelect('leg.paxs', 'paxs');
    }

    return query.getOne();
  }

  /**
   * Créer ou met à jour une liste de legs
   * @param legs
   * @param flight
   */
  async createUpdate(legs: Leg[], flight: Flight): Promise<Leg[]> {
    legs.map(leg => {
      if (!leg.id) {
        delete leg.id;
      }
      leg.flight = flight;

      return leg;
    });

    const legsIds = legs.filter(l => !!l.id).map(l => l.id).flat();

    const query = this._legsRepository.createQueryBuilder('leg')
      .select()
      .where('leg.flightId = :flightId', {flightId: flight.id});
    if (legsIds && legsIds.length > 0)
      query.andWhere('leg.id not in(:...legsIds)', {legsIds});

    const legsToDelete = await query.getMany();
    await this._legsRepository.delete({id: In(legsToDelete.map((leg) => leg.id))});

    return this._legsRepository.save(this._cleanLegs(legs));
  }

  /**
   * Mise à jour du statut d'un leg
   * Le statut d'un leg dépend de plusieurs facteurs dont le fait qu'il y ait déjà des documents générés ou pas
   * @param legId
   * @param legStatus
   */
  async updateStatus(legId: number, legStatus: LegStatus) {
    if ([LegStatus.to_schedule, LegStatus.incomplete, LegStatus.finalized].includes(legStatus)) {
      return this._legsRepository.update({id: legId}, {status: legStatus});
    }

    const leg = await this._legsRepository.findOne({
      where: {id: legId},
      relations: ['documents']
    });
    switch (legStatus) {
      case LegStatus.scheduled:
        if (leg.documents) {
          legStatus = LegStatus.lmc;
        }
        break;
      // On passe en LMC que si le leg a déjà des documents générés et que le statut est à finalisé ou prêt
      case LegStatus.lmc:
        if (!leg.documents || ![LegStatus.finalized, LegStatus.incomplete, LegStatus.to_schedule].includes(leg.status)) {
          legStatus = leg.status;
        }
        break;
    }
    return this._legsRepository.update({id: legId}, {status: legStatus});
  }

  /**
   * Placement automatique des PAXs sur un leg
   * @param legId
   * @param autoPlacement
   * @param reset
   */
  async autoPlacement(legId: number, autoPlacement?: AutoPlacementDto, reset?: boolean) {
    const query = this._legsRepository.createQueryBuilder('leg')
      .select()
      .leftJoinAndSelect('leg.departure', 'departure')
      .leftJoinAndSelect('leg.arrival', 'arrival')
      .leftJoinAndSelect('leg.flight', 'flight')
      .leftJoinAndSelect('flight.aircraft', 'aircraft')
      .leftJoinAndSelect('aircraft.seats_template', 'seats_template')
      .leftJoinAndSelect('leg.paxs', 'paxs')
      .leftJoinAndSelect('paxs.rank', 'rank')
      .leftJoinAndSelect('paxs.animals', 'animals')
      .where({id: legId});

    const leg: Leg = await query.getOne();

    if (!leg.flight.aircraft && !reset) {
      throw new HttpException('Impossible d\'effectuer un placement automatique en Free Seating.', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    let seats: Seat[] = [];
    let paxsMoved: Pax[] = [];
    if (leg.flight.aircraft) {
      [seats, paxsMoved] = await this._generateSeats(leg, autoPlacement, reset);
    }
    await this._seatService.deleteByLeg(legId);
    const seatsToReturn = await this._seatService.create(seats);
    await this._paxManifestService.lmc(paxsMoved, leg);

    if (!reset) {
      await this.updateStatus(legId, LegStatus.scheduled);
    }
    return seatsToReturn;
  }

  /**
   * Récupération uniquement du statut d'un leg
   * @param legId
   */
  async getStatus(legId: number): Promise<LegStatus> {
    return (await this._legsRepository.findOneBy({id: legId})).status;
  }

  /**
   * Récupération de la liste des documents d'un leg
   * @param legId
   */
  async getDocuments(legId: number): Promise<LegDocument> {
    const query = this._legsRepository.createQueryBuilder('leg')
      .leftJoinAndSelect('leg.documents', 'legs_documents')
      .where({id: legId});

    this._legDocuments.forEach(d => {
      query.leftJoin(`legs_documents.${d}`, `${d}_leg`);
      query.addSelect([`${d}_leg.id`, `${d}_leg.filename`]);
    });

    return (await query.getOne()).documents;
  }

  /**
   * Récupération des legs et du nombre de PAXs associés
   * @param legsIds
   */
  async findLegsWithPaxs(legsIds: number []): Promise<Leg[]> {
    return this._legsRepository.createQueryBuilder('leg')
      .loadRelationCountAndMap('leg.paxNumber', 'leg.paxs', 'paxNumber')
      .where({id: In(legsIds)})
      .getMany();
  }

  /**
   * Finalisation d'un leg et génération des documents associés
   * @param id
   * @param finalizeDto
   */
  async finalize(id: number, finalizeDto: FinalizeDto): Promise<Leg> {
    const leg = await this.findOneFull(id);
    if (![LegStatus.scheduled, LegStatus.lmc].includes(leg.status)) {
      throw new HttpException(`Le leg n'est pas au status "Prêt" ou "LMC".`, HttpStatus.BAD_REQUEST);
    }
    finalizeDto.onlyLmc = leg.status === LegStatus.lmc && !!finalizeDto.onlyLmc;

    let paxs: Pax[];
    [finalizeDto.unit, paxs] = await Promise.all([
      this._unitService.findOne(finalizeDto.unit.id),
      this._paxService.findByLeg(leg.id, true)
    ]);

    await this._legDocumentService.resetDocuments(leg.flight.id, leg.id, finalizeDto.onlyLmc);
    leg.documents = await this._legDocumentService.generateDocuments(leg, paxs, finalizeDto);
    await this._legsRepository.update({id: leg.id}, {status: LegStatus.finalized});
    await this._flightService.finalize(leg.flight.id, finalizeDto);
    return leg;
  }

  /**
   * Vérification si un utilisateur a les droits de consultation / édition sur un leg
   * @param legId
   * @param authUser
   * @param flightArchived
   * @param checkLegFlight
   */
  async checkUserAccess(legId: number, authUser: UserDto, flightArchived: boolean, checkLegFlight?: boolean): Promise<boolean> {
    if (authUser && authUser.role === UserRole.god) {
      return true;
    }
    const airports = authUser ? authUser.authorizedAirports : null;
    let whereClause = '';
    if (airports && airports.length > 0) {
      whereClause = checkLegFlight ? `("legs"."departureId" IN (${airports}) OR "legs"."arrivalId" IN (${airports}))` :
        `("departureId" IN (${airports}) OR "arrivalId" IN (${airports}))`;
    } else if (authUser) {
      return false;
    }
    const query = this._legsRepository.createQueryBuilder('leg')
      .leftJoin('leg.flight', 'flight')
      .where(`"leg"."id" = ${legId}`)
      .andWhere(whereClause);
    if (flightArchived !== undefined && flightArchived !== null) {
      query.andWhere(`"flight"."archived" = ${flightArchived}`);
    }
    if (checkLegFlight) {
      query.leftJoin('flight.legs', 'legs');
    }
    return (await this._legsRepository.query(`SELECT EXISTS(${query.getQuery()}) AS "exists"`))[0]?.exists;
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Génération des sièges pour un leg, placement des PAXs dessus suivant le placement automatique choisi
   * @param leg
   * @param autoPlacement
   * @param reset
   * @private
   */
  private async _generateSeats(leg: Leg, autoPlacement?: AutoPlacementDto, reset?: boolean): Promise<[Seat[], Pax[]]> {
    const seatsTemplate = leg.flight.aircraft.seats_template;
    const paxs = leg.paxs;
    const oldSeats: Seat[] = await this._seatService.findByLeg(leg.id, true);

    // On s'assure de remettre le tableau dans le bon ordre
    switch (autoPlacement?.autoPlacementModel) {
      case AutoPlacementModel.external:
        const ponderation = {A: 1, C: 101, D: 1, E: 101, F: 101, G: 1, H: 101, J: 101, K: 1};
        seatsTemplate.sort((a, b) => (a.number + ponderation[a.row]) - (b.number + ponderation[b.row]));
        // seatsTemplate.sort((a, b) => ponderation[a.row] - ponderation[b.row]);
        break;
      case AutoPlacementModel.one_on_two:
        const ponderationBis = {A: 1, C: 0, D: 1, E: 0, F: 1, G: 0, H: 1, J: 1, K: 0};
        seatsTemplate.sort((a, b) =>
          (a.number + Math.abs(ponderationBis[a.row] - ((a.number + (Math.floor(a.number / 13) ? 1 : 0)) % 2)) * 100)
          - (b.number + Math.abs(ponderationBis[b.row] - ((b.number + (Math.floor(b.number / 13) ? 1 : 0)) % 2)) * 100));
        break;
      case AutoPlacementModel.row:
      default:
        seatsTemplate.sort((a, b) => a.row.localeCompare(b.row));
        seatsTemplate.sort((a, b) => a.number - b.number);
        break;
    }
    seatsTemplate.sort((a, b) => a.zone.localeCompare(b.zone));

    // On trie les PAX à placer par leur grade puis par leur armée
    if (!reset) {
      paxs.sort((a, b) => a.unit.localeCompare(b.unit));
      paxs.sort((a, b) => a.atr?.localeCompare(b.atr));
      paxs.sort((a, b) => a.last_name.localeCompare(b.last_name));
      paxs.sort((a, b) => a.rank?.category.localeCompare(b.rank?.category));
      paxs.sort((a, b) => a.rank?.rank - b.rank?.rank);
    }

    // @ts-ignore
    const fullSeats: Seat[] = seatsTemplate.map(s => {
      const oldSeat = oldSeats?.find(x => x.row === s.row && x.number === s.number);
      return {
        zone: s.zone,
        row: s.row,
        number: s.number,
        pax: null,
        blocked: !oldSeat || reset ? s.blocked : oldSeat.blocked,
        observations: oldSeat && !reset ? oldSeat.observations : null,
        leg: leg
      }
    });
    if (reset) {
      return [fullSeats, []];
    }

    const zones = ['OA', 'OB', 'OC', 'OD'];
    zones.forEach(zone => {
      let capacity = autoPlacement[zone];
      const seatsZone = fullSeats.filter(s => s.zone === zone);

      seatsZone.forEach(s => {
        if (capacity > 0 && paxs.length > 0 && !s.blocked) {
          s.pax = paxs.shift();
          capacity--;
        }
      })
    });

    // Si on a pas réussi à placer tout les paxs, erreur
    if (paxs.length > 0) {
      throw new HttpException('Impossible de placer tout les PAX par rapport au centrage demandé.', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    const paxsMoved = [];
    fullSeats.forEach(s => {
      if (s.pax) {
        const oldSeat = oldSeats?.find(x => x.row === s.row && x.number === s.number);
        if (oldSeat?.pax && oldSeat.pax.id !== s.pax.id) {
          paxsMoved.push(s.pax);
        }
      }
    });

    return [fullSeats, paxsMoved];
  }

  /**
   * Formatage des objets legs avant création / édition
   * @param legs
   * @private
   */
  private _cleanLegs(legs: Leg[]): Leg[] {
    legs.forEach(l => {
      delete l.seats;
      // @ts-ignore
      l.flight = {id: l.flight.id}
    });
    return legs;
  }
}
