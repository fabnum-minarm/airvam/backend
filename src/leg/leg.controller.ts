import { Body, Controller, forwardRef, Get, Inject, Param, Post, UseGuards } from '@nestjs/common';
import { LegService } from './leg.service';
import { AuthGuard } from "@nestjs/passport";
import { AutoPlacementDto } from "./dto/auto-placement.dto";
import { Seat } from "../seat/entities/seat.entity";
import { plainToInstance } from "class-transformer";
import camelcaseKeys = require("camelcase-keys");
import { SeatDto } from "../seat/dto/seat.dto";
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import { FlightHistoricService } from "../flight-historic/flight-historic.service";
import { LegStatus } from "./enums/leg-status.enum";
import { ApiOperation } from "@nestjs/swagger";
import { LegDto } from "./dto/leg.dto";
import { FinalizeDto } from "./dto/finalize.dto";
import { LegGuard } from "../core/guards/leg.guard";
import { FlightArchived } from "../core/decorators/flight-archived.decorator";
import { LegDocumentDto } from "../leg-document/dto/leg-document.dto";
import { LegFlight } from "../core/decorators/leg-flight.decorator";

@Controller('leg')
@UseGuards(AuthGuard('jwt'), LegGuard)
export class LegController {
  constructor(private readonly _legService: LegService,
              @Inject(forwardRef(() => FlightHistoricService))
              private _flightHistoricService: FlightHistoricService) {
  }

  @Get(':id/status')
  @ApiOperation({summary: 'Retourne le statut d\'un leg'})
  @LegFlight(true)
  async getStatus(@Param('id') legId: string): Promise<LegStatus> {
    return this._legService.getStatus(+legId);
  }

  @Get(':id/documents')
  @ApiOperation({summary: 'Retourne tout les documents d\'un leg'})
  @LegFlight(true)
  async getDocuments(@Param('id') legId: string): Promise<LegDocumentDto> {
    const legDocuments = await this._legService.getDocuments(+legId);
    return plainToInstance(LegDocumentDto, camelcaseKeys(legDocuments, {deep: true}));
  }

  @Post(':id/autoPlacement')
  @ApiOperation({summary: 'Effectue un placement automatique des PAXs sur un leg'})
  @LegFlight(true)
  @FlightArchived(false)
  async update(@Param('id') id: string,
               @Body() autoPlacementDto: AutoPlacementDto,
               @AuthUser() user: UserDto) {
    const seats: Seat[] = await this._legService.autoPlacement(+id, autoPlacementDto);
    await this._flightHistoricService.insertByLegId(+id, user.email, `Placement automatique`);
    return plainToInstance(SeatDto, camelcaseKeys(seats, {deep: true}));
  }

  @Post(':id/finalize')
  @ApiOperation({summary: 'Génération des documents'})
  @LegFlight(true)
  @FlightArchived(false)
  async finalize(@Param('id') id: string,
                 @Body() finalizeDto: FinalizeDto,
                 @AuthUser() user: UserDto): Promise<LegDto> {
    const leg = await this._legService.finalize(+id, finalizeDto);
    await this._flightHistoricService.insertByLegId(+id, user.email, `Finalisation du leg ${leg.id}, ${leg.departure?.oaci}, ${leg.arrival?.oaci}`);
    return plainToInstance(LegDto, camelcaseKeys(leg, {deep: true}));
  }

}
