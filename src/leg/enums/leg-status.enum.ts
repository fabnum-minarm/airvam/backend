export enum LegStatus {
  finalized = 'finalized',
  lmc = 'lmc',
  scheduled = 'scheduled',
  to_schedule = 'to_schedule',
  incomplete = 'incomplete',
  archived = 'archived'
}
