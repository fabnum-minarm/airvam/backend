import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Like, Repository, UpdateResult } from "typeorm";
import { Airport } from "./entities/airport.entity";
import { AirportDto } from "./dto/airport.dto";
import { plainToInstance } from "class-transformer";
import camelcaseKeys = require("camelcase-keys");

@Injectable()
export class AirportService {

  constructor(@InjectRepository(Airport)
              private readonly _airportRepository: Repository<Airport>) {
  }

  /**
   * Création d'un aéroport
   * @param airport
   */
  create(airport: Airport): Promise<Airport> {
    return this._airportRepository.save(airport);
  }

  /**
   * Récupération de tous les aéroports
   */
  findAll(): Promise<Airport[]> {
    return this._airportRepository.find({
      relations: ['timezone'],
      order: {
        disabled: "ASC",
        oaci: "ASC"
      }
    });
  }

  /**
   * Récupération d'un aéroport
   * @param id
   */
  findOne(id: number): Promise<Airport> {
    return this._airportRepository.findOne({
      where: {id: id},
      relations: ['timezone']
    });
  }

  /**
   * Récupération d'un aéroport selon une clause WHERE
   * @param whereClause
   */
  findOneWithClause(whereClause: any): Promise<Airport> {
    return this._airportRepository.findOne({
      where: whereClause
    });
  }

  /**
   * Mise à jour d'un aéroport
   * @param id
   * @param airport
   */
  update(id: number, airport: Airport): Promise<UpdateResult> {
    return this._airportRepository.update({id: id}, airport);
  }

  /**
   * Activation d'un aéroport si celui-ci a été désactivé
   * @param id
   */
  async enable(id: number): Promise<void> {
    await this._airportRepository.update({id: id}, {disabled: false});
  }

  /**
   * Désactivation d'un aéroport
   * @param id
   */
  async remove(id: number): Promise<void> {
    await this._airportRepository.update({id: id}, {disabled: true});
  }

  /**
   * Formatage de plusieurs objets airports
   * @param airports
   */
  formatAirports(airports: Airport[]): AirportDto[] {
    return plainToInstance(AirportDto, camelcaseKeys(airports.map(a => this.formatAirport(a)), {deep: true}));
  }

  /**
   * Formatage d'un objet aéroport avant d'être renvoyé au frontend
   * @param airport
   */
  formatAirport(airport: Airport): AirportDto {
    const airportToReturn: any = Object.assign({}, airport);
    airportToReturn.timezone = airportToReturn.timezone.location;
    return plainToInstance(AirportDto, camelcaseKeys(airportToReturn, {deep: true}));
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._airportRepository.delete({name: Like('CYTEST%')});
  }
}
