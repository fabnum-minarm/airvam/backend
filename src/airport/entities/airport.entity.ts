import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Leg } from "../../leg/entities/leg.entity";
import { Timezone } from "../../timezone/entities/timezone.entity";
import { Unit } from "../../unit/entities/unit.entity";
import { Pax } from "../../pax/entities/pax.entity";

@Entity('airport')
@Unique(["oaci"])
@Unique(["oaci", "iata"])
export class Airport {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({length: 4})
  oaci: string;

  @Column({nullable: false, length: 3})
  iata: string;

  @Column({nullable: true, length: 255})
  name: string;

  @Column({nullable: true, length: 255})
  ticket_mention: string;

  @ManyToOne(type => Timezone, timezone => timezone.airports)
  timezone: Timezone;

  @Column({default: false})
  disabled: boolean;

  @OneToMany(type => Leg, leg => leg.departure)
  departures: Leg[];

  @OneToMany(type => Pax, pax => pax.departure)
  paxDepartures: Pax[];

  @OneToMany(type => Leg, leg => leg.arrival)
  arrivals: Leg[];

  @OneToMany(type => Pax, pax => pax.arrival)
  paxArrivals: Pax[];

  @ManyToMany(type => Unit, unit => unit.airports)
  units: Unit[];
}
