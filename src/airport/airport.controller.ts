import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { AirportService } from './airport.service';
import { ApiOperation } from "@nestjs/swagger";
import { plainToInstance } from "class-transformer";
import { Airport } from "./entities/airport.entity";
import { CreateAirportDto } from "./dto/create-airport.dto";
import { AirportDto } from "./dto/airport.dto";
import { AuthGuard } from "@nestjs/passport";
import { RolesGuard } from "../core/guards/role.guard";
import { UserRole } from "../user/entities/user.entity";
import { Roles } from "../core/decorators/role.decorator";
import { Dev } from "../core/decorators/dev.decorator";
import { DeleteResult } from "typeorm";
const snakecaseKeys = require('snakecase-keys');

@Controller('airport')
export class AirportController {
  constructor(private readonly _airportService: AirportService) {}

  @Post()
  @ApiOperation({summary: 'Créer un aéroport'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async create(@Body() createAirportDto: CreateAirportDto): Promise<AirportDto> {
    const airportToReturn: Airport = await this._airportService.create(plainToInstance(Airport, snakecaseKeys(createAirportDto)));
    const airport = await this._airportService.findOne(airportToReturn.id);
    return this._airportService.formatAirport(airport);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({summary: 'Retourne tout les aéroports'})
  async findAll(): Promise<AirportDto[]> {
    const airports: Airport[] = await this._airportService.findAll();
    return this._airportService.formatAirports(airports);
  }

  @Patch(':id')
  @ApiOperation({summary: 'Edition d\'un aéroport'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async update(@Param('id') id: string, @Body() updateAirportDto: CreateAirportDto): Promise<AirportDto> {
    await this._airportService.update(+id, plainToInstance(Airport, snakecaseKeys(updateAirportDto)));
    const airport = await this._airportService.findOne(+id);
    return this._airportService.formatAirport(airport);
  }

  @Patch(':id/activate')
  @ApiOperation({summary: 'Ré-activation d\'un aéroport'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async activate(@Param('id') id: string): Promise<AirportDto> {
    await this._airportService.enable(+id);
    const airport = await this._airportService.findOne(+id);
    return this._airportService.formatAirport(airport);
  }

  @Delete(':id')
  @ApiOperation({summary: 'Désactivation d\'un aéroport'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async remove(@Param('id') id: string): Promise<AirportDto> {
    await this._airportService.remove(+id)
    const airport = await this._airportService.findOne(+id);
    return this._airportService.formatAirport(airport);
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._airportService.removeTestData();
  }
}
