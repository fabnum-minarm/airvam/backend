import { IsOptional } from "class-validator";
import { CreateAirportDto } from "./create-airport.dto";

export class AirportDto extends CreateAirportDto {
  @IsOptional()
  id: number;

  @IsOptional()
  active: boolean;
}
