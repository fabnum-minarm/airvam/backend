import { IsNotEmpty, IsOptional, IsString, Length } from "class-validator";

export class CreateAirportDto {
  @IsString()
  @IsNotEmpty()
  @Length(4, 4)
  oaci: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 3)
  iata: string;

  @IsString()
  @IsOptional()
  @Length(1, 255)
  name: string;

  @IsString()
  @IsOptional()
  @Length(0, 255)
  ticketMention: string;

  @IsString()
  @IsNotEmpty()
  timezone: string;
}
