import { IsNotEmpty, IsObject, IsOptional, IsString } from "class-validator";
import { FlightDto } from "../../flight/dto/flight.dto";

export class FlightDocumentDto {
  @IsNotEmpty()
  id: number;

  @IsObject()
  @IsNotEmpty()
  flight: FlightDto;

  @IsString()
  @IsOptional()
  cover: string;
}
