import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Flight } from "../flight/entities/flight.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FlightDocument } from "./entities/flight-document.entity";
import * as fs from "fs";
import { AirvamLogger } from "../logger/airvam.logger";
import { Pax } from "../pax/entities/pax.entity";
import { FinalizeDto } from "../leg/dto/finalize.dto";
import { FlightService } from "../flight/flight.service";
import { DatabaseFileService } from "../database-file/database-file.service";
import { DocumentUtils } from "../core/document-utils";
import { Response } from "express";
import { LegDocumentService } from "../leg-document/leg-document.service";
import { LegDocument } from "../leg-document/entities/leg-document.entity";

const hbs = require("handlebars");
const path = require("path");
const archiver = require('archiver');

@Injectable()
export class FlightDocumentService {
  private readonly _templatePath = path.resolve(`./dist/flight_documents_template`);
  private readonly _logger = new AirvamLogger('FlightDocumentService');

  public documents = ['cover'];

  constructor(@InjectRepository(FlightDocument)
              private readonly _flightDocumentsRepository: Repository<FlightDocument>,
              @Inject(forwardRef(() => FlightService))
              private _flightService: FlightService,
              @Inject(forwardRef(() => LegDocumentService))
              private _legDocumentService: LegDocumentService,
              private _fileService: DatabaseFileService) {
    hbs.registerHelper('inc', function (value, options) {
      return (parseInt(value) + 1) + (options.hash?.page_index ? options.hash.page_index : 0) * 45;
    })
  }

  /**
   * Récupération d'un document en vue de son téléchargement
   * @param documentId
   */
  async getOne(documentId: number) {
    return await this._fileService.getFile(documentId);
  }

  /**
   * Récupération d'un fichier compressé zip contenant les documents d'un vol
   * @param flightId
   * @param res
   */
  async getZip(flightId: number, res: Response) {
    const archive = archiver('zip', {
      zlib: {level: 9} // Sets the compression level.
    });

    const flight: Flight = await this._flightService.findOneUltraLight(flightId);

    //set the archive name
    res.attachment(`${flight.mission_number}.zip`);

    //this is the streaming magic
    archive.pipe(res);

    const flightDocument: FlightDocument = await this._flightDocumentsRepository.findOne({
      relations: this.documents,
      where: {flight: {id: flightId}}
    });
    const legDocuments: LegDocument[] = await this._legDocumentService.getByFlight(flightId);
    for (const doc of this.documents) {
      if (!!flightDocument[doc]) {
        archive.append(flightDocument[doc].data, {name: flightDocument[doc].filename});
      }
    }
    const legDocumentsName = this._legDocumentService.getDocumentsName();
    for (const legDocument of legDocuments) {
      const legPrefix = `${legDocument.leg.departure.oaci}_${legDocument.leg.arrival.oaci}_`
      for (const doc of legDocumentsName) {
        if (!!legDocument[doc]) {
          archive.append(legDocument[doc].data, {name: legPrefix + legDocument[doc].filename});
        }
      }
    }

    archive.finalize();
  }

  /**
   * Génération des documents lors de la finalisation de tous les legs d'un vol
   * @param flight
   * @param paxs
   * @param finalizeDto
   */
  async generateDocuments(flight: Flight, paxs: Pax[], finalizeDto: FinalizeDto): Promise<FlightDocument> {
    let flightDocument = new FlightDocument();
    const freeSeating = !flight.aircraft;
    // @ts-ignore
    flightDocument.flight = {id: flight.id};
    flightDocument = await this._flightDocumentsRepository.save(flightDocument);
    try {
      const data = {
        flight: flight,
        paxs: DocumentUtils.sortPax(paxs)
      }
      await Promise.all([
        this._generateCover(DocumentUtils.generateFileName('Couverture', 'pdf', flight), flightDocument, data, finalizeDto, freeSeating)
      ]);
    } catch (e) {
      this._logger.error('ERREUR GENERATION DES DOCUMENTS VOLS', e);
      throw new HttpException('ERREUR GENERATION DES DOCUMENTS VOLS', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return this._flightDocumentsRepository.findOneBy({id: flightDocument.id});
  }

  /**
   * Suppression des documents caducs d'un vol
   * @param flightId
   */
  async resetDocuments(flightId: number) {
    const query = this._flightDocumentsRepository.createQueryBuilder('flight_document')
      .select()
      .where({flight: {id: flightId}});
    this.documents.forEach(d => {
      query.leftJoin(`flight_document.${d}`, d);
      query.addSelect(`${d}.id`);
    });
    const flightDocument: FlightDocument = await query.getOne();
    const promises = [];
    this.documents.forEach(d => {
      if (flightDocument && flightDocument[d]) {
        promises.push(this._fileService.delete(flightDocument[d].id));
      }
    });
    promises.push(!!flightDocument ? this._flightDocumentsRepository.delete(flightDocument.id) : null);
    return Promise.all(promises);
  };

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Génération de la page de couverture du dossier
   * @param fileName
   * @param flightDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generateCover(fileName: string,
                               flightDocument: FlightDocument,
                               data: { flight: Flight, paxs: Pax[] },
                               finalizeDto: FinalizeDto,
                               freeSeating: boolean) {
    const dataPassengerFolder = DocumentUtils.getDataPassengerCover(data.flight, data.paxs, finalizeDto, freeSeating);

    const html = fs.readFileSync(`${this._templatePath}/dossier_passager.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataPassengerFolder), false);

    await this._fileService.saveFlightLegFile(flightDocument, fileName, buffer, 'flight_document_cover');
  }
}
