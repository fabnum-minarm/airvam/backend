import { forwardRef, Module } from '@nestjs/common';
import { FlightDocumentService } from './flight-document.service';
import { FlightDocumentController } from './flight-document.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { FlightDocument } from "./entities/flight-document.entity";
import { FlightModule } from "../flight/flight.module";
import { DatabaseFileModule } from "../database-file/database-file.module";
import { LegDocumentModule } from "../leg-document/leg-document.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([FlightDocument]),
    forwardRef(() => FlightModule),
    DatabaseFileModule,
    forwardRef(() => LegDocumentModule),
  ],
  controllers: [FlightDocumentController],
  providers: [FlightDocumentService],
  exports: [FlightDocumentService]
})
export class FlightDocumentModule {
}
