import { Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Flight } from "../../flight/entities/flight.entity";
import { DatabaseFile } from "../../database-file/entities/database-file.entity";

@Entity('flight_document')
export class FlightDocument {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type => Flight, flight => flight.documents, {onDelete: 'CASCADE'})
  @JoinColumn()
  flight: Flight;

  @OneToOne(type => DatabaseFile, file => file.flight_document_cover, {onDelete: 'CASCADE'})
  @JoinColumn()
  cover: DatabaseFile;
}
