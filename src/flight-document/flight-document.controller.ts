import { Controller, Get, Header, Param, Res, StreamableFile, UseGuards } from '@nestjs/common';
import { FlightDocumentService } from './flight-document.service';
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import { AuthGuard } from "@nestjs/passport";
import { FlightGuard } from "../core/guards/flight.guard";
import { Readable } from "stream";
import { Response } from "express";
import { ApiOperation } from "@nestjs/swagger";

@Controller('flight-document')
@UseGuards(AuthGuard('jwt'), FlightGuard)
export class FlightDocumentController {
  constructor(private readonly _flightDocumentService: FlightDocumentService) {
  }

  @Get(':flightId/:documentId')
  @ApiOperation({summary: 'Téléchargement d\'un document'})
  async findOne(@Param('flightId') flightId: string,
                @Param('documentId') documentId: string,
                @AuthUser() user: UserDto): Promise<StreamableFile> {
    const file = await this._flightDocumentService.getOne(+documentId);
    if (file) {
      const stream = Readable.from(file.data);
      return new StreamableFile(stream);
    }
  }

  @Get('/:flightId')
  @ApiOperation({summary: 'Téléchargement d\'un zip de tout les documents d\'un vol'})
  @Header('Content-Disposition', 'attachment; filename=something.pdf')
  async findLegDocuments(@Param('flightId') flightId: string,
                         @Res() res: Response): Promise<any> {
    return this._flightDocumentService.getZip(+flightId, res);
  }
}
