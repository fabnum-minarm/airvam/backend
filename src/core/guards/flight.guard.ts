import { CanActivate, ExecutionContext, Inject } from "@nestjs/common";
import { FlightService } from "../../flight/flight.service";
import { UserDto } from "../../user/dto/user.dto";
import { UserRole } from "../../user/entities/user.entity";
import { Reflector } from "@nestjs/core";

export class FlightGuard implements CanActivate {
  constructor(@Inject(FlightService) private _flightService: FlightService,
              private _reflector: Reflector) {
  }

  /**
   * Route uniquement accessible si l'utilisateur a les droits sur le vol
   * @param context
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const flightArchived = this._reflector.get<boolean>('flightArchived', context.getHandler());
    const request = context.switchToHttp().getRequest();
    const flightId = request.params?.flightId ? +request.params?.flightId : +request.params?.id;
    const user: UserDto = request.user;
    user ? user.authorizedAirports = user.role === UserRole.god ? null : user.unit ? user.unit?.airports.map(a => a.id) : [] : null;

    return await this._flightService.checkUserAccess(flightId, user, flightArchived);
  }
}
