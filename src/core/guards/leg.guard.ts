import { CanActivate, ExecutionContext, Inject } from "@nestjs/common";
import { UserDto } from "../../user/dto/user.dto";
import { LegService } from "../../leg/leg.service";
import { UserRole } from "../../user/entities/user.entity";
import { Reflector } from "@nestjs/core";

export class LegGuard implements CanActivate {
  constructor(@Inject(LegService) private _legService: LegService,
              private _reflector: Reflector) {
  }

  /**
   * Route uniquement accessible si l'utilisateur a les droits sur le leg
   * @param context
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const flightArchived = this._reflector.get<boolean>('flightArchived', context.getHandler());
    const request = context.switchToHttp().getRequest();
    const legId = request.params?.legId ? +request.params?.legId : +request.params?.id;
    const user: UserDto = request.user;
    user ? user.authorizedAirports = user.role === UserRole.god ? null : user.unit ? user.unit?.airports.map(a => a.id) : [] : null;

    const checkLegFlight = this._reflector.get<boolean>('checkLegFlight', context.getHandler());
    return await this._legService.checkUserAccess(legId, user, flightArchived, checkLegFlight);
  }
}
