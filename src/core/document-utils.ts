import { Pax } from "../pax/entities/pax.entity";
import { SeatZone } from "../seat-template/dto/seat-zone.enum";
import { Flight } from "../flight/entities/flight.entity";
import { FinalizeDto } from "../leg/dto/finalize.dto";
import { Leg } from "../leg/entities/leg.entity";
import { Seat } from "../seat/entities/seat.entity";
import { Utils } from "./utils";
import { AirvamLogger } from "../logger/airvam.logger";
import puppeteer from "puppeteer";

const moment = require('moment');

export class DocumentUtils {
  /**
   * Triage des PAXs suivant leur numéro de manifeste
   * @param paxs
   */
  static sortByManifestNumber(paxs: Pax[]) {
    paxs.sort((a, b) => a.pax_manifest && b.pax_manifest ? a.pax_manifest[0].manifest_number - b.pax_manifest[0].manifest_number : null);
    return paxs;
  }

  /**
   * Triage des PAXs
   * Les sorts sont effectués du moins important au plus important
   * @param paxs
   */
  static sortPax(paxs: Pax[]) {
    paxs.sort((a, b) => a.unit?.localeCompare(b.unit));
    paxs.sort((a, b) => a.atr?.localeCompare(b.atr));
    paxs.sort((a, b) => a.last_name?.localeCompare(b.last_name));
    paxs.sort((a, b) => a.rank?.category.localeCompare(b.rank?.category));
    paxs.sort((a, b) => a.rank?.rank - b.rank?.rank);
    return paxs;
  }

  /**
   * Calcul du poids total des PAXs et de leurs bagages
   * @param paxs
   */
  static computeTotalWeight(paxs: Pax[]): number {
    return paxs.reduce((accumulator: number, object: Pax) => {
      return accumulator + object.weight + object.luggage_weight;
    }, 0);
  }

  /**
   * Calcul du nombre de PAXs par zone
   * @param paxs
   * @param leg
   * @param zone
   */
  static nbrByZone(paxs: Pax[], leg: Leg, zone: SeatZone): number {
    return paxs.filter(p => {
      const seat = !!leg ? p.seats.find(s => s.leg.id === leg.id) : p.seats[0];
      return seat.zone === zone;
    }).length;
  }

  /**
   * Récupération du siège via un PAX et son leg
   * @param pax
   * @param leg
   */
  static findSeat(pax: Pax, leg: Leg): Seat {
    return pax.seats.find(s => {
      return s.leg.id === leg.id
    });
  }

  /**
   * Génération des données nécessaires pour le document manifeste PAX
   * @param flight
   * @param firstLeg
   * @param lastLeg
   * @param paxs
   * @param finalizeDto
   * @param freeSeating
   */
  static getDataPaxManifest(flight: Flight,
    firstLeg: Leg,
    lastLeg: Leg,
    paxs: Pax[],
    finalizeDto: FinalizeDto,
    freeSeating: boolean): any {
    paxs = this.sortByManifestNumber(paxs);
    if (paxs.length <= 0) {
      return null;
    }

    if (flight.imperial_system) {
      paxs = paxs.map(p => {
        p.weight = p.weight ? Math.round(p.weight * 2.205) : p.weight;
        p.luggage_weight = p.luggage_weight ? Math.round(p.luggage_weight * 2.205) : p.luggage_weight;
        return p;
      });
    }

    const dataPaxManifest = {
      unit: finalizeDto.unit.code,
      serial_number: flight.mission_name,
      flight_number: flight.mission_number,
      aircraft_type: flight.aircraft ? flight.aircraft.type : flight.aircraft_name,
      aircraft_registration: flight.aircraft ? flight.aircraft.registration : flight.aircraft_id,
      airfield_departure_oaci: firstLeg.departure.oaci,
      airfield_departure_name: firstLeg.departure.name,
      airfield_arrival_oaci: lastLeg.arrival.oaci,
      airfield_arrival_name: lastLeg.arrival.name,
      date: moment(firstLeg.departure_time).format('DD/MM/YYYY'),
      pages: [],
      weight_unit: flight.imperial_system ? 'Lb' : 'Kg',
      total_weight: DocumentUtils.computeTotalWeight(paxs),
      total_paxs: paxs.length
    };
    const paxPageLength = 40;
    paxs.forEach((p: any, idx) => {
      if (!dataPaxManifest.pages[Math.floor(idx / paxPageLength)]) {
        dataPaxManifest.pages.push({
          paxs: [],
          total_weight: null,
          total_paxs: null
        });
      }
      const currentPage = dataPaxManifest.pages[Math.floor(idx / paxPageLength)];
      p.seat = freeSeating ? { number: 'F', row: 'S' } : this.findSeat(p, firstLeg);
      p.manifest_number = p.pax_manifest ? p.pax_manifest[0].manifest_number : null;
      currentPage.paxs.push(p);
      if (idx === paxs.length - 1 || (idx !== 0 && idx % (paxPageLength - 1) === 0)) {
        dataPaxManifest.pages[Math.floor(idx / paxPageLength)].total_weight = DocumentUtils.computeTotalWeight(dataPaxManifest.pages[Math.floor(idx / paxPageLength)].paxs);
        dataPaxManifest.pages[Math.floor(idx / paxPageLength)].total_paxs = dataPaxManifest.pages[Math.floor(idx / paxPageLength)].paxs.length;
      }
    });
    let emptyLines = paxPageLength - dataPaxManifest.total_paxs % paxPageLength;
    emptyLines = emptyLines === paxPageLength ? 0 : emptyLines;
    for (let i = 0; i < emptyLines; i++) {
      dataPaxManifest.pages[dataPaxManifest.pages.length - 1].paxs.push({});
    }
    return dataPaxManifest;
  }

  /**
   * Génération des données nécessaires pour le document manifeste Animaux
   * @param flight
   * @param firstLeg
   * @param lastLeg
   * @param paxs
   * @param finalizeDto
   */
  static getDataAnimalManifest(flight: Flight,
    firstLeg: Leg,
    lastLeg: Leg,
    paxs: Pax[],
    finalizeDto: FinalizeDto): any {
    const conveyors = paxs.filter(p => p.animals && p.animals.length > 0);
    const animals = conveyors.map(p => {
      if (!!p.animals && p.animals.length > 0) {
        return p.animals;
      }
    }).flat();
    if (animals.length <= 0) {
      return null;
    }
    animals.sort((a, b) => a.animal_name?.localeCompare(b.animal_name));
    animals.sort((a, b) => a.animal_unit?.localeCompare(b.animal_unit));

    const dataAnimalManifest = {
      unit: finalizeDto.unit.code,
      serial_number: flight.mission_name,
      flight_number: flight.mission_number,
      aircraft_type: flight.aircraft ? flight.aircraft.type : flight.aircraft_name,
      aircraft_registration: flight.aircraft ? flight.aircraft.registration : flight.aircraft_id,
      airfield_departure_oaci: firstLeg.departure.oaci,
      airfield_departure_name: firstLeg.departure.name,
      airfield_arrival_oaci: lastLeg.arrival.oaci,
      airfield_arrival_name: lastLeg.arrival.name,
      date: moment(firstLeg.departure_time).format('DD/MM/YYYY'),
      pages: [],
      total_animals: animals.length,
      total_conveyors: conveyors.length
    };
    const animalPageLength = 15;
    animals.forEach((a, idx) => {
      if (!dataAnimalManifest.pages[Math.floor(idx / animalPageLength)]) {
        dataAnimalManifest.pages.push({
          animals: [],
          total_animals: null,
          total_conveyors: null
        });
      }
      const currentPage = dataAnimalManifest.pages[Math.floor(idx / animalPageLength)];
      currentPage.animals.push(a);
      if (idx === animals.length - 1 || (idx !== 0 && idx % (animalPageLength - 1) === 0)) {
        dataAnimalManifest.pages[Math.floor(idx / animalPageLength)].total_conveyors = dataAnimalManifest.pages[Math.floor(idx / animalPageLength)].animals.length;
        dataAnimalManifest.pages[Math.floor(idx / animalPageLength)].total_animals = dataAnimalManifest.pages[Math.floor(idx / animalPageLength)].animals.length;
      }
    });
    let emptyLines = animalPageLength - dataAnimalManifest.total_animals % animalPageLength;
    emptyLines = emptyLines === animalPageLength ? 0 : emptyLines;
    for (let i = 0; i < emptyLines; i++) {
      dataAnimalManifest.pages[dataAnimalManifest.pages.length - 1].animals.push({});
    }

    return dataAnimalManifest;
  }

  /**
   * Génération des données nécessaires pour le document de couverture
   * @param flight
   * @param paxs
   * @param finalizeDto
   * @param freeSeating
   */
  static getDataPassengerCover(flight: Flight, paxs: Pax[], finalizeDto: FinalizeDto, freeSeating: boolean): any {
    return {
      unit_name: finalizeDto.unit.name,
      unit_code: finalizeDto.unit.code,
      unit_address: `${finalizeDto.unit.address1 ? finalizeDto.unit.address1 + '<br/>' : ''}
${finalizeDto.unit.address2 ? finalizeDto.unit.address2 + '<br/>' : ''}
${finalizeDto.unit.address3 ? finalizeDto.unit.address3 + '<br/>' : ''}
${finalizeDto.unit.email ? finalizeDto.unit.email + '<br/>' : ''}
${finalizeDto.unit.phone_number ? finalizeDto.unit.phone_number : ''}`,
      serial_number: flight.mission_name,
      flight_number: flight.mission_number,
      cotam_number: flight.cotam_number,
      aircraft_type: flight.aircraft ? flight.aircraft.type : flight.aircraft_name,
      aircraft_registration: flight.aircraft ? flight.aircraft.registration : flight.aircraft_id,
      airfield_departure_oaci: flight.legs[0].departure.oaci,
      airfield_departure_name: flight.legs[0].departure.name,
      airfield_arrival_oaci: flight.legs[flight.legs.length - 1].arrival.oaci,
      airfield_arrival_name: flight.legs[flight.legs.length - 1].arrival.name,
      date: moment(flight.legs[0].departure_time).format('DD/MM/YYYY'),
      time: moment(flight.legs[0].departure_time).format('HH[h]mm'),
      regulator_user: `${flight.regulator_user?.last_name} ${flight.regulator_user?.first_name}`,
      legs: flight.legs,
      total_paxs: paxs.length,
      zone_oa: freeSeating ? 0 : this.nbrByZone(paxs, null, SeatZone.OA),
      zone_ob: freeSeating ? 0 : this.nbrByZone(paxs, null, SeatZone.OB),
      zone_oc: freeSeating ? 0 : this.nbrByZone(paxs, null, SeatZone.OC),
      zone_od: freeSeating ? 0 : this.nbrByZone(paxs, null, SeatZone.OD)
    };
  }

  /**
   * Formate les noms de fichiers associés aux vols et legs
   * @param fileName
   * @param extension
   * @param flight
   * @param lmc
   * @param leg
   */
  static generateFileName(fileName: string, extension: string, flight: Flight, lmc: boolean = false, leg?: Leg): string {
    let date = leg ? leg.departure_time : flight.legs[0].departure_time;
    const departureAirport = leg ? leg.departure.oaci : flight.legs[0].departure.oaci;
    const arrivalAirport = leg ? leg.arrival.oaci : flight.legs[flight.legs.length - 1].arrival.oaci;
    date = moment(date).format('DD/MM/YYYY')
    const toReturn = `${fileName}${lmc ? '_LMC' : ''}_${flight.cotam_number}_${date}_${flight.mission_number}_${departureAirport}-${arrivalAirport}.${extension}`;
    return Utils.removeDiacritics(toReturn.replace(/\s/g, ''));
  }

  /**
   * Génération du document PDF via protocol webDriverBiDi
   * @param templateName
   * @param data
   * @param a4
   */
  static async generatePDF(html: string, a4 = true): Promise<Buffer> {
    const _logger = new AirvamLogger('DocumentUtils');
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
      protocol: 'webDriverBiDi'
    });

    try {
      const page = await browser.newPage();
      await page.setContent(html, {
        waitUntil: 'domcontentloaded'
      });

      const pdfBuffer = await page.pdf({
        format: a4 ? 'A4' : 'A3',
        landscape: !a4,
        margin: {
          top: '20px',
          right: '20px',
          bottom: '20px'
        }
      });

      return Buffer.from(pdfBuffer);
    } catch (e) {
      _logger.error('ERREUR GENERATION PDF', e);
    } finally {
      await browser.close();
    }
  }
}
