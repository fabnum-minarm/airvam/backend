import format from "pg-format";
import { PaginationQueryDto } from "./dto/pagination-query.dto";

export class PaginationUtils {
  /**
   * Génération de la clause WHERE des routes de pagination
   * Prend en compte une query de recherche ainsi que les attributs sur lesquels cette query s'applique
   * @param pagination
   * @param attributes
   * @param entity
   */
  static setQuery(pagination: PaginationQueryDto,
                  attributes: string[],
                  entity?: string): string {
    let whereClause: string = '';

    let queries = pagination && pagination.query ? pagination.query.trim().split(' ') : null;
    if (!!queries && queries.length > 0) {
      queries = queries.map(q => {
        return `%${q}%`;
      });
      whereClause = '((';
      attributes.forEach((a, idx) => {
        whereClause += idx > 0 ? ') or (' : '';
        queries.forEach((q, idxQuery) => {
          whereClause += idxQuery > 0 ? ' and ' : '';
          whereClause += format(`unaccent(upper(${entity ? entity + '.' : ''}%I)) like unaccent(%L)`, a, q.toUpperCase()); 
        });
      });
      whereClause += '))';
    }

    return whereClause;
  }
}
