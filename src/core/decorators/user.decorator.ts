import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserRole } from "../../user/entities/user.entity";

export const AuthUser = createParamDecorator((data: string, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const user = request.user;
  user ? user.authorizedAirports = user.role === UserRole.god ? null : user.unit ? user.unit?.airports.map(a => a.id) : [] : null;

  return data ? user?.[data] : user;
});
