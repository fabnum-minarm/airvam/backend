import { SetMetadata } from '@nestjs/common';

export const FlightArchived = (archived: boolean) => SetMetadata('flightArchived', archived);
