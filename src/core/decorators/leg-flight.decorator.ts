import { SetMetadata } from '@nestjs/common';

export const LegFlight = (checkLegFlight: boolean) => SetMetadata('checkLegFlight', checkLegFlight);
