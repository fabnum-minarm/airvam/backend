const testFlights = [
  {
    mission_name: 'CYTEST_SINGLE_LEG_NAME',
    mission_number: 'CYTEST_SINGLE_LEG_NUMBER',
    cotam_number: 'CYTEST_SINGLE_LEG_COTAM',
    aircraft: {name: 'A330.200 ESTEREL - Plan cabine F-UJCT'},
    legs: [
      {
        departure: {oaci: 'LFMI'},
        arrival: {oaci: 'LFPC'},
        departure_time: new Date()
      }
    ]
  },
  {
    mission_name: 'CYTEST_SINGLE_LEG_OACI_NAME',
    mission_number: 'CYTEST_SINGLE_LEG_OACI_NUMBER',
    cotam_number: 'CYTEST_SINGLE_LEG_OACI_COTAME',
    aircraft: {name: 'A330.200 ESTEREL - Plan cabine F-UJCT'},
    legs: [
      {
        departure: {oaci: 'LFAT'},
        arrival: {oaci: 'CYAG'},
        departure_time: new Date(),
        arrival_time: new Date()
      }
    ]
  },
  {
    mission_name: 'CYTEST_TWO_LEGS_NAME',
    mission_number: 'CYTEST_TWO_LEGS_NUMBER',
    cotam_number: 'CYTEST_TWO_LEGS_COTAM',
    aircraft: {name: 'A330.200 ESTEREL - Plan cabine F-UJCS'},
    legs: [
      {
        departure: {oaci: 'LFMI'},
        arrival: {oaci: 'LFPC'},
        departure_time: new Date(),
        arrival_time: new Date()
      },
      {
        departure: {oaci: 'LFPC'},
        arrival: {oaci: 'LFMI'},
        departure_time: new Date(),
        arrival_time: new Date()
      }
    ]
  }
];

export {
  testFlights
};
