import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
import { ThrottlerModule } from "@nestjs/throttler";
import { FlightModule } from './flight/flight.module';
import { AuthModule } from './auth/auth.module';
import { LegModule } from './leg/leg.module';
import { AirportModule } from './airport/airport.module';
import { FlightHistoricModule } from './flight-historic/flight-historic.module';
import { PaxModule } from './pax/pax.module';
import { AircraftModule } from "./aircraft/aircraft.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { RankModule } from './rank/rank.module';
import { TimezoneModule } from './timezone/timezone.module';
import { UserModule } from './user/user.module';
import { LoggerModule } from "./logger/logger.module";
import { APP_GUARD, APP_INTERCEPTOR } from "@nestjs/core";
import { LoggerInterceptor } from "./core/interceptors/logger.interceptor";
import { DevGuard } from "./core/guards/dev.guard";
import { UnitModule } from './unit/unit.module';
import { AnimalModule } from './animal/animal.module';
import { SeatModule } from './seat/seat.module';
import { SeatTemplateModule } from './seat-template/seat-template.module';
import { FlightDocumentModule } from './flight-document/flight-document.module';
import { LegDocumentModule } from './leg-document/leg-document.module';
import { DocumentModule } from './document/document.module';
import { DataSource } from 'typeorm';
import { ScheduleModule } from "@nestjs/schedule";
import { SharedModule } from "./shared/shared.module";
import * as path from "path";
import { MailerModule } from "@nestjs-modules/mailer";
import { HandlebarsAdapter } from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: 'postgres',
        url: process.env.DATABASE_URI,
        entities: [
          __dirname + '/**/*.entity{.ts,.js}',
        ],
        logging: ['test', 'dev', 'review', 'local'].includes(process.env.APP_ENV) ? ['error', 'schema'] : false,
        migrations: [__dirname + '/migrations/*{.ts,.js}'],
        cli: {
          migrationsDir: "src/migrations"
        },
        synchronize: false,
        ssl: !['review', 'local'].includes(process.env.APP_ENV),
        extra: !['review', 'local'].includes(process.env.APP_ENV) ? {
          ssl: {
            rejectUnauthorized: false,
          },
        } : {},
        maxQueryExecutionTime: 1000
      }),
      dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize();
        await dataSource.synchronize();
        await dataSource.runMigrations();
        return dataSource;
      },
    }),
    // Rate limit, 300 requêtes maximum toutes les 15min par IP
    ThrottlerModule.forRoot(
        [{
          ttl: 60 * 15,
          limit: 300,
        }]
    ),
    MailerModule.forRoot({
      transport: {
        host: `${process.env.MAIL_HOST}`,
        port: `${process.env.MAIL_PORT}`,
        secure: !process.env.INTRADEF || process.env.INTRADEF === 'false', // true for 465, false for other ports
        auth: {
          user: `${process.env.MAIL_USER}`,
          pass: `${process.env.MAIL_PASSWORD}`
        },
        tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false
        }
      },
      template: {
        dir: path.resolve(__dirname, '..', 'mail_template'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    AircraftModule,
    FlightModule,
    AuthModule,
    LegModule,
    AirportModule,
    FlightHistoricModule,
    PaxModule,
    RankModule,
    TimezoneModule,
    UserModule,
    LoggerModule,
    UnitModule,
    AnimalModule,
    SeatModule,
    SeatTemplateModule,
    FlightDocumentModule,
    LegDocumentModule,
    DocumentModule,
    ScheduleModule.forRoot(),
    SharedModule
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggerInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: DevGuard,
    }
  ],
})
export class AppModule {
}
