import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { DatabaseFile } from "./entities/database-file.entity";
import { FlightDocument } from "../flight-document/entities/flight-document.entity";
import { LegDocument } from "../leg-document/entities/leg-document.entity";

@Injectable()
export class DatabaseFileService {

  constructor(@InjectRepository(DatabaseFile)
              private readonly _fileRepository: Repository<DatabaseFile>) {
  }

  /**
   * Sauvegarde d'un document vol ou leg
   * @param flightLegDocument
   * @param filename
   * @param dataBuffer
   * @param documentAttribute
   */
  async saveFlightLegFile(flightLegDocument: FlightDocument | LegDocument, filename: string, dataBuffer: Buffer, documentAttribute: string) {
    const fileToSave = {
      filename: filename,
      data: dataBuffer
    }
    fileToSave[documentAttribute] = flightLegDocument
    await this._fileRepository.save(fileToSave)
  }

  /**
   * Récupération d'un document
   * @param fileId
   */
  async getFile(fileId: number) {
    return this._fileRepository.findOneBy({id: fileId});
  }

  /**
   * Suppression d'un document
   * @param id
   */
  async delete(id: number) {
    return this._fileRepository.delete({id: id});
  }
}
