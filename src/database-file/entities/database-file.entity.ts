import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { FlightDocument } from "../../flight-document/entities/flight-document.entity";
import { LegDocument } from "../../leg-document/entities/leg-document.entity";


@Entity('database_file')
export class DatabaseFile {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToOne(type => FlightDocument, flightDocument => flightDocument.cover)
  flight_document_cover: FlightDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.pax_manifest)
  leg_document_pax_manifest: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.animal_manifest)
  leg_document_animal_manifest: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.work_sheet)
  leg_document_work_sheet: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.pax_xlsx)
  leg_document_pax_xlsx: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.tickets)
  leg_document_tickets: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.seats)
  leg_document_seats: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.cover)
  leg_document_cover: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.pnl)
  leg_document_pnl: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.tm5)
  leg_document_tm5: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.pax_manifest_lmc)
  leg_document_pax_manifest_lmc: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.animal_manifest_lmc)
  leg_document_animal_manifest_lmc: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.work_sheet_lmc)
  leg_document_work_sheet_lmc: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.tickets_lmc)
  leg_document_tickets_lmc: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.pnl_lmc)
  leg_document_pnl_lmc: LegDocument;

  @OneToOne(type => LegDocument, legDocument => legDocument.tm5_lmc)
  leg_document_tm5_lmc: LegDocument;

  @Column()
  filename: string;

  @Column({
    type: 'bytea',
  })
  data: Uint8Array;
}
