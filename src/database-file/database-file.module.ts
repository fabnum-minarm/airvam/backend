import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { DatabaseFile } from "./entities/database-file.entity";
import { DatabaseFileService } from "./database-file.service";

@Module({
  imports: [TypeOrmModule.forFeature([DatabaseFile])],
  controllers: [],
  providers: [DatabaseFileService],
  exports: [DatabaseFileService]
})
export class DatabaseFileModule {}
