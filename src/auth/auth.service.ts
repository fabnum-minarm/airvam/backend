import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from "../user/user.service";
import { JwtService } from "@nestjs/jwt";
import { plainToInstance } from "class-transformer";
import { UserDto } from "../user/dto/user.dto";
import camelcaseKeys = require("camelcase-keys");

@Injectable()
export class AuthService {
  constructor(private readonly _userService: UserService,
              private readonly _jwtService: JwtService,) {
  }

  /**
   * Vérification si l'utilisateur a le droit de se connecter et renvoi du token JWT si il a le droit
   * @param userEmail
   */
  public async validateUser(userEmail: string): Promise<any> {
    let userToReturn: any = await this._userService.findOne(userEmail, true);
    if (!!userToReturn && userToReturn.disabled) {
      throw new HttpException('Votre compte a été supprimé. Merci de prendre contact avec l\'administrateur si vous souhaitez réactiver votre compte.',
        HttpStatus.UNAUTHORIZED);
    }
    if (!userToReturn) {
      throw new HttpException('Mauvais identifiant ou mot de passe.',
        HttpStatus.UNAUTHORIZED);
    }
    await this._userService.login(userToReturn.email);
    userToReturn = plainToInstance(UserDto, camelcaseKeys(userToReturn, {deep: true}))
    return {
      token: this._jwtService.sign(JSON.parse(JSON.stringify(userToReturn))),
    };
  }
}
