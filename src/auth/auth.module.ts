import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserModule } from "../user/user.module";
import { JwtModule } from "@nestjs/jwt";
import { ConfigModule } from "@nestjs/config";
import { JwtStrategy } from "./strategies/jwt.strategy";
import { PassportModule } from "@nestjs/passport";
import { Oauth2Strategy } from "./strategies/oauth2.strategy";
import { HttpModule } from "@nestjs/axios";

@Module({
  imports: [
    ConfigModule.forRoot(),
    UserModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {expiresIn: '1d'},
    }),
    PassportModule.register({
      session: true,
      defaultStrategy: 'oauth2'
    }),
    HttpModule
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtStrategy,
    Oauth2Strategy
  ]
})
export class AuthModule {
}
