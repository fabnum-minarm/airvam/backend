import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-oauth2";
import { AuthService } from "../auth.service";
import { HttpService } from "@nestjs/axios";
import { catchError } from "rxjs";
import { Request } from "express";

@Injectable()
export class Oauth2Strategy extends PassportStrategy(Strategy, 'oauth2') {
  constructor(private _authService: AuthService,
              private readonly _httpService: HttpService) {
    super({
      authorizationURL: process.env.OPENID_AUTHORIZATION,
      tokenURL: process.env.OPENID_TOKEN,
      callbackURL: process.env.OPENID_CALLBACK_URL,
      clientID: process.env.OPENID_CLIENT_ID,
      clientSecret: process.env.OPENID_CLIENT_SECRET,
      scope: ['email', 'profile', 'openid'],
    });
  }

  authenticate(req: Request, options?: any) {
    const response = req.res;
    super.redirect = (url: string, status: number) => {
      if(!status || status === 302) {
        response.send(url);
        response.end();
      } else {
        response.setHeader('Location', url);
        response.setHeader('Content-Length', '0');
        response.end();
      }
    }
    super.authenticate(req, options);
  }

  async validate(accessToken: string, refreshToken: string, profile: any, done: Function): Promise<any> {
    const jwt: string = await this._authService.validateUser(profile.email);
    return done(null, jwt);
  }

  async userProfile(accessToken: string, done: Function) {
    const config = {
      headers: {Authorization: `Bearer ${accessToken}`}
    };

    this._httpService.get(process.env.OPENID_USERINFO, config)
      .pipe(
        catchError(e => {
          return done(e);
        }),
      )
      .subscribe((response: any) => {
        return done(null, response.data);
      });
  }
}
