import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiOperation } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";
import { LoginDto } from "./dto/login.dto";
import { Dev } from "../core/decorators/dev.decorator";
import { Oauth2Guard } from "./guards/oauth2.guard";
import {  Response } from "express";


@Controller('auth')
export class AuthController {
  constructor(private readonly _authService: AuthService) {
  }

  @Get('login')
  @UseGuards(Oauth2Guard)
  @ApiOperation({summary: 'Route à visiter pour s\'authentifier'})
  login() {
  }

  @Get('login/token')
  @UseGuards(Oauth2Guard)
  @ApiOperation({summary: `Route à appeler pour récupérer le JWT`})
  redirect(@Res() res: Response) {
    res.send(res.req?.user);
  }

  @Get('logout')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({summary: 'Déconnecte un utilisateur'})
  logout() {
  }

  @Post('local-login')
  @Dev()
  @ApiOperation({summary: 'Route à visiter pour s\'authentifier'})
  localLogin(@Body() loginDto: LoginDto) {
    return this._authService.validateUser(loginDto.email);
  }

}
