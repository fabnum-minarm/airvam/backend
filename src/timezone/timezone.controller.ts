import { Controller, Get, UseGuards } from '@nestjs/common';
import { TimezoneService } from './timezone.service';
import { TimezoneDto } from "./dto/timezone.dto";
import { plainToInstance } from "class-transformer";
import { Timezone } from "./entities/timezone.entity";
import camelcaseKeys = require("camelcase-keys");
import { ApiOperation } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";

@Controller('timezone')
@UseGuards(AuthGuard('jwt'))
export class TimezoneController {
  constructor(private readonly _timezoneService: TimezoneService) {}

  @Get()
  @ApiOperation({summary: 'Retourne toutes les timezones'})
  async findAll(): Promise<TimezoneDto[]> {
    const timezones: Timezone[] = await this._timezoneService.findAll();
    return plainToInstance(TimezoneDto, camelcaseKeys(timezones, {deep: true}));
  }
}
