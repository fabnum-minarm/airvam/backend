import { Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Airport } from "../../airport/entities/airport.entity";

@Entity('timezone')
export class Timezone {
  @PrimaryColumn({length: 255})
  location: string;

  @OneToMany(type => Airport, airport => airport.timezone)
  airports: Airport[];
}
