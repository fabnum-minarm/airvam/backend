import { IsNotEmpty } from "class-validator";

export class TimezoneDto {
  @IsNotEmpty()
  location: boolean;
}
