import { Module } from '@nestjs/common';
import { TimezoneService } from './timezone.service';
import { TimezoneController } from './timezone.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Timezone } from "./entities/timezone.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Timezone])],
  controllers: [TimezoneController],
  providers: [TimezoneService],
  exports: [TimezoneService]
})
export class TimezoneModule {}
