import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Timezone } from "./entities/timezone.entity";

@Injectable()
export class TimezoneService {

  constructor(@InjectRepository(Timezone)
              private readonly _timezoneRepository: Repository<Timezone>) {
  }

  /**
   * Récupération de toutes les timezones
   */
  findAll(): Promise<Timezone[]> {
    return this._timezoneRepository.find({
      order: {
      }
    });
  }
}
