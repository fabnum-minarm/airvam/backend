import {
  Controller,
  Post,
  Body,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Param,
  Get, Res, HttpException, HttpStatus
} from '@nestjs/common';
import { PaxService } from './pax.service';
import { AuthGuard } from "@nestjs/passport";
import { FileInterceptor } from "@nestjs/platform-express";
import { ApiBody, ApiConsumes, ApiOperation } from "@nestjs/swagger";
import { FileUploadDto } from "../core/dto/file-upload.dto";
import { PaxDto } from "./dto/pax.dto";
import { plainToInstance } from "class-transformer";
import { Pax } from "./entities/pax.entity";
import { Utils } from "../core/utils";
import { FlightHistoricService } from "../flight-historic/flight-historic.service";
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import { AirvamLogger } from "../logger/airvam.logger";
import { FlightGuard } from "../core/guards/flight.guard";
import { FlightArchived } from "../core/decorators/flight-archived.decorator";
import camelcaseKeys = require("camelcase-keys");
const snakecaseKeys = require('snakecase-keys');

@Controller('pax')
@UseGuards(AuthGuard('jwt'))
export class PaxController {
  private readonly _logger = new AirvamLogger('PaxController');

  constructor(private readonly _paxService: PaxService,
              private readonly _flightHistoricService: FlightHistoricService) {
  }

  @Get(':flightId')
  @ApiOperation({summary: 'Retourne tout les PAXs d\'un vol'})
  @UseGuards(FlightGuard)
  async findByFlight(@Param('flightId') flightId: string): Promise<PaxDto[]> {
    const paxs: Pax[] = await this._paxService.findByFlight(+flightId);
    return plainToInstance(PaxDto, camelcaseKeys(paxs, {deep: true}));
  }

  @Post('parse/:flightId')
  @UseInterceptors(
    FileInterceptor(
      'file',
      {
        fileFilter: Utils.excelFileFilter,
        limits: {
          fileSize: 1e+7
        },
      }
    )
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Liste PAX (excel)',
    type: FileUploadDto,
  })
  @ApiOperation({summary: 'Parsing du fichier excel'})
  parseFile(@Param('flightId') flightId: string,
            @UploadedFile() file): Promise<PaxDto[]> {
    return this._paxService.parse(+flightId, file);
  }

  @Post('/updateList/:flightId')
  @UseGuards(FlightGuard)
  @FlightArchived(false)
  @ApiOperation({summary: 'Mise à jour de PAXS (ajout / édition / suppression)'})
  async updateList(@Param('flightId') flightId: string,
                   @Body() payload: { paxs: PaxDto[], to_delete: number[] },
                   @AuthUser() user: UserDto): Promise<PaxDto[]> {
    // @ts-ignore
    const paxListReturned = await this._paxService.updateList(+flightId, plainToInstance(Pax, snakecaseKeys(payload.paxs)), payload.to_delete);
    await this._flightHistoricService.insertByFlightId(+flightId, user.email, `Mise à jour de la liste PAX`);
    return plainToInstance(PaxDto, camelcaseKeys(paxListReturned, {deep: true}));
  }

  @Get('export/:flightId')
  @UseGuards(FlightGuard)
  @ApiOperation({summary: 'Export XLS des PAXS'})
  async exportFile(@Param('flightId') flightId: string,
                   @Res() res): Promise<any> {
    try{
      const streamFile = await this._paxService.exportXls(+flightId);
      res.setHeader("Content-disposition", `attachment;`);
      res.contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      streamFile.pipe(res);
    }
    catch (err) {
      this._logger.error('', err);
      throw new HttpException(`Une erreur est survenue durant l'export de la liste PAX`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
