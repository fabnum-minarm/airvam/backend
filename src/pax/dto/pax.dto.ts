import { CreatePaxDto } from "./create-pax.dto";
import { IsArray, IsBoolean, IsOptional } from "class-validator";
import { LegDto } from "../../leg/dto/leg.dto";

export class PaxDto extends CreatePaxDto {
  @IsOptional()
  id: number;

  @IsArray()
  @IsOptional()
  legs: LegDto[];

  @IsBoolean()
  @IsOptional()
  underage: boolean;
}
