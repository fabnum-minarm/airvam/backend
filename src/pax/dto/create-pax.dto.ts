import { IsNotEmpty, IsNumber, IsObject, IsOptional, IsString } from "class-validator";
import { AirportDto } from "../../airport/dto/airport.dto";
import { RankDto } from "../../rank/dto/rank.dto";

export class CreatePaxDto {
  @IsString()
  @IsOptional()
  atr: string;

  @IsObject()
  @IsOptional()
  departure: AirportDto;

  @IsObject()
  @IsOptional()
  arrival: AirportDto;

  @IsNumber()
  @IsOptional()
  number: string;

  @IsNumber()
  @IsNotEmpty()
  weight: string;

  @IsNumber()
  @IsOptional()
  luggageWeight: string;

  @IsString()
  @IsOptional()
  gender: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsObject()
  @IsNotEmpty()
  rank: RankDto;

  @IsString()
  @IsOptional()
  category: string;

  @IsString()
  @IsOptional()
  militaryId: string;

  @IsString()
  @IsOptional()
  birthdate: string;

  @IsString()
  @IsNotEmpty()
  unit: string;

  @IsString()
  @IsNotEmpty()
  nationality: string;

  @IsString()
  @IsOptional()
  observations: string;
}
