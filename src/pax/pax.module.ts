import { forwardRef, Module } from '@nestjs/common';
import { PaxService } from './pax.service';
import { PaxController } from './pax.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Pax } from "./entities/pax.entity";
import { AirportModule } from "../airport/airport.module";
import { RankModule } from "../rank/rank.module";
import { UnitModule } from "../unit/unit.module";
import { FlightHistoricModule } from "../flight-historic/flight-historic.module";
import { LegModule } from "../leg/leg.module";
import { SeatModule } from "../seat/seat.module";
import { FlightModule } from "../flight/flight.module";
import { FlightDocumentModule } from "../flight-document/flight-document.module";
import { PaxManifestModule } from "../pax-manifest/pax-manifest.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Pax]),
    AirportModule,
    RankModule,
    UnitModule,
    FlightHistoricModule,
    forwardRef(() => LegModule),
    SeatModule,
    forwardRef(() => FlightModule),
    forwardRef(() => FlightDocumentModule),
    PaxManifestModule
  ],
  controllers: [PaxController],
  providers: [PaxService],
  exports: [PaxService]
})
export class PaxModule {}
