import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { PaxDto } from './dto/pax.dto';
import { AirvamLogger } from "../logger/airvam.logger";
import { Pax } from "./entities/pax.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { In, Repository, SelectQueryBuilder } from "typeorm";
import { AirportService } from "../airport/airport.service";
import { RankService } from "../rank/rank.service";
import { UnitService } from "../unit/unit.service";
import { plainToInstance } from "class-transformer";
import { AirportDto } from "../airport/dto/airport.dto";
import { RankDto } from "../rank/dto/rank.dto";
import { LegService } from "../leg/leg.service";
import { SeatService } from "../seat/seat.service";
import { FlightService } from "../flight/flight.service";
import { LegStatus } from "../leg/enums/leg-status.enum";
import { FlightDocumentService } from "../flight-document/flight-document.service";
import * as fs from "fs";
import { Flight } from "../flight/entities/flight.entity";
import { PaxManifestService } from "../pax-manifest/pax-manifest.service";
import { Workbook, Worksheet } from "exceljs";
import camelcaseKeys = require("camelcase-keys");

const moment = require('moment');
const ExcelJS = require('exceljs');
const uuid = require('uuid');

@Injectable()
export class PaxService {
  private readonly _logger = new AirvamLogger('PaxService');

  constructor(@InjectRepository(Pax)
              private readonly _paxRepository: Repository<Pax>,
              private _airportService: AirportService,
              private _rankService: RankService,
              private _unitService: UnitService,
              @Inject(forwardRef(() => LegService))
              private _legService: LegService,
              private _seatService: SeatService,
              @Inject(forwardRef(() => FlightService))
              private _flightService: FlightService,
              @Inject(forwardRef(() => FlightDocumentService))
              private _flightDocumentService: FlightDocumentService,
              private _paxManifestService: PaxManifestService) {
  }

  /**
   * Récupération des PAXs d'un vol
   * @param flightId
   * @param withSeat
   * @param legId
   */
  async findByFlight(flightId: number, withSeat = false, legId?: number): Promise<Pax[]> {
    const flight = await this._flightService.findOneLight(flightId);
    const query = this._getPaxQueryBuilder()
      .where('"legs"."id" IN (:...legsId)', {
        legsId: flight.legs.map(l => l.id),
      });
    if (withSeat) {
      query.leftJoinAndSelect('pax.seats', 'seats', !!legId ? `seats.leg.id = ${legId}` : '');
      query.leftJoinAndSelect('seats.leg', 'leg');
    }

    return query.getMany();
  }

  /**
   * Récupération des PAXs d'un leg
   * @param legId
   * @param withSeat
   */
  async findByLeg(legId: number, withSeat = false): Promise<Pax[]> {
    const query = this._getPaxQueryBuilder()
      .where('"legs"."id" = :legId', {
        legId: legId,
      });
    if (withSeat) {
      query.leftJoinAndSelect('pax.seats', 'seats')
      query.leftJoinAndSelect('seats.leg', 'leg')
    }
    query.leftJoin('pax.pax_manifest', 'pax_manifest', `"pax_manifest"."legId" = :legId`, {
      lmc: true,
      legId: legId,
    })
      .addSelect(['pax_manifest.id', 'pax_manifest.manifest_number', 'pax_manifest.lmc']);

    return query.getMany();
  }

  /**
   * Suppression des PAXs d'un vol
   * @param flightId
   */
  async deleteByFlight(flightId: number) {
    const flight = await this._flightService.findOneLight(flightId);
    const paxIds = await this._paxRepository.createQueryBuilder('pax')
      .select('pax.id')
      .leftJoin('pax.legs', 'legs')
      .where('"legs"."id" IN (:...legsId)', {
        legsId: flight.legs.map(l => l.id),
      }).getMany();
    await this._paxRepository.delete({id: In(paxIds.map(p => p.id))});
  }

  /**
   * Parsing d'un fichier excel de PAXs
   * @param flightId
   * @param file
   */
  async parse(flightId: number, file): Promise<PaxDto[]> {
    let workbook = new ExcelJS.Workbook();
    let paxList: PaxDto[];
    try {
      await workbook.xlsx.load(file.buffer);
      paxList = this._convertExcelToJson(workbook.worksheets[0]);
    } catch (error) {
      this._logger.error('ERREUR PARSING FICHIER PAX', error);
      throw new HttpException('Le fichier fournit ne peut pas être lu.', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    paxList = await this._addDependencies(flightId, paxList);

    return paxList;
  }

  /**
   * Mise à jour de la liste de PAXs d'un vol
   * Ajout, édition et suppression
   * @param flightId
   * @param paxs
   * @param to_delete
   */
  async updateList(flightId: number, paxs: Pax[], to_delete: number[]): Promise<Pax[]> {
    let flight = await this._flightService.findOneLight(flightId);
    paxs = paxs.map(p => {
      p.legs = this._affectLeg(p, flight);
      p.underage = this._computeUnderage(p);
      if (flight.legs.findIndex(l => l.departure.id === p.departure?.id) < 0) {
        p.departure = null;
      }
      if (flight.legs.findIndex(l => l.arrival.id === p.arrival?.id) < 0) {
        this._logger.error('AEROPORT ARRIVEE PAX NON PRESENT DANS LE VOL', null);
        throw new HttpException(`L'aéroport d'arrivée du PAX doit être présent dans les aéroports d'arrivée des legs du vol.`, HttpStatus.BAD_REQUEST);
      }
      return p;
    });
    paxs = paxs.filter(p => p.legs && p.legs.length > 0);
    await this._checkSeatsData(flight, paxs, to_delete);

    let paxsSaved = await this._paxRepository.save(paxs);

    // Mise à jour des PAXs en LMC si un des legs était en LMC.
    flight = await this._flightService.findOneLight(flightId);
    paxsSaved = paxsSaved.map((p: Pax) => {
      p.legs = this._affectLeg(p, flight);
      // @ts-ignore
      p.lmc = true;
      return p;
    });

    await Promise.all([
      this._paxManifestService.lmc(paxsSaved.filter((p: any) => p.lmc)),
      this._paxRepository.delete({id: In(to_delete)}),
    ]);

    const paxsToReturn = await this._getPaxQueryBuilder()
      .where('"legs"."id" IN (:...legsId)', {
        legsId: flight.legs.map(l => l.id),
      })
      .getMany();

    if (paxs.length <= 0 && paxsToReturn.length <= 0) {
      const promises = [];
      flight.legs.forEach(l => {
        promises.push(this._legService.updateStatus(l.id, LegStatus.incomplete));
      });
      await Promise.all(promises);
    }
    return paxsToReturn;
  }

  /**
   * Export de la liste des PAXs d'un vol au format XLSX
   * @param flightId
   */
  exportXls(flightId: number): Promise<fs.ReadStream> {
    return new Promise<fs.ReadStream>(async (resolve, reject) => {
      const workbook = await this.generateWorkbook(flightId);

      const guidForClient = uuid.v1();
      let pathNameWithGuid = `${guidForClient}_result.xlsx`;
      await workbook.xlsx.writeFile(pathNameWithGuid);
      let stream = fs.createReadStream(pathNameWithGuid);
      stream.on("close", () => {
        fs.unlink(pathNameWithGuid, (error) => {
          if (error) {
            throw error;
          }
        });
      });
      resolve(stream);
      return;
    });
  }

  /**
   * Génération d'un fichier excel
   * @param flightId
   * @param legId
   * @param freeSeating
   */
  async generateWorkbook(flightId: number, legId?: number, freeSeating?: boolean): Promise<Workbook> {
    const workbook = new ExcelJS.Workbook();
    const sheet = workbook.addWorksheet('PAX');
    sheet.addRows(await this._generateWorksheet(flightId, legId, freeSeating));
    this._setPropertiesOnXls(sheet);
    return workbook;
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Converti une feuille excel en entrée vers un tableau d'objets PAXs
   * @param worksheet
   */
  private _convertExcelToJson(worksheet: Worksheet): PaxDto[] {
    const headers = {
      'ATR': 'atr',
      'Departure': 'departure',
      'Arrival': 'arrival',
      'Pax Nbr': 'number',
      'Weight p.p. (Kg)': 'weight',
      'Luggage Weight p.p. (Kg)': 'luggageWeight',
      'Gender': 'gender',
      'Name': 'lastName',
      'First Name': 'firstName',
      'Rank/Civil': 'rank',
      'Pax Category': 'category',
      'Mil ID / Civ ID': 'militaryId',
      'Date of Birth': 'birthdate',
      'Unit': 'unit',
      'Unit (mil)': 'unit',
      'Nationality': 'nationality',
      'Observations': 'observations'
    };
    const paxs = [];
    let firstRow = worksheet.getRow(1);
    if (!firstRow.cellCount) return;
    let keys = []
    for (let i = 1; i <= +firstRow.values.length; i++) {
      keys.push(firstRow.getCell(i).text);
    }

    worksheet.eachRow((row, rowNumber) => {
      if (rowNumber <= 1) return;
      let values = row.values;
      let t: any = {};
      for (let i = 0; i < keys.length; i++) {
        const header = Object.keys(headers).find(h =>
          h?.replace(/\W/g, '')?.toLowerCase().trim() === keys[i]?.replace(/\W/g, '')?.toLowerCase().trim()
        );
        if (!!header) {
          t[headers[header]] = values[i + 1];
        }
      }
      t.atr = t.atr?.trim();
      if (t.birthdate instanceof Date) {
        t.birthdate = `${t.birthdate.getDate().toString().padStart(2, '0')}/${(t.birthdate.getMonth() + 1).toString().padStart(2, '0')}/${t.birthdate.getFullYear()}`;
      }
      paxs.push(t);
    });
    return paxs;
  }

  /**
   * Rajoute les différents sous-objets à la liste des PAXs
   * @param flightId
   * @param paxList
   * @private
   */
  private async _addDependencies(flightId: number, paxList: PaxDto[]): Promise<PaxDto[]> {
    const flight = await this._flightService.findOneLight(flightId);
    paxList = await Promise.all(paxList.map(async (p) => {
      p.departure = plainToInstance(AirportDto, camelcaseKeys(await this._airportService.findOneWithClause({
        oaci: p.departure
      }), {deep: true}));
      p.arrival = plainToInstance(AirportDto, camelcaseKeys(await this._airportService.findOneWithClause({
        oaci: p.arrival
      }), {deep: true}));
      p.rank = plainToInstance(RankDto, camelcaseKeys(await this._rankService.findOneWithClause({name: p.rank}), {deep: true}));
      p.legs = this._affectLeg(p, flight);
      p.underage = this._computeUnderage(p);
      return p;
    }));
    return paxList;
  }

  /**
   * Affecte un PAX aux différents legs qu'il emprunte
   * @param pax
   * @param flight
   * @private
   */
  private _affectLeg(pax, flight) {
    const legs = [];
    let idxStart = 0;
    if (pax.departure) {
      const idx = flight.legs.findIndex(l => l.departure.id === pax.departure.id);
      idxStart = idx >= 0 ? idx : 0;
    }
    if (pax.arrival) {
      const idx = flight.legs.findIndex(l => l.arrival.id === pax.arrival.id);
      for (let i = idxStart; i <= idx; i++) {
        legs.push(flight.legs[i]);
      }
    }
    return legs;
  }

  /**
   * Retourne l'objet Query Builder générique pour la récupération d'un PAX
   * @private
   */
  private _getPaxQueryBuilder(): SelectQueryBuilder<Pax> {
    return this._paxRepository.createQueryBuilder('pax')
      .select(['pax.id', 'pax.atr', 'pax.number', 'pax.weight', 'pax.luggage_weight', 'pax.gender', 'pax.last_name', 'pax.first_name',
        'pax.category', 'pax.military_id', 'pax.birthdate', 'pax.unit', 'pax.nationality', 'pax.observations', 'pax.underage', 'legs.id', 'animals.id'])
      .leftJoinAndSelect('pax.departure', 'departure')
      .leftJoinAndSelect('pax.arrival', 'arrival')
      .leftJoinAndSelect('pax.rank', 'rank')
      .leftJoinAndSelect('pax.animals', 'animals')
      .leftJoin('pax.legs', 'legs')
  }

  /**
   * Vérification si un PAX est mineur
   * @param pax
   * @private
   */
  private _computeUnderage(pax): boolean {
    if (['ENFANT', 'BB'].includes(pax.rank?.name)) {
      return true;
    }
    if (pax.birthdate && moment(pax.birthdate, 'DD/MM/YYYY').isValid()) {
      const years = moment().diff(moment(pax.birthdate, 'DD/MM/YYYY'), 'years', true);
      return years < 18;
    }
    return false;
  }

  /**
   * Mise à jours des données liées aux PAXs lors de la mise à jour ou la suppression de ceux-ci
   * Reset des sièges associés, des status des legs ...
   * @param flight
   * @param paxs
   * @param toDelete
   * @private
   */
  private async _checkSeatsData(flight: Flight, paxs: Pax[], toDelete: number[]) {
    const freeSeating = !flight.aircraft;
    const legsToUpdate = flight.legs.map(l => {
      return {
        id: l.id,
        status: null
      }
    });
    const paxLegDeleted = {};
    for (const p of paxs) {
      // Si id, on doit vérifier que les legs associés n'ont pas changés
      // Si changement (rajout de leg) on passe le statut en incomplet
      // Si changement (suppression de leg) on change pas le statut si Prêt, si Finalisé il passe en LMC
      if (!!p.id) {
        const {newLegs, legsDeleted, legsUnchanged} = await this._checkLegsPax(p);
        newLegs.forEach(leg => {
          this._changeLegStatus(legsToUpdate, leg.id, !freeSeating ? LegStatus.incomplete : LegStatus.scheduled);
        });
        legsDeleted.forEach(leg => {
          this._changeLegStatus(legsToUpdate, leg.id, LegStatus.scheduled);
          paxLegDeleted[leg.id] ? paxLegDeleted[leg.id].push(p.id) : paxLegDeleted[leg.id] = [p.id];
        });
        legsUnchanged.forEach(leg => {
          this._changeLegStatus(legsToUpdate, leg.id, LegStatus.lmc);
        });
      } else {
        // Si pas d'id, le statut des legs concernés passent en incomplet
        p.legs.forEach(leg => {
          this._changeLegStatus(legsToUpdate, leg.id, !freeSeating ? LegStatus.incomplete : LegStatus.scheduled);
        });
      }
    }
    const paxsToDelete = toDelete.length > 0 ? await this._getPaxQueryBuilder()
      .where('"pax"."id" IN (:...paxsId)', {
        paxsId: toDelete,
      }).getMany() : [];
    for (const p of paxsToDelete) {
      p.legs.forEach(leg => {
        this._changeLegStatus(legsToUpdate, leg.id, LegStatus.scheduled);
      });
    }
    // Reset des PAX sur les sièges concernés
    const promises = toDelete.length > 0 ? [this._seatService.deleteByPaxIds(toDelete)] : [];
    Object.keys(paxLegDeleted).forEach(legId => {
      promises.push(this._seatService.deleteByPaxAndLeg(+legId, paxLegDeleted[legId]));
    });
    legsToUpdate.forEach(leg => {
      if (!!leg.status) {
        promises.push(this._legService.updateStatus(leg.id, leg.status));
      }
    });
    await Promise.all(promises);
  }

  /**
   * Renvoi les modifications des legs associés à un PAX modifié
   * Si l'aéroport d'arrivée ou de départ d'un PAX est modifié, on renvoi les legs affectés
   * @param newPax
   * @private
   */
  private async _checkLegsPax(newPax: Pax) {
    const oldPax = await this._paxRepository.createQueryBuilder('pax')
      .select(['pax.id', 'legs.id'])
      .leftJoin('pax.legs', 'legs')
      .whereInIds([newPax.id])
      .getOne();
    const newLegs = newPax.legs.filter(leg => oldPax.legs.findIndex(l => l.id === leg.id) < 0);
    const legsDeleted = oldPax.legs.filter(leg => newPax.legs.findIndex(l => l.id === leg.id) < 0);
    const legsUnchanged = newPax.legs.filter(leg => oldPax.legs.findIndex(l => l.id === leg.id) >= 0)
    return {newLegs, legsDeleted, legsUnchanged};
  }

  /**
   * Mise à jour du statut des legs
   * @param legs
   * @param legId
   * @param newStatus
   * @private
   */
  private _changeLegStatus(legs: any[], legId, newStatus: LegStatus) {
    const leg = legs.find(l => l.id === legId);
    leg.status = newStatus;
  }

  /**
   * Génération d'une feuille excel avec les données des PAXs
   * @param flightId
   * @param legId
   * @param freeSeating
   * @private
   */
  private async _generateWorksheet(flightId: number, legId?: number, freeSeating?: boolean) {
    const paxs = await this.findByFlight(flightId, !!legId, legId);
    const rows = [['ATR', 'Departure', 'Arrival *', 'Pax Nbr', 'Weight p.p. (Kg) *', 'Luggage Weight p.p. (Kg) *', 'Gender', 'Name *', 'First Name *',
      'Rank/Civil *', 'Pax Category *', 'Mil ID / Civ ID*', 'Date of Birth', 'Unit *', 'Nationality', 'Observations']];
    if (!!legId) {
      rows[0].push('Seat');
    }
    paxs.forEach((pax: Pax) => {
      rows.push(this._generateRow(pax, freeSeating));
    });
    return rows;
  }

  /**
   * Génération d'une ligne pour une feuille excel
   * @param pax
   * @private
   */
  private _generateRow(pax: Pax, freeSeating?: boolean) {
    const row = [
      pax.atr ? pax.atr : '',
      pax.departure ? pax.departure.oaci : '',
      pax.arrival ? pax.arrival.oaci : '',
      pax.number ? pax.number.toString(10) : '',
      pax.weight.toString(10),
      pax.luggage_weight ? pax.luggage_weight.toString(10) : '',
      pax.gender ? pax.gender : '',
      pax.last_name,
      pax.first_name,
      pax.rank ? pax.rank.name : '',
      pax.category,
      pax.military_id,
      pax.birthdate ? pax.birthdate : '',
      pax.unit,
      pax.nationality ? pax.nationality : '',
      pax.observations ? pax.observations : '',
    ]
    if (pax.seats) {
      if (freeSeating) {
        row.push('FS');
      } else {
        row.push(pax.seats[0] ? `${pax.seats[0].number}${pax.seats[0].row}` : '');
      }
    }
    return row;
  }

  /**
   * Edition des propriétés de la feuille excel
   * @param worksheet
   * @private
   */
  private _setPropertiesOnXls(worksheet: Worksheet) {
    for (let i = 1; i < 17; i++) {
      worksheet.getColumn(i).width = 18;
    }
  }
}
