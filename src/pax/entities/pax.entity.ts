import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Leg } from "../../leg/entities/leg.entity";
import { Seat } from "../../seat/entities/seat.entity";
import { Airport } from "../../airport/entities/airport.entity";
import { Rank } from "../../rank/entities/rank.entity";
import { Animal } from "../../animal/entities/animal.entity";
import { PaxManifest } from "../../pax-manifest/entities/pax-manifest.entity";

@Entity('pax')
export class Pax {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(type => Leg, leg => leg.paxs, {onDelete: 'CASCADE'})
  @JoinTable()
  legs: Leg[];

  @OneToMany(type => PaxManifest, pax_manifest => pax_manifest.pax)
  pax_manifest: PaxManifest[];

  @OneToMany(type => Seat, seat => seat.pax)
  seats: Seat[];

  @OneToMany(type => Animal, animal => animal.pax)
  animals: Animal[];

  @Column({nullable: true, length: 20})
  atr: string;

  @Index('pax_departureId_index')
  @ManyToOne(type => Airport, airport => airport.paxDepartures)
  departure: Airport;

  @Index('pax_arrivalId_index')
  @ManyToOne(type => Airport, airport => airport.paxArrivals)
  arrival: Airport;

  @Column({nullable: true, type: 'integer'})
  number: number;

  @Column({nullable: false, type: 'float'})
  weight: number;

  @Column({nullable: true, type: 'float'})
  luggage_weight: number;

  @Column({nullable: true, length: 100})
  gender: string;

  @Column({nullable: false, length: 255})
  last_name: string;

  @Column({nullable: false, length: 255})
  first_name: string;

  @Index('pax_rankId_index')
  @ManyToOne(type => Rank, rank => rank.paxs)
  rank: Rank;

  @Column({nullable: true, length: 100})
  category: string;

  @Column({nullable: true, length: 100})
  military_id: string;

  @Column({nullable: true, length: 100})
  birthdate: string;

  @Column({nullable: false, length: 255, default: 'unit'})
  unit: string;

  @Column({nullable: true, length: 100})
  nationality: string;

  @Column({nullable: true, length: 255})
  observations: string;

  @Column({default: false})
  underage: boolean;
}
