import { Controller, Get, Header, Param, Res, StreamableFile, UseGuards } from '@nestjs/common';
import { LegDocumentService } from './leg-document.service';
import { AuthGuard } from "@nestjs/passport";
import { Readable } from "stream";
import { Response } from "express";
import { FlightGuard } from "../core/guards/flight.guard";
import { ApiOperation } from "@nestjs/swagger";

@Controller('leg-document')
@UseGuards(AuthGuard('jwt'), FlightGuard)
export class LegDocumentController {
  constructor(private readonly _legDocumentService: LegDocumentService) {
  }

  @Get('/:flightId/:legId/:documentId')
  @ApiOperation({summary: 'Téléchargement d\'un document'})
  async findOne(@Param('flightId') flightId: string,
                @Param('legId') legId: string,
                @Param('documentId') documentId: string): Promise<StreamableFile> {
    const file = await this._legDocumentService.getOne(+documentId);
    if (file) {
      const stream = Readable.from(file.data);
      return new StreamableFile(stream);
    }
  }

  @Get('/:flightId/:legId')
  @ApiOperation({summary: 'Téléchargement d\'un zip de tout les documents d\'un leg'})
  @Header('Content-Disposition', 'attachment; filename=something.pdf')
  async findLegDocuments(@Param('flightId') flightId: string,
                         @Param('legId') legId: string,
                         @Res() res: Response): Promise<any> {
    return this._legDocumentService.getZip(+flightId, +legId, res);
  }
}
