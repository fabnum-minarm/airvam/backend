import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { AirvamLogger } from "../logger/airvam.logger";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { LegDocument } from "./entities/leg-document.entity";
import { Pax } from "../pax/entities/pax.entity";
import { Leg } from "../leg/entities/leg.entity";
import { Seat } from "../seat/entities/seat.entity";
import { FinalizeDto } from "../leg/dto/finalize.dto";
import { SeatZone } from "../seat-template/dto/seat-zone.enum";
import { PaxService } from "../pax/pax.service";
import { PaxManifestService } from "../pax-manifest/pax-manifest.service";
import { DatabaseFileService } from "../database-file/database-file.service";
import * as fs from "fs";
import { Response } from "express";
import { DatabaseFile } from "../database-file/entities/database-file.entity";
import { Flight } from "../flight/entities/flight.entity";
import { Aircraft } from "../aircraft/entities/aircraft.entity";
import { Utils } from "../core/utils";
import { DocumentUtils } from "../core/document-utils";
import { FlightService } from "../flight/flight.service";
import { LegDocumentDocuments, LegDocumentDocumentsLmc } from "./dto/leg-document.dto";
import { Workbook } from "exceljs";
import { AnimalService } from "../animal/animal.service";

const path = require("path");
const hbs = require("handlebars");
const moment = require('moment');
const ExcelJS = require('exceljs');
const archiver = require('archiver');

@Injectable()
export class LegDocumentService {
  private readonly _templatePath = path.resolve(`./dist/flight_documents_template`);
  private readonly _logger = new AirvamLogger('LegDocumentService');

  constructor(@InjectRepository(LegDocument)
              private readonly _legDocumentsRepository: Repository<LegDocument>,
              @Inject(forwardRef(() => PaxService))
              private _paxService: PaxService,
              private _paxManifestService: PaxManifestService,
              private _fileService: DatabaseFileService,
              @Inject(forwardRef(() => FlightService))
              private _flightService: FlightService,
              @Inject(forwardRef(() => AnimalService))
              private _animalService: AnimalService) {
  }

  /**
   * Récupération d'un document en vue de son téléchargement
   * @param documentId
   */
  async getOne(documentId: number): Promise<DatabaseFile> {
    return await this._fileService.getFile(documentId);
  }

  /**
   * Listing des noms de tous les documents
   */
  getDocumentsName(): string[] {
    return LegDocumentDocuments.concat(LegDocumentDocumentsLmc);
  }

  /**
   * Récupération de la liste des documents d'un vol
   * @param flightId
   */
  async getByFlight(flightId: number): Promise<LegDocument[]> {
    return this._legDocumentsRepository.find({
      relations: this.getDocumentsName().concat(['leg', 'leg.departure', 'leg.arrival']),
      where: {leg: {flight: {id: flightId}}}
    });
  }

  /**
   * Récupération d'un fichier compressé zip contenant les documents d'un leg
   * @param flightId
   * @param legId
   * @param res
   */
  async getZip(flightId: number, legId: number, res: Response) {
    const archive = archiver('zip', {
      zlib: {level: 9} // Sets the compression level.
    });

    const flight: Flight = await this._flightService.findOneLight(flightId);

    //set the archive name
    const archiveName = `${flight.mission_number}-${flight.legs.findIndex(l => l.id === legId) + 1}_${flight.legs.length}`;
    res.attachment(`${archiveName}.zip`);

    //this is the streaming magic
    archive.pipe(res);

    const legDocuments = await this._legDocumentsRepository.findOne({
      relations: this.getDocumentsName(),
      where: {leg: {id: legId}}
    });
    for (const doc of this.getDocumentsName()) {
      if (!!legDocuments[doc]) {
        archive.append(legDocuments[doc].data, {name: legDocuments[doc].filename});
      }
    }

    archive.finalize();
  }

  /**
   * Génération des documents lors de la finalisation d'un leg
   * Possibilité de générer seulement les LMC où tous les documents
   * @param leg
   * @param paxs
   * @param finalizeDto
   */
  async generateDocuments(leg: Leg, paxs: Pax[], finalizeDto: FinalizeDto): Promise<LegDocument> {
    const freeSeating = !leg.flight.aircraft;
    let legDocument = await this._legDocumentsRepository.findOneBy({leg: {id: leg.id}});
    if (!legDocument) {
      legDocument = new LegDocument();
      // @ts-ignore
      legDocument.leg = {id: leg.id};
      legDocument = await this._legDocumentsRepository.save(legDocument);
    }
    try {
      const data: any = {
        leg: leg,
        paxs: await this._assignManifestNumber(DocumentUtils.sortPax(paxs), leg, finalizeDto),
      }
      data.paxsLmc = data.paxs.filter(p => p.pax_manifest[0].lmc);
      await Promise.all([
        this._generatePaxManifest(DocumentUtils.generateFileName('ManifestePax', 'pdf', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto, freeSeating),
        this._generateLiveAnimalsManifest(DocumentUtils.generateFileName('ManifesteAnimal', 'pdf', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto),
        this._generateWorkSheet(DocumentUtils.generateFileName('FeuilleTravail', 'pdf', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto, freeSeating),
        this._generateTickets(DocumentUtils.generateFileName('Billets', 'pdf', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto, freeSeating),
        this._generateCover(DocumentUtils.generateFileName('Couverture', 'pdf', leg.flight, false, leg), legDocument, data, finalizeDto, freeSeating),
        this._generatePNL(DocumentUtils.generateFileName('PNL', 'txt', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto, freeSeating),
        this._generateTM5(DocumentUtils.generateFileName('TM5', 'xlsx', leg.flight, finalizeDto.onlyLmc, leg), legDocument, data, finalizeDto),
        this._generatePaxXlsx(DocumentUtils.generateFileName('ExcelPax', 'xlsx', leg.flight, false, leg), legDocument, data, finalizeDto, freeSeating),
        freeSeating ? null : this._generateSeats(DocumentUtils.generateFileName('PlanCabine', 'pdf', leg.flight, false, leg), legDocument, data, finalizeDto)
      ]);
    } catch (e) {
      this._logger.error('ERREUR GENERATION DES DOCUMENTS LEG', e);
      throw new HttpException('ERREUR GENERATION DES DOCUMENTS LEG', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    await this._paxManifestService.resetLmc(paxs, leg);
    const query = this._legDocumentsRepository.createQueryBuilder('leg_document')
      .select()
      .where({id: legDocument.id});
    this.getDocumentsName().forEach(d => {
      query.leftJoin(`leg_document.${d}`, d);
      query.addSelect([`${d}.id`, `${d}.filename`]);
    });
    return query.getOne();
  }

  /**
   * Suppression des documents caducs d'un leg
   * Suppression ou reset de données associées à ces documents
   * @param flightId
   * @param legId
   * @param onlyLmc
   */
  async resetDocuments(flightId: number, legId: number, onlyLmc: boolean) {
    const docs = onlyLmc ? LegDocumentDocumentsLmc : this.getDocumentsName();
    const query = this._legDocumentsRepository.createQueryBuilder('leg_document')
      .select()
      .where({leg: {id: legId}});
    docs.forEach(d => {
      query.leftJoin(`leg_document.${d}`, d);
      query.addSelect(`${d}.id`);
    });
    const legDocument = await query.getOne();
    const promises = [];
    docs.forEach(d => {
      if (legDocument && legDocument[d]) {
        promises.push(this._fileService.delete(legDocument[d].id));
      }
    });
    if (legDocument) {
      docs.forEach(d => {
        legDocument[d] = null;
      });
    }
    if (!onlyLmc) {
      promises.push(this._paxManifestService.deleteByLeg(legId));
      promises.push(this._animalService.resetlmcByLeg(legId))
      promises.push(legDocument && legDocument.id ? this._legDocumentsRepository.delete(legDocument.id) : null);
    } else {
      promises.push(legDocument && legDocument.id ? this._legDocumentsRepository.update(legDocument.id, {
        pax_manifest_lmc: null,
        animal_manifest_lmc: null,
        work_sheet_lmc: null,
        tickets_lmc: null,
        tm5_lmc: null,
        pnl_lmc: null
      }) : null);
    }
    return await Promise.all(promises);
  };

  /**
   * Suppression des documents caducs d'un vol
   * @param flight
   */
  async resetDocumentsByFlight(flight: Flight) {
    const promises = [];
    flight.legs.forEach(l => {
      promises.push(this.resetDocuments(flight.id, l.id, false))
    });
    return Promise.all(promises);
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Génération du manifeste PAX (DOC Officiel)
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generatePaxManifest(fileName: string,
                                     legDocument: LegDocument,
                                     data: { leg: Leg, paxs: Pax[], paxsLmc: Pax[] },
                                     finalizeDto: FinalizeDto,
                                     freeSeating: boolean) {
    const dataPaxManifest = DocumentUtils.getDataPaxManifest(data.leg.flight, data.leg, data.leg, finalizeDto.onlyLmc ? data.paxsLmc : data.paxs, finalizeDto, freeSeating);
    if (!dataPaxManifest) {
      return;
    }
    const html = fs.readFileSync(`${this._templatePath}/pax_manifest_template.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataPaxManifest));

    await this._fileService.saveFlightLegFile(legDocument, fileName, buffer, finalizeDto.onlyLmc ? 'leg_document_pax_manifest_lmc' : 'leg_document_pax_manifest');
  }

  /**
   * Génération du fichier excel du listing des PAXs
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generatePaxXlsx(fileName: string,
                                 legDocument: LegDocument,
                                 data: { leg: Leg, paxs: Pax[] },
                                 finalizeDto: FinalizeDto,
                                 freeSeating: boolean) {
    const workbook: Workbook = await this._paxService.generateWorkbook(data.leg.flight.id, data.leg.id, freeSeating);
    // @ts-ignore
    const buffer: Buffer = await workbook.xlsx.writeBuffer();

    await this._fileService.saveFlightLegFile(legDocument, fileName, buffer, 'leg_document_pax_xlsx');
  }

  /**
   * Génération de la feuille de travail
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generateWorkSheet(fileName: string,
                                   legDocument: LegDocument,
                                   data: { leg: Leg, paxs: Pax[], paxsLmc: Pax[] },
                                   finalizeDto: FinalizeDto,
                                   freeSeating: boolean) {
    const paxs = DocumentUtils.sortByManifestNumber(finalizeDto.onlyLmc ? data.paxsLmc : data.paxs);
    paxs.sort((a, b) => a.first_name?.localeCompare(b.first_name));
    paxs.sort((a, b) => a.last_name?.localeCompare(b.last_name));
    if (paxs.length <= 0) {
      return;
    }

    const dataWorkSheet = {
      unit: finalizeDto.unit.name,
      serial_number: data.leg.flight.mission_name,
      flight_number: data.leg.flight.mission_number,
      aircraft_type: data.leg.flight.aircraft ? data.leg.flight.aircraft.type : data.leg.flight.aircraft_name,
      aircraft_registration: data.leg.flight.aircraft ? data.leg.flight.aircraft.registration : data.leg.flight.aircraft_id,
      airfield_departure_oaci: data.leg.departure.oaci,
      airfield_departure_name: data.leg.departure.name,
      airfield_arrival_oaci: data.leg.arrival.oaci,
      airfield_arrival_name: data.leg.arrival.name,
      date: moment(data.leg.departure_time).format('DD/MM/YYYY'),
      pages: [],
      total_paxs: paxs.length
    };
    const paxPageLength = 40;
    paxs.forEach((p: any, idx) => {
      if (!dataWorkSheet.pages[Math.floor(idx / paxPageLength)]) {
        dataWorkSheet.pages.push({
          paxs: [],
          total_paxs: null
        });
      }
      const currentPage = dataWorkSheet.pages[Math.floor(idx / paxPageLength)];
      p.seat = freeSeating ? {number: 'F', row: 'S'} : DocumentUtils.findSeat(p, data.leg);
      p.manifest_number = p.pax_manifest[0].manifest_number;
      currentPage.paxs.push(p);
      if (idx === paxs.length - 1 || (idx !== 0 && idx % (paxPageLength - 1) === 0)) {
        dataWorkSheet.pages[Math.floor(idx / paxPageLength)].total_paxs = dataWorkSheet.pages[Math.floor(idx / paxPageLength)].paxs.length;
      }
    });
    let emptyLines = paxPageLength - dataWorkSheet.total_paxs % paxPageLength;
    emptyLines = emptyLines === paxPageLength ? 0 : emptyLines;
    for (let i = 0; i < emptyLines; i++) {
      dataWorkSheet.pages[dataWorkSheet.pages.length - 1].paxs.push({});
    }

    const html = fs.readFileSync(`${this._templatePath}/feuille_travail.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataWorkSheet));

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      buffer,
      finalizeDto.onlyLmc ? 'leg_document_work_sheet_lmc' : 'leg_document_work_sheet');
  }

  /**
   * Génération du manifeste Animaux (DOC Officiel)
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @private
   */
  private async _generateLiveAnimalsManifest(fileName: string, legDocument: LegDocument, data: { leg: Leg, paxs: Pax[], paxsLmc: Pax[] }, finalizeDto: FinalizeDto) {
    const dataAnimalManifest = DocumentUtils.getDataAnimalManifest(data.leg.flight, data.leg, data.leg, finalizeDto.onlyLmc ? data.paxsLmc : data.paxs, finalizeDto);
    if (!dataAnimalManifest) {
      return;
    }

    const html = fs.readFileSync(`${this._templatePath}/animal_manifest_template.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataAnimalManifest));

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      buffer,
      finalizeDto.onlyLmc ? 'leg_document_animal_manifest_lmc' : 'leg_document_animal_manifest');
  }

  /**
   * Génération des billets (DOC Officiel)
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generateTickets(fileName: string,
                                 legDocument: LegDocument,
                                 data: { leg: Leg, paxs: Pax[], paxsLmc: Pax[] },
                                 finalizeDto: FinalizeDto,
                                 freeSeating: boolean) {
    let paxs = DocumentUtils.sortByManifestNumber(finalizeDto.onlyLmc ? data.paxsLmc : data.paxs);
    if (paxs.length <= 0) {
      return;
    }
    const dataTickets = {
      unit_code: finalizeDto.unit.code,
      serial_number: data.leg.flight.mission_name,
      airfield_departure_oaci: data.leg.departure.oaci,
      airfield_arrival_oaci: data.leg.arrival.oaci,
      airfield_arrival_name: data.leg.arrival.name,
      date: moment(data.leg.departure_time).format('DD/MM/YYYY'),
      time: moment(data.leg.flight.legs[0].departure_time).format('HH:mm'),
      weight_unit: data.leg.flight.imperial_system ? 'Lb' : 'Kg',
      ticket_mention: data.leg.departure.ticket_mention,
      pages: [],
    };
    const ticketPageLength = 5;
    const totalPagesLength = Math.ceil(paxs.length / ticketPageLength);
    // Tri des paxs pour pouvoir massicoter toutes les pages ensembles
    paxs = paxs.sort((a, b) => (Math.floor((a.pax_manifest[0].manifest_number - 1) / totalPagesLength) + ((a.pax_manifest[0].manifest_number - 1) % totalPagesLength) * 1000) -
      (Math.floor((b.pax_manifest[0].manifest_number - 1) / totalPagesLength) + ((b.pax_manifest[0].manifest_number - 1) % totalPagesLength) * 1000));

    paxs.forEach((p: any, idx) => {
      if (!dataTickets.pages[Math.floor(idx / ticketPageLength)]) {
        dataTickets.pages.push({
          paxs: []
        });
      }
      const currentPage = dataTickets.pages[Math.floor(idx / ticketPageLength)];
      p.seat = freeSeating ? {number: 'F', row: 'S'} : DocumentUtils.findSeat(p, data.leg);
      p.manifest_number = p.pax_manifest[0].manifest_number;
      currentPage.paxs.push(p);
    });

    const html = fs.readFileSync(`${this._templatePath}/tickets.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataTickets));

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      buffer,
      finalizeDto.onlyLmc ? 'leg_document_tickets_lmc' : 'leg_document_tickets');
  }

  /**
   * Génération de la page de couverture du dossier
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generateCover(fileName: string,
                               legDocument: LegDocument,
                               data: { leg: Leg, paxs: Pax[] },
                               finalizeDto: FinalizeDto,
                               freeSeating: boolean) {
    const dataPassengerFolder = DocumentUtils.getDataPassengerCover(data.leg.flight, data.paxs, finalizeDto, freeSeating);

    const html = fs.readFileSync(`${this._templatePath}/dossier_passager.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataPassengerFolder), false);

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      buffer,
      'leg_document_cover');
  }

  /**
   * Génération du fichier texte PNL (DOC Officiel)
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @param freeSeating
   * @private
   */
  private async _generatePNL(fileName: string,
                             legDocument: LegDocument,
                             data: { leg: Leg, paxs: Pax[], paxsLmc: Pax[] },
                             finalizeDto: FinalizeDto,
                             freeSeating: boolean) {
    const paxs = DocumentUtils.sortByManifestNumber(finalizeDto.onlyLmc ? data.paxsLmc : data.paxs);
    if (finalizeDto.onlyLmc && (!paxs || paxs.length <= 0)) {
      return;
    }
    let pnlText = 'PNL\n';
    pnlText += `${data.leg.flight.cotam_number}/${moment(data.leg.departure_time).format('DDMMM')} ${data.leg.departure.iata} PART1\n`;
    pnlText += `-${data.leg.arrival.iata}${String(paxs.length).padStart(3, '0')}Y\n`

    paxs.forEach(p => {
      const seat = freeSeating ? {number: 'F', row: 'S'} : DocumentUtils.findSeat(p, data.leg);
      pnlText += `1${p.last_name?.toUpperCase()}/${p.first_name?.toUpperCase()} .R/RQST HK1`;
      if (seat) {
        pnlText += `${seat.number}${seat.row} .R/ETLP HK1 1111111111111`;
      }
      pnlText += `\n`;
    });

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      Buffer.from(pnlText, 'utf8'),
      finalizeDto.onlyLmc ? 'leg_document_pnl_lmc' : 'leg_document_pnl');
  }

  /**
   * Génération du fichier excel TM5 pour les civils (DOC Officiel)
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @private
   */
  private async _generateTM5(fileName: string,
                             legDocument: LegDocument,
                             data: { leg: Leg, paxs: Pax[]; paxsLmc: Pax[] },
                             finalizeDto: FinalizeDto) {
    const paxs = finalizeDto.onlyLmc ? data.paxsLmc : data.paxs;
    const civilians: Pax[] = paxs.filter(p => ['ENFANT', 'MR', 'ME', 'BB', 'CIV'].includes(p.rank?.name));
    if (civilians.length <= 0) {
      return;
    }

    const tm5 = new ExcelJS.Workbook();
    await tm5.xlsx.readFile(`${this._templatePath}/tm5.xlsm`);
    const templateSheet = tm5.worksheets[0];

    const workbook = new ExcelJS.Workbook();
    civilians.forEach(civil => {
      const worksheet = workbook.addWorksheet('worksheet');
      worksheet.model = Object.assign(templateSheet.model, {
        mergeCells: templateSheet.model.merges
      });
      worksheet.name = `${civil.last_name?.toUpperCase()} ${civil.first_name?.toUpperCase()}`;
      worksheet.getCell('E4').value = civil.last_name?.toUpperCase();
      worksheet.getCell('F4').value = civil.first_name?.toUpperCase();
      worksheet.getCell('E9').value = moment(data.leg.departure_time).format('DD MMMM YYYY')
      worksheet.getCell('E10').value = data.leg.departure.name;
      worksheet.getCell('I10').value = data.leg.arrival.name;

      workbook.worksheets.push(worksheet);
    });

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      await workbook.xlsx.writeBuffer(),
      finalizeDto.onlyLmc ? 'leg_document_tm5_lmc' : 'leg_document_tm5');
  }

  /**
   * Génération du plan cabine
   * @param fileName
   * @param legDocument
   * @param data
   * @param finalizeDto
   * @private
   */
  private async _generateSeats(fileName: string, legDocument: LegDocument, data: { leg: Leg, paxs: Pax[] }, finalizeDto: FinalizeDto) {
    const dataSeats = {
      colored: finalizeDto.printColor,
      serial_number: data.leg.flight.mission_name,
      destination: data.leg.arrival.oaci,
      date: moment(data.leg.departure_time).format('DD/MM/YYYY'),
      aircraft_type: data.leg.flight.aircraft.name,
      aircraft_plan_cab: data.leg.flight.aircraft.plan_cab,
      zone_oa: DocumentUtils.nbrByZone(data.paxs, data.leg, SeatZone.OA),
      zone_ob: DocumentUtils.nbrByZone(data.paxs, data.leg, SeatZone.OB),
      zone_oc: DocumentUtils.nbrByZone(data.paxs, data.leg, SeatZone.OC),
      zone_od: DocumentUtils.nbrByZone(data.paxs, data.leg, SeatZone.OD),
      show_oa: !['A330 88Y'].includes(data.leg.flight.aircraft.short_name),
      show_ob: !['A330 88Y'].includes(data.leg.flight.aircraft.short_name),
      show_oc: true,
      show_od: ['A330 F-UJCT'].includes(data.leg.flight.aircraft.short_name),
      comments: this._compileComments(data.leg, data.leg.seats, data.paxs),
      seats: this._computeSeats(data.leg, data.leg.seats, data.paxs)
    }

    const html = fs.readFileSync(`${this._templatePath}/seats.hbs`, "utf-8");
    const buffer = await DocumentUtils.generatePDF(hbs.compile(html)(dataSeats));

    await this._fileService.saveFlightLegFile(
      legDocument,
      fileName,
      buffer,
      'leg_document_seats');
  }

  /**
   * Assignation des numéros de manifeste
   * Si le PAX en a déjà un on n'y touche pas
   * S'il n'en a pas, on incrémente par rapport au numéro maximum de la liste de PAXs
   * @param paxs
   * @param leg
   * @param finalizeDto
   * @private
   */
  private async _assignManifestNumber(paxs: Pax[], leg: Leg, finalizeDto: FinalizeDto) {
    let maxManifestNumber = finalizeDto.onlyLmc ? Math.max(...paxs.map(p => isNaN(p.pax_manifest && p.pax_manifest[0]?.manifest_number) ? 0 : p.pax_manifest[0]?.manifest_number)) : 0;
    paxs.forEach(p => {
      p.pax_manifest = p.pax_manifest ? p.pax_manifest : [];
      // @ts-ignore
      p.pax_manifest[0] = !!p.pax_manifest[0] ? p.pax_manifest[0] : {
        id: null,
        manifest_number: ++maxManifestNumber
      }
      if (finalizeDto.onlyLmc && !p.pax_manifest[0].id) {
        p.pax_manifest[0].lmc = true;
      }
    });
    await this._paxManifestService.updateManifestNumbers(paxs, leg);
    return paxs;
  }

  /**
   * Renvoi un listing des commentaires sur les sièges
   * @param leg
   * @param seats
   * @param paxs
   * @private
   */
  private _compileComments(leg: Leg, seats: Seat[], paxs: Pax[]): any[] {
    return seats.filter(s => !!s.observations).map(s => {
      const paxAssociated = paxs.find(p => p.seats.findIndex(seat => seat.leg.id === leg.id && seat.id === s.id) >= 0);
      return {
        title: !!paxAssociated ? `${s.number}${s.row} - ${paxAssociated.first_name} ${paxAssociated.last_name}` : `${s.number}${s.row}`,
        text: s.observations
      }
    });
  }

  /**
   * Formatage des données afin de générer les plans cabines
   * @param leg
   * @param seats
   * @param paxs
   * @private
   */
  private _computeSeats(leg: Leg, seats: Seat[], paxs: Pax[]): any[] {
    const toReturn: any[] = seats.map(s => {
      return {name: s.zone}
    }).filter((v, i, a) => a.findIndex(value => value.name === v.name) === i).sort((a, b) => a.name.localeCompare(b.name));

    const columnsArray = seats.map(s => {
      return {letter: s.row}
    });
    const columns: any[] = columnsArray.filter((v, i, a) => a.findIndex(value => value.letter === v.letter) === i).sort((a, b) => a.letter.localeCompare(b.letter));
    columns.splice(6, 0, {header: true, previous_spacer: true});
    columns.splice(2, 0, {header: true, next_spacer: true});

    toReturn.forEach((x: any) => {
      x.rows = seats.filter(s => s.zone === x.name).map(s => {
        return {number: s.number}
      }).filter((v, i, a) => a.findIndex(x => x.number === v.number) === i).sort((a, b) => a.number - b.number);
      x.rows.splice(0, 0, {header: true});
      switch (leg.flight.aircraft.short_name) {
        case 'A330 40-144-88':
          if (x.name === 'OC') {
            x.rows.splice(9, 0, {header: true, hide: ['E']});
          }
          break;
        case 'A330 F-UJCU':
          if (x.name === 'OC') {
            x.rows.splice(10, 0, {header: true, hide: ['E']});
          }
          break;
        case 'A330 F-UJCS':
          if (x.name === 'OA') {
            x.rows.splice(0, 1, {header: true, hide: ['C', 'F', 'G', 'J']});
          }
          break;
        case 'A330 F-UJCT':
          if (x.name === 'OA') {
            x.rows.splice(0, 1, {header: true, hide: ['C', 'F', 'G', 'J']});
            x.rows.splice(2, 0, {header: true, hide: ['D', 'E', 'F', 'G']});
          }
          if (x.name === 'OB') {
            x.rows.splice(0, 1, {header: true, hide: ['C', 'F', 'G', 'J']});
          }
          break;
        case 'A330 88Y':
          if (x.name === 'OC') {
            x.rows.splice(9, 0, {header: true, hide: ['E']});
          }
          break;
      }

      x.rows.forEach(r => {
        r.columns = JSON.parse(JSON.stringify(columns));
        r.columns.forEach(c => {
          const seat = seats?.find(s => s.row === c.letter && s.number === r.number);
          const pax = paxs.find(p => p.seats.findIndex(s => s.id === seat?.id) >= 0);
          c.text = this._getCellText(c.letter, r.number, pax);
          c.css = ``;
          if (r.header || c.header) c.css += ' seat-cell--header';
          if (this._isCellHidden(c, r, seats)) c.css += ' seat-cell--hide';
          if (!this._isCellDisplay(c, r, leg.flight.aircraft)) c.css += ' seat-cell--not-display';
          if (c.header) c.css += ' seat-cell--small';
          if (this._isCellLarge(c, r, leg.flight.aircraft)) c.css += ' seat-cell--large';
          if (seat?.blocked) c.css += ' seat-cell--blocked';
          if (seat?.observations) c.css += ' seat-cell--comment';
          c.css += this._getRankClassName(pax);
        });
      });
    });
    return toReturn;
  }

  /**
   * Vérification si le siège / cellule doit être caché sur le plan cabine
   * @param col
   * @param row
   * @param seats
   * @private
   */
  private _isCellHidden(col, row, seats: Seat[]): boolean {
    if (col.header && row.header) {
      return true;
    } else if (col.header || row.header) {
      return false;
    }
    return !(seats.findIndex(s => s.number === row.number && s.row === col.letter) >= 0);
  }

  /**
   * Vérification si le siège / cellule doit être généré sur le plan cabine
   * @param col
   * @param row
   * @param aircraft
   * @private
   */
  private _isCellDisplay(col, row, aircraft: Aircraft): boolean {
    if ((col.header || row.header) && (col?.hide?.includes(row.number) || row?.hide?.includes(col.letter))) {
      return false;
    }
    if (col.header || row.header) {
      return true;
    }
    switch (aircraft.short_name) {
      case 'A330 40-144-88':
        if (row.number >= 34 && col.letter === 'E')
          return false;
        break;
      case 'A330 F-UJCS':
        if (row.number < 5 && ['C', 'F', 'G', 'J'].includes(col.letter))
          return false;
        break;
      case 'A330 F-UJCT':
        if (row.number < 9 && ['F', 'G'].includes(col.letter))
          return false;
        if ((row.number < 2 || (row.number < 9 && row.number > 3)) && ['C', 'J'].includes(col.letter))
          return false;
        break;
      case 'A330 88Y':
        if (row.number >= 34 && col.letter === 'E')
          return false;
        break;
      case 'A330 F-UJCU':
        if (row.number >= 35 && col.letter === 'E')
          return false;
        break;
    }
    return true;
  }

  /**
   * Vérification si le siège / cellule doit être affiché en grand (large)
   * Typiquement pour les sièges en première classe sur certains avions
   * @param col
   * @param row
   * @param aircraft
   * @private
   */
  private _isCellLarge(col, row, aircraft: Aircraft): boolean {
    if (col.header) {
      return false;
    }
    switch (aircraft.short_name) {
      case 'A330 F-UJCS':
        if (row.number < 5 || row.hide?.length > 0)
          return true;
        break;
      case 'A330 F-UJCT':
        if ((row.number < 9 && !(['A', 'C', 'J', 'K'].includes(col.letter) && (row.number === 2 || row.number === 3))))
          return true;
        if (row.hide && ['C', 'F', 'G', 'J'].sort().join(',') === row.hide.sort().join(','))
          return true;
        break;
    }
    return false;
  }

  /**
   * Renvoi les classes CSS associées aux rangs
   * @param pax
   * @private
   */
  private _getRankClassName(pax: Pax): string {
    if (!pax) return ' seat-cell--no-pax';
    if (!pax.rank) return ' seat-cell--no-rank';
    if (['ENFANT', 'MR', 'ME', 'BB', 'CIV'].includes(pax.rank.name)) {
      return ' bgCivilRanks';
    } else if (pax.rank.rank > 16) {
      return ' bgTroopRanks';
    } else if (pax.rank.rank > 14) {
      return ' bgJuniorSubOfficers';
    } else if (pax.rank.rank > 10) {
      return ' bgSeniorSubOfficers';
    } else if (pax.rank.rank > 8) {
      return ' bgJuniorOfficers';
    } else if (pax.rank.rank > 5) {
      return ' bgSeniorOfficers';
    } else if (pax.rank.rank > 0) {
      return ' bgGeneralOfficers';
    }
  }

  /**
   * Renvoi le texte à afficher sur le siège (icône, nom du PAX etc ...)
   * @param col
   * @param row
   * @param pax
   * @private
   */
  private _getCellText(col: string, row: number, pax: Pax): string {
    if (pax) {
      let icon = null;
      if (!pax) {
        icon = null;
      } else if (this._checkObservations(pax.observations, ['VIP'])) {
        icon = ` <svg class="icon" width="12" height="12" viewBox="0 0 12 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.00008 9.07419L9.60508 11.25L8.64841 7.14919L11.8334 4.39002L7.63925 4.03419L6.00008 0.166687L4.36091 4.03419L0.166748 4.39002L3.35175 7.14919L2.39508 11.25L6.00008 9.07419Z"
                          fill="black"/>
                </svg>`;
      } else if (this._checkObservations(pax.observations, ['Stratevac', 'Medevac', 'Evasan'])) {
        icon = `<svg class="icon" width="12" height="12" viewBox="0 0 12 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 2.5H8.3125V2.0625C8.31106 1.71485 8.17232 1.38184 7.92649 1.13601C7.68066 0.890183 7.34765 0.75144 7 0.75H4.375C4.02735 0.75144 3.69434 0.890183 3.44851 1.13601C3.20268 1.38184 3.06394 1.71485 3.0625 2.0625V2.5H0.875C0.642936 2.5 0.420376 2.59219 0.256282 2.75628C0.0921872 2.92038 0 3.14294 0 3.375V10.375C0 10.6071 0.0921872 10.8296 0.256282 10.9937C0.420376 11.1578 0.642936 11.25 0.875 11.25H10.5C10.7321 11.25 10.9546 11.1578 11.1187 10.9937C11.2828 10.8296 11.375 10.6071 11.375 10.375V3.375C11.375 3.14294 11.2828 2.92038 11.1187 2.75628C10.9546 2.59219 10.7321 2.5 10.5 2.5ZM3.9375 2.0625C3.9375 1.94647 3.98359 1.83519 4.06564 1.75314C4.14769 1.67109 4.25897 1.625 4.375 1.625H7C7.11603 1.625 7.22731 1.67109 7.30936 1.75314C7.39141 1.83519 7.4375 1.94647 7.4375 2.0625V2.5H3.9375V2.0625ZM10.5 10.375H0.875V3.375H10.5V10.375ZM7.65625 6.875C7.65625 6.99103 7.61016 7.10231 7.52811 7.18436C7.44606 7.26641 7.33478 7.3125 7.21875 7.3125H6.125V8.40625C6.125 8.52228 6.07891 8.63356 5.99686 8.71561C5.91481 8.79766 5.80353 8.84375 5.6875 8.84375C5.57147 8.84375 5.46019 8.79766 5.37814 8.71561C5.29609 8.63356 5.25 8.52228 5.25 8.40625V7.3125H4.15625C4.04022 7.3125 3.92894 7.26641 3.84689 7.18436C3.76484 7.10231 3.71875 6.99103 3.71875 6.875C3.71875 6.75897 3.76484 6.64769 3.84689 6.56564C3.92894 6.48359 4.04022 6.4375 4.15625 6.4375H5.25V5.34375C5.25 5.22772 5.29609 5.11644 5.37814 5.03439C5.46019 4.95234 5.57147 4.90625 5.6875 4.90625C5.80353 4.90625 5.91481 4.95234 5.99686 5.03439C6.07891 5.11644 6.125 5.22772 6.125 5.34375V6.4375H7.21875C7.33478 6.4375 7.44606 6.48359 7.52811 6.56564C7.61016 6.64769 7.65625 6.75897 7.65625 6.875Z"
                          fill="black"/>
                </svg>`;
      } else if (this._checkObservations(pax.observations, ['Convoyeur'])) {
        icon = `<svg class="icon" width="10" height="10" viewBox="0 0 10 10" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M7.1001 2.19998H9.2001C9.5851 2.19998 9.9001 2.51498 9.9001 2.89998V9.19998C9.9001 9.58498 9.5851 9.89998 9.2001 9.89998H0.800098C0.415098 9.89998 0.100098 9.58498 0.100098 9.19998V2.89998C0.100098 2.51498 0.415098 2.19998 0.800098 2.19998H2.9001V0.799976C2.9001 0.414976 3.2151 0.0999756 3.6001 0.0999756H6.4001C6.7851 0.0999756 7.1001 0.414976 7.1001 0.799976V2.19998ZM6.4001 0.799976H3.6001V2.19998H6.4001V0.799976Z"
                          fill="black"/>
                </svg>`;
      } else if ((pax.animals && pax.animals.length > 0) || this._checkObservations(pax.observations, ['Maitre-chien'])) {
        icon = `<svg class="icon" width="14" height="14" viewBox="0 0 14 14" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <circle cx="2.62508" cy="5.54165" r="1.45833" fill="black"/>
                    <circle cx="5.25008" cy="3.20833" r="1.45833" fill="black"/>
                    <circle cx="8.75008" cy="3.20833" r="1.45833" fill="black"/>
                    <circle cx="11.3751" cy="5.54165" r="1.45833" fill="black"/>
                    <path d="M10.115 8.66833C9.60754 8.07333 9.18171 7.56583 8.66838 6.97083C8.40004 6.65583 8.05588 6.34083 7.64754 6.20083C7.58338 6.1775 7.51921 6.16 7.45504 6.14833C7.30921 6.125 7.15171 6.125 7.00004 6.125C6.84838 6.125 6.69088 6.125 6.53921 6.15417C6.47504 6.16583 6.41088 6.18333 6.34671 6.20667C5.93838 6.34667 5.60004 6.66167 5.32588 6.97667C4.81838 7.57167 4.39254 8.07917 3.87921 8.67417C3.11504 9.43833 2.17588 10.2842 2.35088 11.4683C2.52004 12.0633 2.94588 12.6525 3.71004 12.8217C4.13588 12.9092 5.49504 12.565 6.94171 12.565H7.04671C8.49338 12.565 9.85254 12.9033 10.2784 12.8217C11.0425 12.6525 11.4684 12.0575 11.6375 11.4683C11.8184 10.2783 10.8792 9.4325 10.115 8.66833Z"
                          fill="black"/>
                </svg>`;
      } else if (pax.underage) {
        icon = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="black">
                  <g id="surface1">
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 9.1875 6.125 C 9.1875 6.527344 8.859375 6.855469 8.457031 6.855469 C 8.054688 6.855469 7.730469 6.527344 7.730469 6.125 C 7.730469 5.722656 8.054688 5.394531 8.457031 5.394531 C 8.859375 5.394531 9.1875 5.722656 9.1875 6.125 Z M 9.1875 6.125 "/>
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 6.269531 6.125 C 6.269531 6.527344 5.945312 6.855469 5.542969 6.855469 C 5.140625 6.855469 4.8125 6.527344 4.8125 6.125 C 4.8125 5.722656 5.140625 5.394531 5.542969 5.394531 C 5.945312 5.394531 6.269531 5.722656 6.269531 6.125 Z M 6.269531 6.125 "/>
                  <path style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;" d="M 13.382812 6.613281 C 13.234375 5.734375 12.589844 5.015625 11.742188 4.765625 C 11.433594 4.113281 10.996094 3.539062 10.464844 3.066406 C 9.542969 2.246094 8.328125 1.75 7 1.75 C 5.671875 1.75 4.457031 2.246094 3.535156 3.066406 C 3 3.539062 2.5625 4.117188 2.257812 4.765625 C 1.410156 5.015625 0.765625 5.726562 0.617188 6.613281 C 0.59375 6.738281 0.582031 6.867188 0.582031 7 C 0.582031 7.132812 0.59375 7.261719 0.617188 7.386719 C 0.765625 8.265625 1.410156 8.984375 2.257812 9.234375 C 2.5625 9.882812 3 10.453125 3.523438 10.921875 C 4.445312 11.75 5.664062 12.25 7 12.25 C 8.335938 12.25 9.554688 11.75 10.484375 10.921875 C 11.007812 10.453125 11.445312 9.875 11.75 9.234375 C 12.589844 8.984375 13.234375 8.273438 13.382812 7.386719 C 13.40625 7.261719 13.417969 7.132812 13.417969 7 C 13.417969 6.867188 13.40625 6.738281 13.382812 6.613281 Z M 11.082031 8.167969 C 11.023438 8.167969 10.972656 8.15625 10.914062 8.148438 C 10.796875 8.539062 10.628906 8.902344 10.414062 9.234375 C 9.683594 10.347656 8.429688 11.082031 7 11.082031 C 5.570312 11.082031 4.316406 10.347656 3.585938 9.234375 C 3.371094 8.902344 3.203125 8.539062 3.085938 8.148438 C 3.027344 8.15625 2.976562 8.167969 2.917969 8.167969 C 2.273438 8.167969 1.75 7.640625 1.75 7 C 1.75 6.359375 2.273438 5.832031 2.917969 5.832031 C 2.976562 5.832031 3.027344 5.84375 3.085938 5.851562 C 3.203125 5.460938 3.371094 5.097656 3.585938 4.765625 C 4.316406 3.652344 5.570312 2.917969 7 2.917969 C 8.429688 2.917969 9.683594 3.652344 10.414062 4.765625 C 10.628906 5.097656 10.796875 5.460938 10.914062 5.851562 C 10.972656 5.84375 11.023438 5.832031 11.082031 5.832031 C 11.726562 5.832031 12.25 6.359375 12.25 7 C 12.25 7.640625 11.726562 8.167969 11.082031 8.167969 Z M 7 9.917969 C 8.171875 9.917969 9.183594 9.199219 9.625 8.167969 L 4.375 8.167969 C 4.816406 9.199219 5.828125 9.917969 7 9.917969 Z M 7 9.917969 "/>
                  </g>
                </svg>`;
      }
      let name = `<div> ${pax.rank ? `<span class="seat-cell--rank">${pax.rank.name}</span><br/>` : ''}${pax.last_name ? `<span class="seat-cell--name">${pax.last_name}</span></div>` : 'INCONNU'}`;
      return icon ? `<div class="seat-cell--icon">${icon}</div> ${name}` : name;
    }
    return `${row ? row : ''}${col ? col : ''}`;
  }

  /**
   * Vérification si il y a un commentaire sur le siège
   * @param observations
   * @param types
   * @private
   */
  private _checkObservations(observations, types): boolean {
    return types.findIndex(type => {
      const obs = Utils.removeDiacritics(observations)?.replace(/[^a-zA-Z]/g, "").trim().toLowerCase();
      return obs ? obs.includes(type.trim().toLowerCase()) : false;
    }) >= 0;
  }
}
