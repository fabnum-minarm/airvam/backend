import { IsNotEmpty, IsObject, IsOptional, IsString } from "class-validator";
import { LegDto } from "../../leg/dto/leg.dto";

export class LegDocumentDto {
  @IsNotEmpty()
  id: number;

  @IsObject()
  @IsNotEmpty()
  leg: LegDto;

  @IsString()
  @IsOptional()
  paxManifest: string;

  @IsString()
  @IsOptional()
  animalManifest: string;

  @IsString()
  @IsOptional()
  workSheet: string;

  @IsString()
  @IsOptional()
  tickets: string;

  @IsString()
  @IsOptional()
  seats: string;

  @IsString()
  @IsOptional()
  cover: string;

  @IsString()
  @IsOptional()
  pnl: string;

  @IsString()
  @IsOptional()
  tm5: string;

  @IsString()
  @IsOptional()
  paxManifestLmc: string;

  @IsString()
  @IsOptional()
  animalManifestLmc: string;

  @IsString()
  @IsOptional()
  workSheetLmc: string;

  @IsString()
  @IsOptional()
  ticketsLmc: string;

  @IsString()
  @IsOptional()
  pnlLmc: string;

  @IsString()
  @IsOptional()
  tm5Lmc: string;
}

const LegDocumentDocuments = ['pax_manifest', 'animal_manifest', 'work_sheet', 'pax_xlsx', 'tm5', 'tickets', 'pnl', 'cover', 'seats'];
const LegDocumentDocumentsLmc = ['pax_manifest_lmc', 'animal_manifest_lmc', 'work_sheet_lmc', 'tickets_lmc', 'tm5_lmc', 'pnl_lmc'];
export { LegDocumentDocuments, LegDocumentDocumentsLmc };
