import { forwardRef, Module } from '@nestjs/common';
import { LegDocumentService } from './leg-document.service';
import { LegDocumentController } from './leg-document.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { LegDocument } from "./entities/leg-document.entity";
import { PaxModule } from "../pax/pax.module";
import { PaxManifestModule } from "../pax-manifest/pax-manifest.module";
import { DatabaseFileModule } from "../database-file/database-file.module";
import { FlightModule } from "../flight/flight.module";
import { AnimalModule } from "../animal/animal.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([LegDocument]),
    forwardRef(() => PaxModule),
    PaxManifestModule,
    DatabaseFileModule,
    forwardRef(() => FlightModule),
    forwardRef(() => AnimalModule)
  ],
  controllers: [LegDocumentController],
  providers: [LegDocumentService],
  exports: [LegDocumentService]
})
export class LegDocumentModule {}
