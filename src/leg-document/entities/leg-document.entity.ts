import { Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Leg } from "../../leg/entities/leg.entity";
import { DatabaseFile } from "../../database-file/entities/database-file.entity";

@Entity('leg_document')
export class LegDocument {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type => Leg, leg => leg.documents, {onDelete: 'CASCADE'})
  @JoinColumn()
  leg: Leg;

  @OneToOne(type => DatabaseFile, file => file.leg_document_pax_manifest, {onDelete: 'CASCADE'})
  @JoinColumn()
  pax_manifest: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_animal_manifest, {onDelete: 'CASCADE'})
  @JoinColumn()
  animal_manifest: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_work_sheet, {onDelete: 'CASCADE'})
  @JoinColumn()
  work_sheet: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_pax_xlsx, {onDelete: 'CASCADE'})
  @JoinColumn()
  pax_xlsx: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_tickets, {onDelete: 'CASCADE'})
  @JoinColumn()
  tickets: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_seats, {onDelete: 'CASCADE'})
  @JoinColumn()
  seats: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_cover, {onDelete: 'CASCADE'})
  @JoinColumn()
  cover: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_pnl, {onDelete: 'CASCADE'})
  @JoinColumn()
  pnl: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_tm5, {onDelete: 'CASCADE'})
  @JoinColumn()
  tm5: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_pax_manifest_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  pax_manifest_lmc: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_animal_manifest_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  animal_manifest_lmc: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_work_sheet_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  work_sheet_lmc: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_tickets_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  tickets_lmc: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_pnl_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  pnl_lmc: DatabaseFile;

  @OneToOne(type => DatabaseFile, file => file.leg_document_tm5_lmc, {onDelete: 'CASCADE'})
  @JoinColumn()
  tm5_lmc: DatabaseFile;
}
