import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Inject,
  forwardRef
} from '@nestjs/common';
import { FlightService } from './flight.service';
import { CreateFlightDto } from './dto/create-flight.dto';
import { UpdateFlightDto } from './dto/update-flight.dto';
import { plainToInstance } from "class-transformer";
import { Flight } from "./entities/flight.entity";
import camelcaseKeys = require("camelcase-keys");
import { ApiOperation } from "@nestjs/swagger";
import { FlightDto } from "./dto/flight.dto";
import { AuthGuard } from "@nestjs/passport";
import { PaginationQueryDto } from "../core/dto/pagination-query.dto";
import { Pagination } from "nestjs-typeorm-paginate";
import { FlightFilterDto } from "./dto/flight-filter.dto";
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import { FlightHistoricService } from "../flight-historic/flight-historic.service";
import { Dev } from "../core/decorators/dev.decorator";
import { DeleteResult } from "typeorm";
import { FlightGuard } from "../core/guards/flight.guard";
import { FlightArchived } from "../core/decorators/flight-archived.decorator";
import { FlightDocumentDto } from "../flight-document/dto/flight-document.dto";
const snakecaseKeys = require('snakecase-keys');

@Controller('flight')
export class FlightController {
  constructor(private readonly _flightService: FlightService,
              @Inject(forwardRef(() => FlightHistoricService))
              private _flightHistoricService: FlightHistoricService) {
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({summary: 'Création d\'un vol'})
  async create(@Body() createFlightDto: CreateFlightDto,
               @AuthUser() user: UserDto) {
    const flight = await this._flightService.create(plainToInstance(Flight, snakecaseKeys(createFlightDto)), user);
    await this._flightHistoricService.insert(flight, user.email, `Création du vol`);
    return plainToInstance(FlightDto, camelcaseKeys(flight, {deep: true}));
  }

  @Post('search')
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({summary: 'Retourne les vols paginés'})
  async getFlightsPagination(@Query() options: PaginationQueryDto,
                             @Body() filters: FlightFilterDto,
                             @AuthUser() user: UserDto): Promise<Pagination<FlightDto>> {
    const flights: Pagination<Flight> = await this._flightService.paginate(options, filters, user);
    flights.items.filter(f => !!f).map(f => plainToInstance(FlightDto, camelcaseKeys(f, {deep: true})));
    // @ts-ignore
    return camelcaseKeys(flights, {deep: true});
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'), FlightGuard)
  @ApiOperation({summary: 'Retourne un vol spécifique'})
  async findOne(@Param('id') id: string,
                @AuthUser() user: UserDto): Promise<FlightDto> {
    const flight: Flight = await this._flightService.findOne(+id);
    return plainToInstance(FlightDto, camelcaseKeys(flight, {deep: true}));
  }

  @Get(':id/documents')
  @ApiOperation({summary: 'Retourne tout les documents d\'un vol'})
  async getDocuments(@Param('id') flightId: string): Promise<FlightDocumentDto> {
    const flightDocuments = await this._flightService.getDocuments(+flightId);
    return plainToInstance(FlightDocumentDto, camelcaseKeys(flightDocuments, {deep: true}));
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'), FlightGuard)
  @FlightArchived(false)
  @ApiOperation({summary: 'Mise à jour d\'un vol'})
  async update(@Param('id') id: string,
               @Body() updateFlightDto: UpdateFlightDto,
               @AuthUser() user: UserDto) {
    const flight = await this._flightService.update(+id, plainToInstance(Flight, snakecaseKeys(updateFlightDto)), user);
    await this._flightHistoricService.insert(flight, user.email, `Modification du vol ${flight.id}, ${flight.cotam_number}, ${flight.mission_name}, ${flight.mission_number}`);
    return plainToInstance(FlightDto, camelcaseKeys(flight, {deep: true}));
  }

  @Patch(':id/activate')
  @UseGuards(AuthGuard('jwt'), FlightGuard)
  @ApiOperation({summary: 'Désarchivage d\'un vol'})
  async activate(@Param('id') id: string,
                 @AuthUser() user: UserDto): Promise<FlightDto> {
    await this._flightService.enable(+id);
    const flight = await this._flightService.findOneLight(+id);
    await this._flightHistoricService.insert(flight, user.email, `Désarchivage du vol ${flight.id}, ${flight.cotam_number}, ${flight.mission_name}, ${flight.mission_number}`);
    return plainToInstance(FlightDto, camelcaseKeys(flight, {deep: true}));
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'), FlightGuard)
  @ApiOperation({summary: 'Archivage d\'un vol'})
  async remove(@Param('id') id: string,
               @AuthUser() user: UserDto): Promise<FlightDto> {
    await this._flightService.remove(+id);
    const flight = await this._flightService.findOneLight(+id);
    await this._flightHistoricService.insert(flight, user.email, `Archivage du vol ${flight.id}, ${flight.cotam_number}, ${flight.mission_name}, ${flight.mission_number}`);
    return plainToInstance(FlightDto, camelcaseKeys(flight, {deep: true}));
  }

  @Post('populateTestData')
  @Dev()
  @ApiOperation({summary: 'Génération des données nécessaires pour les tests'})
  async populateTestData(): Promise<void> {
    return this._flightService.populateTestData();
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._flightService.removeTestData();
  }
}
