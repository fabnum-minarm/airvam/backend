import {
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  ValidateIf
} from "class-validator";
import { CreateAircraftDto } from "../../aircraft/dto/create-aircraft.dto";
import { CreateLegDto } from "../../leg/dto/create-leg.dto";

export class CreateFlightDto {
  @IsString()
  @IsNotEmpty()
  missionName: string;

  @IsString()
  @IsNotEmpty()
  missionNumber: string;

  @IsString()
  @IsNotEmpty()
  cotamNumber: string;

  @ValidateIf(o => !o.aircraft)
  @IsString()
  @IsNotEmpty()
  aircraftName: string;

  @ValidateIf(o => !o.aircraft)
  @IsString()
  @IsNotEmpty()
  aircraftId: string;

  @IsObject()
  @IsOptional()
  aircraft: CreateAircraftDto;

  @IsArray()
  @IsNotEmpty()
  legs: CreateLegDto[];

  @IsString()
  @IsOptional()
  legUser: string;

  @IsBoolean()
  @IsOptional()
  freeSeating: boolean;

  @IsBoolean()
  @IsOptional()
  imperialSystem: boolean;
}
