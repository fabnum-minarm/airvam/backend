import { IsOptional } from "class-validator";

export class FlightFilterDto {
  @IsOptional()
  archived: boolean;

  @IsOptional()
  airports: number[];
}
