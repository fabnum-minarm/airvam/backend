import { IsObject, IsOptional } from "class-validator";
import { CreateFlightDto } from "./create-flight.dto";
import { User } from "../../user/entities/user.entity";

export class FlightDto extends CreateFlightDto {
  @IsOptional()
  id: number;

  @IsOptional()
  archived: boolean;

  @IsObject()
  @IsOptional()
  regulatorUser: User;

  @IsOptional()
  status: 'inProgess' | 'finish';
}
