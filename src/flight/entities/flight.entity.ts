import { Column, Entity, Index, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Aircraft } from "../../aircraft/entities/aircraft.entity";
import { Leg } from "../../leg/entities/leg.entity";
import { FlightHistoric } from "../../flight-historic/entities/flight-historic.entity";
import { User } from "../../user/entities/user.entity";
import { Animal } from "../../animal/entities/animal.entity";
import { FlightDocument } from "../../flight-document/entities/flight-document.entity";

@Entity('flight')
export class Flight {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false, length: 255})
  mission_name: string;

  @Column({nullable: false, length: 255})
  mission_number: string;

  @Column({nullable: false, length: 255})
  cotam_number: string;

  @Column({nullable: true, length: 255})
  aircraft_name: string;

  @Column({nullable: true, length: 255})
  aircraft_id: string;

  @Column({default: false})
  imperial_system: boolean;

  @ManyToOne(type => Aircraft, aircraft => aircraft.flights, {
    eager: true,
  })
  @Index('flight_aircraftId_index')
  aircraft: Aircraft;

  @ManyToOne(type => User, user => user.regulator_flights, {onUpdate: 'CASCADE'})
  regulator_user: User;

  @Column({nullable: true, length: 255})
  leg_user: string;

  @OneToMany(type => Leg, leg => leg.flight, {
    eager: true,
  })
  legs: Leg[];

  @OneToMany(type => FlightHistoric, flightHistoric => flightHistoric.flight)
  historic: FlightHistoric[];

  @OneToMany(type => Animal, animal => animal.flight)
  animals: Animal[];

  @Column({default: false})
  archived: boolean;

  @Column({type: "timestamp", nullable: true})
  archived_date: Date;

  @OneToOne(type => FlightDocument, document => document.flight)
  documents: FlightDocument;

  static getAttributesToSearch() {
    return ['mission_name', 'mission_number', 'cotam_number'];
  }
}
