import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { Flight } from "./entities/flight.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, In, LessThan, Like, Repository, SelectQueryBuilder } from "typeorm";
import { LegService } from "../leg/leg.service";
import { PaginationQueryDto } from "../core/dto/pagination-query.dto";
import { paginate, Pagination } from "nestjs-typeorm-paginate";
import { PaginationUtils } from "../core/pagination-utils";
import { FlightFilterDto } from "./dto/flight-filter.dto";
import { UserDto } from "../user/dto/user.dto";
import { PaxService } from "../pax/pax.service";
import { AnimalService } from "../animal/animal.service";
import { SeatService } from "../seat/seat.service";
import { LegStatus } from "../leg/enums/leg-status.enum";
import { FlightDocumentService } from "../flight-document/flight-document.service";
import { Leg } from "../leg/entities/leg.entity";
import { FinalizeDto } from "../leg/dto/finalize.dto";
import { LegDocumentService } from "../leg-document/leg-document.service";
import { UserRole } from "../user/entities/user.entity";
import { Cron, CronExpression } from "@nestjs/schedule";
import { AirvamLogger } from "../logger/airvam.logger";
import { DatabaseFileService } from "../database-file/database-file.service";
import { LegDocumentDocuments, LegDocumentDocumentsLmc } from "../leg-document/dto/leg-document.dto";
import { FlightDocument } from "../flight-document/entities/flight-document.entity";
import { testFlights } from "../core/test/data";
import { AirportService } from "../airport/airport.service";

const moment = require('moment');

@Injectable()
export class FlightService {
  private readonly _logger = new AirvamLogger('FlightService');
  private _documents = ['cover'];
  private _legDocuments = LegDocumentDocuments.concat(LegDocumentDocumentsLmc);


  constructor(@InjectRepository(Flight)
              private readonly _flightsRepository: Repository<Flight>,
              @Inject(forwardRef(() => PaxService))
              private _paxService: PaxService,
              @Inject(forwardRef(() => AnimalService))
              private _animalService: AnimalService,
              private _seatService: SeatService,
              @Inject(forwardRef(() => LegService))
              private _legService: LegService,
              @Inject(forwardRef(() => LegDocumentService))
              private _legDocumentService: LegDocumentService,
              private _flightDocumentService: FlightDocumentService,
              private _databaseFileService: DatabaseFileService,
              private _airportService: AirportService) {
  }

  /**
   * Création d'un vol
   * @param flight
   * @param authUser
   * @param oldFlight
   */
  async create(flight: Flight, authUser: UserDto, oldFlight?: Flight): Promise<Flight> {
    // @ts-ignore
    flight.regulator_user = {email: authUser?.email};
    const isNewFlight = !flight.id;
    const flightUpdated = await this._flightsRepository.save(this._cleanFlight(flight));

    const legs: Leg[] = await this._legService.createUpdate(flight.legs, flightUpdated);
    // On génère des plans cabines vierges si c'est un nouveau vol ou si l'avion a changé
    if ((isNewFlight && flight.aircraft)
      || (oldFlight && oldFlight.aircraft?.name !== flight.aircraft?.name)) {
      const promises = [];
      legs.forEach(l => {
        promises.push(this._legService.autoPlacement(l.id, null, true));
      })
      await Promise.all(promises);
    }
    return this.findOne(flightUpdated.id);
  }

  /**
   * Récupération du listing des vols paginé
   * @param options
   * @param filters
   * @param authUser
   */
  async paginate(options: PaginationQueryDto, filters: FlightFilterDto, authUser: UserDto): Promise<Pagination<Flight>> {
    filters.airports = authUser ? authUser.authorizedAirports : null;
    const results = await paginate<Flight>(
      this._getFlightQueryBuilder(PaginationUtils.setQuery(options, Flight.getAttributesToSearch()), filters, authUser),
      options
    );

    // Obligé de faire ça pour la pagination quand il y a des left join
    return new Pagination(
      // @ts-ignore
      await Promise.all(results.items.map(async (item: Flight) => {
        item.legs = await this._legService.findByFlight(item.id);
        // @ts-ignore
        item.status = this._computeStatus(item);

        return item;
      })),
      results.meta,
      results.links,
    );
  }

  /**
   * Récupération d'un objet flight sans aucune relation
   * @param id
   */
  findOneUltraLight(id: number): Promise<Flight> {
    return this._flightsRepository.createQueryBuilder('flight')
      .where({id: id})
      .getOne();
  }

  /**
   * Récupération d'un objet flight avec les relations principales
   * @param id
   */
  findOneLight(id: number): Promise<Flight> {
    return this._flightsRepository.createQueryBuilder('flight')
      .leftJoinAndSelect('flight.aircraft', 'aircraft')
      .leftJoinAndSelect('flight.legs', 'legs')
      .leftJoinAndSelect('legs.departure', 'departure')
      .leftJoinAndSelect('legs.arrival', 'arrival')
      .where({id: id})
      .orderBy('legs.id', 'ASC')
      .getOne();
  }

  /**
   * Récupération d'un objet flight avec toutes ses relations
   * @param id
   */
  findOne(id: number): Promise<Flight> {
    const query = this._flightsRepository.createQueryBuilder('flight')
      .select(['flight.id', 'flight.mission_name', 'flight.mission_number', 'flight.cotam_number',
        'flight.aircraft_name', 'flight.aircraft_id', 'flight.imperial_system', 'flight.leg_user', 'flight.archived'])
      .leftJoinAndSelect('flight.aircraft', 'aircraft')
      .leftJoinAndSelect('flight.regulator_user', 'regulator_user')
      .leftJoinAndSelect('flight.documents', 'documents')
      .leftJoinAndSelect('flight.legs', 'legs')
      .leftJoinAndSelect('legs.departure', 'departure')
      .leftJoinAndSelect('legs.arrival', 'arrival')
      .leftJoinAndSelect('legs.documents', 'legs_documents')
      .where({id: id})
      .orderBy('legs.id', 'ASC');

    this._documents.forEach(d => {
      query.leftJoin(`documents.${d}`, `${d}_flight`);
      query.addSelect([`${d}_flight.id`, `${d}_flight.filename`]);
    });
    this._legDocuments.forEach(d => {
      query.leftJoin(`legs_documents.${d}`, `${d}_leg`);
      query.addSelect([`${d}_leg.id`, `${d}_leg.filename`]);
    });

    return query.getOne();
  }

  /**
   * Récupération de la liste de documents d'un vol
   * @param flightId
   */
  async getDocuments(flightId: number): Promise<FlightDocument> {
    const query = this._flightsRepository.createQueryBuilder('flight')
      .leftJoinAndSelect('flight.documents', 'documents')
      .where({id: flightId});

    this._documents.forEach(d => {
      query.leftJoin(`documents.${d}`, `${d}_flight`);
      query.addSelect([`${d}_flight.id`, `${d}_flight.filename`]);
    });

    return (await query.getOne()).documents;
  }

  /**
   * Mise à jour d'un vol
   * @param id
   * @param flight
   * @param authUser
   */
  async update(id: number, flight: Flight, authUser: UserDto): Promise<Flight> {
    flight.id = id;
    const originalFlight = await this.findOneLight(id);
    const flightToReturn = await this.create(this._cleanFlight(flight), authUser, originalFlight);
    await this._checkResetData(originalFlight, flightToReturn);
    return flightToReturn;
  }

  /**
   * Désarchivage d'un vol
   * @param id
   */
  async enable(id: number): Promise<void> {
    await this._flightsRepository.update({id: id}, {archived: false, archived_date: null});
  }

  /**
   * Archivage d'un vol
   * @param id
   */
  async remove(id: number): Promise<void> {
    await this._flightsRepository.update({id: id}, {archived: true, archived_date: new Date()});
  }

  /**
   * Finalisation d'un vol si tous ses legs sont finalisés
   * @param id
   * @param finalizeDto
   */
  async finalize(id: number, finalizeDto: FinalizeDto): Promise<Flight> {
    const flight = await this.findOne(id);
    for (let i = 0; i < flight.legs.length; i++) {
      if (flight.legs[i].status !== LegStatus.finalized) {
        return;
      }
    }
    await this._flightDocumentService.resetDocuments(flight.id);
    const paxs = await this._paxService.findByFlight(flight.id, true);
    flight.documents = await this._flightDocumentService.generateDocuments(flight, paxs, finalizeDto);
    return flight;
  }

  /**
   * Vérification si un utilisateur a les droits de consultation / édition sur un vol
   * @param flightId
   * @param authUser
   * @param flightArchived
   */
  async checkUserAccess(flightId: number, authUser: UserDto, flightArchived: boolean): Promise<boolean> {
    if (authUser && authUser.role === UserRole.god) {
      return true;
    }
    const airports = authUser ? authUser.authorizedAirports : null;
    let whereClause = '';
    if (airports && airports.length > 0) {
      whereClause = `("legs"."departureId" IN (${airports}) OR "legs"."arrivalId" IN (${airports}))`;
    } else if (authUser) {
      return false;
    }
    const query = this._flightsRepository.createQueryBuilder('flight')
      .leftJoin('flight.legs', 'legs')
      .where(`"flight"."id" = ${flightId}`)
      .andWhere(whereClause);
    if (flightArchived !== undefined && flightArchived !== null) {
      query.andWhere(`"flight"."archived" = ${flightArchived}`);
    }
    return (await this._flightsRepository.query(`SELECT EXISTS(${query.getQuery()}) AS "exists"`))[0]?.exists;
  }

  /**
   * Suppression des vols qui sont archivés depuis plus de 6 mois
   */
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async cleanData() {
    const expirationDate = new Date();
    expirationDate.setMonth(expirationDate.getMonth() - 6);
    const query = this._flightsRepository.createQueryBuilder('flight')
      .leftJoinAndSelect('flight.documents', 'documents')
      .leftJoinAndSelect('flight.legs', 'legs')
      .leftJoinAndSelect('legs.documents', 'legs_documents')
      .where({
        archived_date: LessThan(expirationDate)
      });
    this._documents.forEach(d => {
      query.leftJoin(`documents.${d}`, `${d}_flight`);
      query.addSelect([`${d}_flight.id`, `${d}_flight.filename`]);
    });
    this._legDocuments.forEach(d => {
      query.leftJoin(`legs_documents.${d}`, `${d}_leg`);
      query.addSelect([`${d}_leg.id`, `${d}_leg.filename`]);
    });
    const flightsToDelete: Flight[] = await query.getMany();
    const promises = [];
    flightsToDelete.forEach(f => {
      promises.push(this._flightDocumentService.resetDocuments(f.id));
      f.legs.forEach(l => {
        promises.push(this._legDocumentService.resetDocuments(f.id, l.id, false));
      })
    });
    await Promise.all(promises);
    const deletedResult = await this._flightsRepository.delete({id: In(flightsToDelete.map(f => f.id))})
    if (deletedResult.affected > 0) {
      this._logger.log(`Suppression de ${deletedResult.affected} vols archivés datant de plus de 6 mois.`);
    }
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Retourne l'objet query builder d'un vol avec certaines de ses relations
   * @param whereClause
   * @param filters
   * @param authUser
   * @private
   */
  private _getFlightQueryBuilder(whereClause: string, filters: FlightFilterDto, authUser?: UserDto): SelectQueryBuilder<Flight> {
    const query = this._flightsRepository
      .createQueryBuilder('flight')
      .select(['flight.id', 'flight.mission_name', 'flight.mission_number', 'flight.cotam_number'])
      .leftJoinAndSelect('flight.aircraft', 'aircraft')
      .andWhere(!!whereClause ? whereClause.toString() : `'1'`)
      .andWhere(`flight.archived = ${filters.archived}`)
      .orderBy({
        'flight.id': 'ASC'
      });

    if (filters.airports && filters.airports.length > 0) {
      query.leftJoin('flight.legs', 'leg');
      query.leftJoin('leg.departure', 'departure');
      query.leftJoin('leg.arrival', 'arrival');
      query.andWhere(`("departure"."id" IN (:...ids) OR "arrival"."id" IN (:...ids))`, {ids: filters.airports});
    } else if (authUser && authUser.role !== UserRole.god) {
      query.andWhere('flight.id < 0');
    }

    return query;
  }

  /**
   * Formatage de l'objet flight avant sa création / édition
   * @param flight
   * @private
   */
  private _cleanFlight(flight: Flight): Flight {
    if (flight.aircraft) {
      flight.aircraft_id = null;
      flight.aircraft_name = null;
    }
    return flight;
  }

  /**
   * Reset de certaines données lors de l'édition d'un vol
   * @param originalFlight
   * @param newFlight
   * @private
   */
  private async _checkResetData(originalFlight: Flight, newFlight: Flight) {
    // Si l'avion change, on reset les plans cabines
    // Si un leg est supprimé on reset les sièges / paxs / animaux
    let resetPlanCab = originalFlight.aircraft?.name !== newFlight.aircraft?.name;
    let isFreeSeating = !newFlight.aircraft;
    const legsUpdated = [];
    originalFlight.legs.forEach(l => {
      // Leg supprimé
      if (newFlight.legs.findIndex(newLeg => newLeg.id === l.id) < 0) {
        legsUpdated.push(l.id);
      } else {
        const newLeg = newFlight.legs.find(ol => ol.id === l.id);
        // Leg qui a un départ ou une arrivée modifié
        if (newLeg.departure.id !== l.departure.id || newLeg.arrival.id !== l.arrival.id) {
          legsUpdated.push(l.id);
        }
      }
    });
    // Si des PAXs sont associés aux legs modifiés, on reset la liste
    const legs = await this._legService.findLegsWithPaxs(legsUpdated);
    // @ts-ignore
    const resetPax = legs.findIndex(l => l.paxNumber > 0) >= 0;
    const resetOnlyDocs = resetPlanCab || resetPax ? false : !this._areFlightEquals(originalFlight, newFlight);
    const legsWithPaxs = await this._legService.findLegsWithPaxs(newFlight.legs.map(l => l.id));
    if (resetPlanCab || resetPax) {
      const promises = [];
      promises.push(this._flightDocumentService.resetDocuments(newFlight.id));
      newFlight.legs.forEach(l => {
        // @ts-ignore
        const isTherePax = legsWithPaxs.find(lp => lp.id === l.id)?.paxNumber > 0;
        // Si on ne reset pas les PAXs, on repasse le statut à incomplet si on est pas en freeseating, sinon à prêt si freeseating & paxs présents
        if (!resetPax) {
          if ((!isFreeSeating || !isTherePax) && l.status !== LegStatus.incomplete) {
            promises.push(this._legService.updateStatus(l.id, LegStatus.incomplete));
          } else if (isFreeSeating && isTherePax) {
            promises.push(this._legService.updateStatus(l.id, LegStatus.scheduled));
          }
        }
        promises.push(this._legDocumentService.resetDocuments(originalFlight.id, l.id, false));
        promises.push(this._legService.autoPlacement(l.id, null, true));
      });
      await Promise.all(promises);
    }
    if (resetPax) {
      await Promise.all([
        this._animalService.deleteByFlight(newFlight.id),
        this._paxService.deleteByFlight(newFlight.id),
        this._legDocumentService.resetDocumentsByFlight(originalFlight),
        this._flightDocumentService.resetDocuments(newFlight.id)
      ]);
    }
    if (resetOnlyDocs) {
      const promises = [];
      promises.push(this._flightDocumentService.resetDocuments(newFlight.id));
      newFlight.legs.forEach(l => {
        if ([LegStatus.lmc, LegStatus.finalized].includes(l.status)) {
          promises.push(this._legService.updateStatus(l.id, LegStatus.scheduled));
        }
        promises.push(this._legDocumentService.resetDocuments(originalFlight.id, l.id, false));
      });
      await Promise.all(promises);
    }
  }

  /**
   * Association d'un statut au vol suivant le statut de ses legs
   * @param flight
   * @private
   */
  private _computeStatus(flight: Flight): string {
    const now = moment();
    let status = null;
    flight.legs.forEach((l, idx) => {
      if (moment(l.departure_time).isBefore(now)) {
        status = (idx + 1 === flight.legs.length) ? 'finish' : 'inProgress';
      }
    });
    return status;
  }

  /**
   * Vérifivation des données principales entre deux objets flight
   * @param flight1
   * @param flight2
   * @private
   */
  private _areFlightEquals(flight1: Flight, flight2: Flight): boolean {
    const flightA = {
      mission_name: flight1.mission_name,
      mission_number: flight1.mission_number,
      cotam_number: flight1.cotam_number,
      aircraft_name: flight1.aircraft_name,
      aircraft_id: flight1.aircraft_id,
      legs: flight1.legs.map(l => {
        return {
          departure: l.departure,
          arrival: l.arrival,
          departure_time: l.departure_time
        }
      })
    };
    const flightB = {
      mission_name: flight2.mission_name,
      mission_number: flight2.mission_number,
      cotam_number: flight2.cotam_number,
      aircraft_name: flight2.aircraft_name,
      aircraft_id: flight2.aircraft_id,
      legs: flight2.legs.map(l => {
        return {
          departure: l.departure,
          arrival: l.arrival,
          departure_time: l.departure_time
        }
      })
    };

    return JSON.stringify(flightA) === JSON.stringify(flightB);
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Ajouts de vols pour les tests E2E
   */
  async populateTestData(): Promise<void> {
    const airports = await Promise.all([
      this._airportService.findOneWithClause({oaci: 'LFMI'}),
      this._airportService.findOneWithClause({oaci: 'LFPC'}),
      this._airportService.findOneWithClause({oaci: 'LFAT'}),
      this._airportService.findOneWithClause({oaci: 'CYAG'})
    ]);
    const archive = {archived: true, archived_date: new Date()};
    // @ts-ignore
    testFlights.forEach(async (flight: Flight) => {
      flight.legs.forEach(l => {
        l.departure = airports.find(a => a.oaci === l.departure.oaci);
        l.arrival = airports.find(a => a.oaci === l.arrival.oaci);
      });
      // @ts-ignore
      await this.create(JSON.parse(JSON.stringify(flight)), {email: 'dev-super-admin@email.com'});
      // @ts-ignore
      await this.create({...JSON.parse(JSON.stringify(flight)), ...archive}, {email: 'dev-super-admin@email.com'});
    });
    return;
  }

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._flightsRepository.delete({mission_name: Like('CYTEST%')});
  }
}
