import { forwardRef, Module } from '@nestjs/common';
import { FlightService } from './flight.service';
import { FlightController } from './flight.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Flight } from "./entities/flight.entity";
import { LegModule } from "../leg/leg.module";
import { FlightHistoricModule } from "../flight-historic/flight-historic.module";
import { PaxModule } from "../pax/pax.module";
import { AnimalModule } from "../animal/animal.module";
import { SeatModule } from "../seat/seat.module";
import { FlightDocumentModule } from "../flight-document/flight-document.module";
import { LegDocumentModule } from "../leg-document/leg-document.module";
import { DatabaseFileModule } from "../database-file/database-file.module";
import { AirportModule } from "../airport/airport.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Flight]),
    forwardRef(() => LegModule),
    forwardRef(() => FlightHistoricModule),
    forwardRef(() => PaxModule),
    forwardRef(() => AnimalModule),
    SeatModule,
    FlightDocumentModule,
    forwardRef(() => LegDocumentModule),
    DatabaseFileModule,
    AirportModule
  ],
  controllers: [FlightController],
  providers: [FlightService],
  exports: [FlightService]
})
export class FlightModule {}
