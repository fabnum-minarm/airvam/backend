import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { In, Repository } from "typeorm";
import { Seat } from "./entities/seat.entity";
import { UpdateResult } from "typeorm/query-builder/result/UpdateResult";
import { LegService } from "../leg/leg.service";
import { Leg } from "../leg/entities/leg.entity";
import { UserDto } from "../user/dto/user.dto";
import { Pax } from "../pax/entities/pax.entity";
import { LegStatus } from "../leg/enums/leg-status.enum";
import { UserRole } from "../user/entities/user.entity";
import { PaxManifestService } from "../pax-manifest/pax-manifest.service";

@Injectable()
export class SeatService {
  constructor(@InjectRepository(Seat)
              private readonly _seatsRepository: Repository<Seat>,
              @Inject(forwardRef(() => LegService))
              private _legService: LegService,
              private _paxManifestService: PaxManifestService) {
  }

  /**
   * Création de sièges
   * @param seats
   */
  create(seats: Seat[]): Promise<Seat[]> {
    return this._seatsRepository.save(seats);
  }

  /**
   * Récupération des sièges d'un leg
   * @param legId
   * @param light
   */
  findByLeg(legId: number, light = false): Promise<Seat[]> {
    const query = this._seatsRepository.createQueryBuilder('seat')
      .leftJoin('seat.leg', 'leg')
      .leftJoinAndSelect('seat.pax', 'pax')
      .where({leg: {id: legId}});

    if (!light) {
      query.leftJoinAndSelect('pax.animals', 'animals')
        .leftJoinAndSelect('pax.rank', 'rank');
    }

    return query.getMany();
  }

  /**
   * Récupération d'un siège avec son PAX lié
   * @param id
   */
  findOne(id: number): Promise<Seat> {
    return this._seatsRepository.findOne({
      where: {id: id},
      relations: [
        'pax',
        'pax.rank'
      ]
    });
  }

  /**
   * Récupération de plusieurs sièges
   * @param ids
   */
  findSeveral(ids: number[]): Promise<Seat[]> {
    return this._seatsRepository.find({
      relations: [
        'pax',
        'pax.rank',
        'pax.animals'
      ],
      where: {id: In(ids)}
    });
  }

  /**
   * Suppression des sièges d'un leg
   * @param legId
   */
  deleteByLeg(legId: number) {
    return this._seatsRepository.createQueryBuilder('seat')
      .leftJoin('seat.leg', 'leg')
      .delete()
      .where('leg.id = :legId', {legId: legId})
      .execute();
  }

  /**
   * Suppression des sièges suivant des PAXs associés
   * @param paxIds
   */
  deleteByPaxIds(paxIds: number[]) {
    return this._seatsRepository.createQueryBuilder('seat')
      .delete()
      .where('seat."paxId" IN (:...paxIds)', {paxIds})
      .execute();
  }

  /**
   * Suppression de sièges suivant un leg et des PAXs associés
   * @param legId
   * @param paxIds
   */
  deleteByPaxAndLeg(legId: number, paxIds: number[]) {
    return this._seatsRepository.createQueryBuilder('seat')
      .delete()
      .where('seat."legId" = :legId', {legId: legId})
      .andWhere('seat."paxId" IN (:...paxIds)', {paxIds: paxIds})
      .execute();
  }

  /**
   * Ajout d'un commentaire sur un siège
   * @param seatId
   * @param observations
   */
  comment(seatId: number, observations: string): Promise<UpdateResult> {
    return this._seatsRepository.update({id: seatId}, {observations: observations});
  }

  /**
   * Blocage d'un siège
   * @param seatId
   */
  async lock(seatId: number): Promise<Seat[]> {
    const seat = await this.findOne(seatId);
    // Si il est déjà bloqué on ne fait rien
    if (seat.blocked) {
      return;
    }
    const leg: Leg = await this._legService.findBySeat(seatId, true);

    const paxNumber = leg.paxs.length;
    const seatsAvailables = leg.seats.filter(s => !s.blocked).length;
    // CHECKER SI ON PEUT BLOQUER UN SIEGE EN PLUS PAR RAPPORT AU NOMBRE DE PAX
    if (seatsAvailables <= paxNumber) {
      throw new HttpException(`Veuillez débloquer un siège afin de pouvoir en bloquer un à nouveau.`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    // Si il n'y a pas de dessus on peut le bloquer directement
    if (!seat.pax) {
      seat.blocked = true;
      return this._seatsRepository.save([seat]);
    }
    // SI OUI ET SIEGE NON VIDE, ON DECALE TOUT LE MONDE, ON BLOQUE
    const seatsToSave = this._shiftPlace(leg.seats, seat, true);
    return this.create(seatsToSave);
  }

  /**
   * Déblocage d'un siège
   * @param seatId
   */
  unlock(seatId: number) {
    return this._seatsRepository.update({id: seatId}, {blocked: false});
  }

  /**
   * Placement d'un PAX sur un siège
   * @param seatId
   * @param paxId
   */
  async place(seatId: number, paxId: number): Promise<Seat[]> {
    const leg: Leg = await this._legService.findBySeat(seatId, true);
    const seat = {...leg.seats.find(s => s.id === seatId)};
    const paxToPlace = leg.paxs.find(p => p.id === paxId);
    let oldSeat = {...leg.seats.find(s => s.pax?.id === paxId)};
    oldSeat = oldSeat.id ? oldSeat : null;
    if (!seat || seat.blocked || !paxToPlace) {
      return;
    }

    // Si il n'y a pas de pax sur le seat et qu'il n'est pas bloqué, on place et on remove le pax de l'ancien seat
    if (!seat.pax) {
      seat.pax = paxToPlace;
      seat.observations = oldSeat ? oldSeat.observations : null;
      const seatsToSave = [seat];
      if (oldSeat) {
        oldSeat.pax = null;
        oldSeat.observations = null;
        seatsToSave.unshift(oldSeat);
      }
      return this.create(seatsToSave);
    }
    // Si il y a un pax sur le seat, on vérifie qu'il y a assez de place pour le replacer
    const seatsAvailables = leg.seats.filter(s => !s.blocked).length;
    if (seatsAvailables <= 0 && !oldSeat) {
      throw new HttpException(`Veuillez débloquer un siège afin de pouvoir placer un passager.`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    // Si oui, on place le nouveau pax, on supprime son ancien siège, et on replace l'ancien
    const paxToReplace = {...seat.pax};
    const newSeat = leg.seats.find(s => s.id === seatId);
    newSeat.pax = paxToPlace;
    newSeat.observations = oldSeat ? oldSeat.observations : null;
    oldSeat ? leg.seats.find(s => s.id === oldSeat.id).pax = null : null;
    oldSeat ? leg.seats.find(s => s.id === oldSeat.id).observations = null : null;
    const seatsToSave = this._shiftPlace(leg.seats, seat, false, paxToReplace);
    if (oldSeat && seatsToSave.findIndex(s => s.id === oldSeat.id) < 0) {
      oldSeat.pax = null;
      oldSeat.observations = null;
      seatsToSave.unshift(oldSeat);
    }
    return this.create(seatsToSave);
  }

  /**
   * À la suite d'un placement d'un PAX, on vérifie si tout les PAXs ont été placés
   * Si oui on met à jour le statut du leg
   * @param seatId
   * @param seatsUpdated
   */
  async computeLegAndPaxs(seatId: number, seatsUpdated: Seat[]) {
    const leg: Leg = await this._legService.findBySeat(seatId, true);
    if (leg.paxs.length !== leg.seats.filter(s => !!s.pax).length) {
      return;
    }
    await this._legService.updateStatus(leg.id, LegStatus.scheduled);
    const legStatus: LegStatus = await this._legService.getStatus(leg.id);
    if (legStatus !== LegStatus.lmc) {
      return;
    }
    const paxsToUpdate: Pax[] = seatsUpdated.filter(s => !!s.pax).map(s => s.pax);
    await this._paxManifestService.lmc(paxsToUpdate, leg);
  }

  /**
   * Vérification si un utilisateur peux effectuer une action sur un siège (blocage, commentaire ...)
   * @param seatId
   * @param authUser
   * @param flightArchived
   * @param checkLegFlight
   */
  async checkUserAccess(seatId: number, authUser: UserDto, flightArchived: boolean, checkLegFlight?: boolean): Promise<boolean> {
    if (authUser && authUser.role === UserRole.god) {
      return true;
    }
    const airports = authUser ? authUser.authorizedAirports : null;
    let whereClause = '';
    if (airports && airports.length > 0) {
      whereClause = checkLegFlight ? `("legs"."departureId" IN (${airports}) OR "legs"."arrivalId" IN (${airports}))` :
        `("leg"."departureId" IN (${airports}) OR "leg"."arrivalId" IN (${airports}))`;
    } else if (authUser) {
      return false;
    }
    const query = this._seatsRepository.createQueryBuilder('seat')
      .leftJoin('seat.leg', 'leg')
      .leftJoin('leg.flight', 'flight')
      .where(`"seat"."id" = ${seatId}`)
      .andWhere(whereClause);
    if (flightArchived !== undefined && flightArchived !== null) {
      query.andWhere(`"flight"."archived" = ${flightArchived}`);
    }
    if (checkLegFlight) {
      query.leftJoin('flight.legs', 'legs');
    }
    return (await this._seatsRepository.query(`SELECT EXISTS(${query.getQuery()}) AS "exists"`))[0]?.exists;
  }

  /************************************************************************************ PRIVATE FUNCTIONS ************************************************************************************/

  /**
   * Placement d'un PAX sur un siège
   * Si celui-ci n'est pas libre on doit replacer le PAX qui est dessus
   * @param fullSeats
   * @param seatToShift
   * @param toBlock
   * @param paxToReplace
   * @private
   */
  private _shiftPlace(fullSeats: Seat[], seatToShift: Seat, toBlock: boolean, paxToReplace?: Pax): Seat[] {
    fullSeats = [...fullSeats];
    fullSeats.sort((a, b) => a.row.localeCompare(b.row));
    fullSeats.sort((a, b) => a.number - b.number);
    fullSeats.sort((a, b) => a.zone.localeCompare(b.zone));

    // CHECKER SI LA PLACE DISPONIBLE EST AVANT OU APRES
    const seatIndex = fullSeats.findIndex(s => s.id === seatToShift.id)
    if (seatIndex > fullSeats.findIndex((s, idx) => !s.blocked && !s.pax && idx > seatIndex)) {
      fullSeats.reverse();
    }

    let shift = false;
    let seatToReplace = null;
    let seatTmp = null;
    const seatsToSave: Seat[] = [];
    fullSeats.every((s, index) => {
      seatTmp = {...s};
      if (!s.blocked && shift) {
        s.pax = seatToReplace.pax;
        s.observations = seatToReplace.observations;
        seatsToSave.push(s);
        if (!seatTmp.pax) {
          return false;
        }
        seatToReplace = seatTmp;
      }
      if (s.id === seatToShift.id) {
        shift = true;
        if (toBlock) {
          s.blocked = true;
          s.pax = null;
        }
        seatsToSave.push(s);
        seatToReplace = seatTmp;
        if (paxToReplace) {
          seatToReplace.pax = paxToReplace;
          seatToReplace.observations = seatToShift.observations ? seatToShift.observations : '';
        }
      }
      return true;
    });
    return seatsToSave;
  }
}
