import { CreateSeatDto } from './create-seat.dto';
import { IsOptional } from "class-validator";

export class SeatDto extends CreateSeatDto {
  @IsOptional()
  id: number;
}
