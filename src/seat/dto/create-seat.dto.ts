import { IsEnum, IsNotEmpty, IsObject, IsOptional } from "class-validator";
import { SeatZone } from "../../seat-template/dto/seat-zone.enum";
import { Leg } from "../../leg/entities/leg.entity";
import { Pax } from "../../pax/entities/pax.entity";

export class CreateSeatDto {
  @IsEnum(SeatZone)
  @IsNotEmpty()
  zone: SeatZone;

  @IsNotEmpty()
  row: string;

  @IsNotEmpty()
  number: number;

  @IsObject()
  @IsNotEmpty()
  leg: Leg;

  @IsObject()
  @IsOptional()
  pax: Pax;

  @IsNotEmpty()
  disabled: boolean;

  @IsOptional()
  observations: string;
}
