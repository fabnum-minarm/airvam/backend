import { forwardRef, Module } from '@nestjs/common';
import { SeatService } from './seat.service';
import { SeatController } from './seat.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Seat } from "./entities/seat.entity";
import { LegModule } from "../leg/leg.module";
import { PaxManifestModule } from "../pax-manifest/pax-manifest.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([Seat]),
    forwardRef(() => LegModule),
    PaxManifestModule
  ],
  controllers: [SeatController],
  providers: [SeatService],
  exports: [SeatService]
})
export class SeatModule {}
