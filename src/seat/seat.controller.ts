import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { SeatService } from './seat.service';
import { AuthGuard } from "@nestjs/passport";
import { Seat } from "./entities/seat.entity";
import { plainToInstance } from "class-transformer";
import { SeatDto } from "./dto/seat.dto";
import camelcaseKeys = require("camelcase-keys");
import { LegGuard } from "../core/guards/leg.guard";
import { SeatGuard } from "../core/guards/seat.guard";
import { FlightArchived } from "../core/decorators/flight-archived.decorator";
import { LegFlight } from 'src/core/decorators/leg-flight.decorator';
import { ApiOperation } from "@nestjs/swagger";

@Controller('seat')
@UseGuards(AuthGuard('jwt'))
export class SeatController {
  constructor(private readonly _seatService: SeatService) {
  }

  @Get(':legId')
  @ApiOperation({summary: 'Retourne les sièges d\'un leg'})
  @UseGuards(LegGuard)
  @LegFlight(true)
  async findByLeg(@Param('legId') legId: string): Promise<Seat[]> {
    const seats: Seat[] = await this._seatService.findByLeg(+legId);
    return plainToInstance(Seat, camelcaseKeys(seats, {deep: true}));
  }

  @Post(':id/comment')
  @ApiOperation({summary: 'Commentaire d\' un siège'})
  @UseGuards(SeatGuard)
  @LegFlight(true)
  @FlightArchived(false)
  async comment(@Param('id') id: string,
                @Body() payload: { observations: string }): Promise<string> {
    await this._seatService.comment(+id, payload.observations);
    const seat: Seat = await this._seatService.findOne(+id);
    return seat.observations;
  }

  @Post(':id/lock')
  @ApiOperation({summary: 'Blocage d\'un siège'})
  @UseGuards(SeatGuard)
  @LegFlight(true)
  @FlightArchived(false)
  async lock(@Param('id') id: string): Promise<SeatDto[]> {
    const seatsUpdated = await this._seatService.lock(+id);
    const seats = await this._seatService.findSeveral(seatsUpdated.map(s => s.id));
    return plainToInstance(SeatDto, camelcaseKeys(seats, {deep: true}));
  }

  @Post(':id/unlock')
  @ApiOperation({summary: 'Déblocage d\'un siège'})
  @UseGuards(SeatGuard)
  @LegFlight(true)
  @FlightArchived(false)
  async unlock(@Param('id') id: string): Promise<SeatDto> {
    await this._seatService.unlock(+id);
    const seat = await this._seatService.findOne(+id);
    return plainToInstance(SeatDto, camelcaseKeys(seat, {deep: true}));
  }

  @Post(':id/place/:paxId')
  @ApiOperation({summary: 'Placement d\'un PAX sur un siège'})
  @UseGuards(SeatGuard)
  @LegFlight(true)
  @FlightArchived(false)
  async placePax(@Param('id') id: string,
                 @Param('paxId') paxId: string): Promise<SeatDto[]> {
    const seatsUpdated = await this._seatService.place(+id, +paxId);
    await this._seatService.computeLegAndPaxs(+id, seatsUpdated);
    const seats = await this._seatService.findSeveral(seatsUpdated.map(s => s.id));
    return plainToInstance(SeatDto, camelcaseKeys(seats, {deep: true}));
  }
}
