import { Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Leg } from "../../leg/entities/leg.entity";
import { Pax } from "../../pax/entities/pax.entity";
import { SeatZone } from "../../seat-template/dto/seat-zone.enum";

@Entity('seat')
export class Seat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false})
  zone: SeatZone;

  @Column({nullable: false, length: 2})
  row: string;

  @Column({nullable: false})
  number: number;

  @Index('seat_legId_index')
  @ManyToOne(type => Leg, leg => leg.seats, {onDelete: 'CASCADE'})
  leg: Leg;

  @ManyToOne(type => Pax, pax => pax.seats)
  pax: Pax;

  @Column({default: false})
  blocked: boolean;

  @Column({nullable: true, length: 255})
  observations: string;
}
