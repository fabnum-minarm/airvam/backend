import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Airport } from "../../airport/entities/airport.entity";
import { User } from "../../user/entities/user.entity";

@Entity('unit')
export class Unit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: true, length: 255})
  code: string;

  @Column({nullable: true, length: 255})
  name: string;

  @Column({nullable: true, length: 255})
  address1: string;

  @Column({nullable: true, length: 255})
  address2: string;

  @Column({nullable: true, length: 255})
  address3: string;

  @Column({nullable: true, length: 255})
  phone_number: string;

  @Column({nullable: true, length: 255})
  email: string;

  @Column({default: false})
  disabled: boolean;

  @ManyToMany(type => Airport, airport => airport.units)
  @JoinTable()
  airports: Airport[];

  @OneToMany(type => User, user => user.unit)
  users: User[];
}
