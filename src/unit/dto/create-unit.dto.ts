import { IsArray, IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber, IsString, Length } from "class-validator";

export class CreateUnitDto {
  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  code: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  name: string;

  @IsString()
  @IsOptional()
  @Length(1, 255)
  address1: string;

  @IsString()
  @IsOptional()
  @Length(1, 255)
  address2: string;

  @IsString()
  @IsOptional()
  @Length(1, 255)
  address3: string;

  @IsPhoneNumber()
  @IsOptional()
  phone_number: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsArray()
  @IsNotEmpty()
  airports: { id: string }[];
}
