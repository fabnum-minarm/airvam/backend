import { CreateUnitDto } from './create-unit.dto';
import { IsOptional } from "class-validator";

export class UnitDto extends CreateUnitDto {
  @IsOptional()
  id: number;

  @IsOptional()
  disabled: boolean;
}
