import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Like, Repository } from "typeorm";
import { Unit } from "./entities/unit.entity";

@Injectable()
export class UnitService {
  constructor(@InjectRepository(Unit)
              private readonly _unitRepository: Repository<Unit>) {
  }

  /**
   * Création d'une unité
   * @param unit
   */
  create(unit: Unit): Promise<Unit> {
    return this._unitRepository.save(unit);
  }

  /**
   * Récupération de toutes les unités
   * Renvoi des objets complets ou allégés
   * @param light
   */
  findAll(light: boolean): Promise<Unit[]> {
    return this._unitRepository.find({
      select: light ? ['id', 'code', 'name'] : [],
      relations: light ? [] : ['airports'],
      order: {
        disabled: "ASC",
        name: "ASC"
      }
    });
  }

  /**
   * Récupération d'une unité selon son ID
   * @param id
   */
  findOne(id: number): Promise<Unit> {
    return this._unitRepository.findOne({
      where: {id: id},
      relations: ['airports']
    });
  }

  /**
   * Récupération d'une unité selon une clause WHERE
   * @param whereClause
   */
  findOneWithClause(whereClause: any): Promise<Unit> {
    return this._unitRepository.findOne({
      where: whereClause
    });
  }

  /**
   * Mise à jour d'une unité
   * @param id
   * @param unit
   */
  update(id: number, unit: Unit): Promise<Unit> {
    unit.id = id;
    return this._unitRepository.save(unit);
  }

  /**
   * Activation d'une unité si celle-ci a été désactivée
   * @param id
   */
  async enable(id: number): Promise<void> {
    await this._unitRepository.update({id: id}, {disabled: false});
  }

  /**
   * Désactivation d'une unité
   * @param id
   */
  async remove(id: number): Promise<void> {
    await this._unitRepository.update({id: id}, {disabled: true});
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._unitRepository.delete({name: Like('CYTEST%')});
  }
}
