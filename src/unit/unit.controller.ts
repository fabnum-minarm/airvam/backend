import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { UnitService } from './unit.service';
import { CreateUnitDto } from './dto/create-unit.dto';
import { UnitDto } from './dto/unit.dto';
import { AuthGuard } from "@nestjs/passport";
import { ApiOperation } from "@nestjs/swagger";
import { plainToInstance } from "class-transformer";
import { Unit } from "./entities/unit.entity";
import { Roles } from "../core/decorators/role.decorator";
import { UserRole } from "../user/entities/user.entity";
import { RolesGuard } from "../core/guards/role.guard";
import { Dev } from "../core/decorators/dev.decorator";
import { DeleteResult } from "typeorm";
import { AuthUser } from "../core/decorators/user.decorator";
import { UserDto } from "../user/dto/user.dto";
import camelcaseKeys = require("camelcase-keys");
const snakecaseKeys = require('snakecase-keys');

@Controller('unit')
export class UnitController {
  constructor(private readonly _unitService: UnitService) {}

  @Post()
  @ApiOperation({summary: 'Créer une unité'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async create(@Body() createUnitDto: CreateUnitDto): Promise<UnitDto> {
    const unit: Unit = await this._unitService.create(plainToInstance(Unit, snakecaseKeys(createUnitDto)));
    const unitToReturn = await this._unitService.findOne(unit.id);
    return plainToInstance(UnitDto, camelcaseKeys(unitToReturn, {deep: true}));
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({summary: 'Retourne toutes les unités'})
  async findAll(@AuthUser() user: UserDto): Promise<UnitDto[]> {
    const units: Unit[] = await this._unitService.findAll(user.role !== UserRole.god);
    return plainToInstance(UnitDto, camelcaseKeys(units, {deep: true}));
  }

  @Patch(':id')
  @ApiOperation({summary: 'Edition d\'une unité'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async update(@Param('id') id: string, @Body() updateUnitDto: CreateUnitDto): Promise<UnitDto> {
    await this._unitService.update(+id, plainToInstance(Unit, snakecaseKeys(updateUnitDto)));
    const unit = await this._unitService.findOne(+id);
    return plainToInstance(UnitDto, camelcaseKeys(unit, {deep: true}));
  }

  @Patch(':id/activate')
  @ApiOperation({summary: 'Ré-activation d\'une unité'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async activate(@Param('id') id: string): Promise<UnitDto> {
    await this._unitService.enable(+id);
    const unit = await this._unitService.findOne(+id);
    return plainToInstance(UnitDto, camelcaseKeys(unit, {deep: true}));
  }

  @Delete(':id')
  @ApiOperation({summary: 'Désactivation d\'une unité'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async remove(@Param('id') id: string): Promise<UnitDto> {
    await this._unitService.remove(+id)
    const unit = await this._unitService.findOne(+id);
    return plainToInstance(UnitDto, camelcaseKeys(unit, {deep: true}));
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._unitService.removeTestData();
  }
}
