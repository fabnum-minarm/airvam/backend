import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AirvamLogger } from "./logger/airvam.logger";
import * as bodyParser from 'body-parser';
const compression = require('compression');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useLogger(app.get(AirvamLogger));
  app.use(compression());
  app.use(bodyParser.json({limit: '5mb'}));
  app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
  // TODO à modifier en PROD
  app.enableCors({
    origin: '*',
    exposedHeaders: ['content-disposition']
  });
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  app.setGlobalPrefix('api');

  // OpenAPI
  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('API AirVam')
    .setVersion('0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(process.env.PORT);
}

bootstrap();
