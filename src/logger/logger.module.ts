import { Module } from '@nestjs/common';
import { AirvamLogger } from "./airvam.logger";

@Module({
  providers: [AirvamLogger],
  exports: [AirvamLogger],
})
export class LoggerModule {
}
