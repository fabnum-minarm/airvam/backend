import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { SharedModule } from "../shared/shared.module";
import { UnitModule } from "../unit/unit.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    SharedModule,
    UnitModule
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
