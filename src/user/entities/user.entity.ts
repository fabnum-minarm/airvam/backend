import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";
import { Rank } from "../../rank/entities/rank.entity";
import { Flight } from "../../flight/entities/flight.entity";
import { Unit } from "../../unit/entities/unit.entity";
import { FlightHistoric } from "../../flight-historic/entities/flight-historic.entity";

export enum UserRole {
  god = 'god',
  admin = 'admin',
  user = 'user'
}

@Entity('airvam_user')
export class User {
  @PrimaryColumn({nullable: false, length: 255})
  email: string;

  @Column({nullable: false, length: 100})
  first_name: string;

  @Column({nullable: false, length: 100})
  last_name: string;

  @Column('enum', {name: 'role', enum: UserRole, default: UserRole.user, nullable: false})
  role: UserRole;

  @CreateDateColumn({type: "timestamp"})
  created_at: Date;

  @Column({type: "timestamp", nullable: true})
  last_login: Date;

  @Column({default: false})
  disabled: boolean;

  @Column({default: false})
  anonymized: boolean;

  @OneToMany(type => Flight, flight => flight.regulator_user)
  regulator_flights: Flight[];

  @ManyToOne(type => Rank, rank => rank.users)
  rank: Rank;

  @ManyToOne(type => Unit, unit => unit.users)
  unit: Unit;

  @OneToMany(type => FlightHistoric, flightHistoric => flightHistoric.user)
  historic: FlightHistoric[];
}
