import { CreateUserDto } from './create-user.dto';
import { IsOptional } from "class-validator";

export class UserDto extends CreateUserDto {
  @IsOptional()
  disabled: boolean;

  @IsOptional()
  lastLogin: number;

  @IsOptional()
  authorizedAirports: number[];
}
