import { IsEnum, IsNotEmpty, IsObject, IsOptional, IsString } from "class-validator";
import { UserRole } from "../entities/user.entity";
import { Rank } from "../../rank/entities/rank.entity";
import { Unit } from "../../unit/entities/unit.entity";

export class EditUserDto {

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsEnum(UserRole)
  @IsNotEmpty()
  role: UserRole;

  @IsObject()
  @IsOptional()
  rank: Rank;

  @IsObject()
  @IsOptional()
  unit: Unit;
}
