import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User, UserRole } from "./entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Like, Repository, UpdateResult } from "typeorm";
import { UserDto } from "./dto/user.dto";
import { CreateUserDto } from "./dto/create-user.dto";
import { EditUserDto } from "./dto/edit-user.dto";
import { MailService } from "../shared/services/mail.service";
import { v4 as uuidv4 } from 'uuid';
import { Unit } from "../unit/entities/unit.entity";
import { UnitService } from "../unit/unit.service";
import { AirvamLogger } from "../logger/airvam.logger";

@Injectable()
export class UserService {
  private readonly _logger = new AirvamLogger('UserService');
  constructor(@InjectRepository(User)
              private readonly _usersRepository: Repository<User>,
              private _mailService: MailService,
              private _unitService: UnitService) {
  }

  /**
   * Récupération de tous les utilisateurs
   * Filtrage par unité si l'utilisateur n'est pas god
   * @param authUser
   */
  findAll(authUser: UserDto): Promise<User[]> {
    const whereClause: any = authUser.role === UserRole.god ? {} : {
      unit: authUser.unit ? {id: authUser.unit?.id} : null,
    };
    whereClause.anonymized = false;
    return this._usersRepository.find({
      relations: ['rank', 'unit'],
      where: whereClause,
      order: {
        disabled: 'ASC',
        first_name: 'ASC',
        last_name: 'ASC'
      }
    });
  }

  /**
   * Récupération de tous les utilisateurs avec seulement les infos nécessaires
   */
  findAllLight(): Promise<User[]> {
    return this._usersRepository.find({
      select: ['email', 'first_name', 'last_name', 'role'],
      order: {
        disabled: 'ASC',
        first_name: 'ASC',
        last_name: 'ASC'
      },
      where: {anonymized: false}
    });
  }

  /**
   * Récupération d'un utilisateur suivant son adresse mail
   * @param email
   * @param withAirports
   */
  findOne(email: string, withAirports = false): Promise<User> {
    if (!email) {
      return null;
    }
    const relations = withAirports ? ['rank', 'unit', 'unit.airports'] : ['rank', 'unit'];
    return this._usersRepository.findOne({
      relations: relations,
      where: {email: email, anonymized: false}
    });
  }

  /**
   * Mise à jour d'un utilisateur et renvoi complet de celui-ci
   * @param email
   * @param data
   */
  async update(email: string, data: any): Promise<User> {
    await this._usersRepository.update({email: email}, data);
    return this.findOne(email);
  }

  /**
   * Création d'un utilisateur et envoi d'un email à celui-ci
   * @param user
   */
  async create(user: User): Promise<User> {
    const userExists = await this.findOne(user.email);
    if (userExists) {
      throw new HttpException('Un utilisateur avec cet email existe déjà.', HttpStatus.FORBIDDEN);
    }
    const userToReturn = await this._usersRepository.save(user);
    try {
      await this._sendEmailCreateUser(userToReturn);
    } catch (e) {
      this._logger.error('', e);
    }
    return userToReturn;
  }

  /**
   * Activation d'un utilisateur si celui-ci a été désactivé
   * @param email
   */
  async enable(email: string): Promise<void> {
    await this._usersRepository.update({email: email, anonymized: false}, {disabled: false});
  }

  /**
   * Désactivation d'un utilisateur
   * @param email
   */
  async delete(email: string): Promise<void> {
    await this._usersRepository.update({email: email, anonymized: false}, {disabled: true});
  }

  /**
   * Anonymisation des informations personnelles d'un utilisateur (RGPD)
   * On garde l'utilisateur en BDD pour la cohérence des données
   * @param email
   */
  async deletePermanent(email: string): Promise<void> {
    const uuid: string = uuidv4();
    await this._usersRepository.update({email: email}, {
      email: `${uuid}@email.com`,
      first_name: uuid,
      last_name: uuid,
      disabled: true,
      rank: null,
      unit: null,
      role: UserRole.user,
      last_login: null,
      anonymized: true
    })
  }

  /**
   * Mise à jour de la date de dernier login
   * @param userEmail
   */
  login(userEmail: string): Promise<UpdateResult> {
    return this._usersRepository.update({email: userEmail}, {last_login: new Date()});
  }

  /**
   * Vérification si un utilisateur peut créer ou mettre à jour un autre utilisateur
   * @param authUser
   * @param user
   */
  canCreateUpdate(authUser: UserDto, user: CreateUserDto | EditUserDto): boolean {
    if (authUser.role === UserRole.god) {
      return true;
    }
    // Un administrateur ne peut créer / modifier des super admin, des utilisateurs en dehors de son unité
    return !(user.role === UserRole.god || (user.unit && user.unit.id !== authUser.unit.id) || (!authUser.unit && user.unit && user.unit.id));
  }

  /**
   * Envoi d'un email lors de la création d'un utilisateur
   * @param user
   * @private
   */
  private async _sendEmailCreateUser(user: User) {
    await this._mailService.sendEmail(user.email,
      'Airvam - Création de compte',
      'create-account',
      {  // Data to be sent to template engine.
        firstName: user.first_name,
        url: `${process.env.HOST_URL}`
      })
      .then();
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Ajout d'utilisateurs pour les tests E2E
   */
  async populateTestData(): Promise<void> {
    const minarmUnit: Unit = await this._unitService.findOneWithClause({name: 'MINARM'});
    await Promise.all([
      this._usersRepository.update({email: 'dev-user@email.com'}, {unit: minarmUnit}),
      this._usersRepository.update({email: 'dev-admin@email.com'}, {unit: minarmUnit})
    ]);
    return;
  }

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._usersRepository.delete({email: Like('CYTEST%')});
  }
}
