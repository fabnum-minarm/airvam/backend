import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpException,
  HttpStatus
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UserDto } from './dto/user.dto';
import { ApiOperation } from "@nestjs/swagger";
import { User, UserRole } from "./entities/user.entity";
import { plainToInstance } from "class-transformer";
import { EditUserDto } from "./dto/edit-user.dto";
import { AuthGuard } from "@nestjs/passport";
import { AuthUser } from "../core/decorators/user.decorator";
import { Dev } from "../core/decorators/dev.decorator";
import { RolesGuard } from "../core/guards/role.guard";
import { Roles } from "../core/decorators/role.decorator";
import { DeleteResult } from "typeorm";
import camelcaseKeys = require("camelcase-keys");
const snakecaseKeys = require('snakecase-keys');

@Controller('user')
export class UserController {
  constructor(private readonly _userService: UserService) {
  }

  @Post('')
  @ApiOperation({summary: 'Création d\'un utilisateur'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.admin, UserRole.god)
  async create(@Body() userDto: CreateUserDto,
               @AuthUser() authUser: UserDto): Promise<UserDto> {
    if (!this._userService.canCreateUpdate(authUser, userDto)) {
      throw new HttpException(`Vous n'avez pas le droit de modifier cet utilisateur`, HttpStatus.FORBIDDEN);
    }
    const user = await this._userService.create(plainToInstance(User, snakecaseKeys(userDto)));
    return plainToInstance(UserDto, camelcaseKeys(user, {deep: true}));
  }

  @Get('')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiOperation({summary: 'Retourne tout les utilisateurs'})
  async getUsers(@AuthUser() authUser: UserDto): Promise<UserDto[]> {
    if (![UserRole.admin, UserRole.god].includes(authUser.role)) {
      return [];
    }
    const users: User[] = await this._userService.findAll(authUser);
    return plainToInstance(UserDto, camelcaseKeys(users, {deep: true}));
  }

  @Get('light')
  @Dev()
  @ApiOperation({summary: 'Retourne tout les utilisateurs'})
  async getUsersLight(): Promise<UserDto[]> {
    const users: User[] = await this._userService.findAllLight();
    return plainToInstance(UserDto, camelcaseKeys(users, {deep: true}));
  }

  @Get('me')
  @ApiOperation({summary: `Retourne l'utilisateur courant`})
  @UseGuards(AuthGuard('jwt'))
  async getMe(@AuthUser() user: UserDto): Promise<UserDto> {
    const userToReturn: User = await this._userService.findOne(user.email);
    return plainToInstance(UserDto, camelcaseKeys(userToReturn, {deep: true}));
  }

  @Patch(':email')
  @ApiOperation({summary: 'Modification d\'un utilisateur'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.admin, UserRole.god)
  async update(@Body() userDto: EditUserDto,
               @Param('email') email: string,
               @AuthUser() authUser: UserDto): Promise<UserDto> {
    if (!this._userService.canCreateUpdate(authUser, userDto)) {
      throw new HttpException(`Vous n'avez pas le droit de modifier cet utilisateur`, HttpStatus.FORBIDDEN);
    }
    await this._userService.update(email, plainToInstance(User, snakecaseKeys(userDto)));
    const user = await this._userService.findOne(email);
    return plainToInstance(UserDto, camelcaseKeys(user, {deep: true}));
  }

  @Patch(':email/activate')
  @ApiOperation({summary: 'Ré-activation d\'un utilisateur'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.admin, UserRole.god)
  async enable(@Param('email') email: string): Promise<UserDto> {
    await this._userService.enable(email);
    const user = await this._userService.findOne(email);
    return plainToInstance(UserDto, camelcaseKeys(user, {deep: true}));
  }

  @Delete(':email')
  @ApiOperation({summary: 'Désactivation d\'un utilisateur'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.admin, UserRole.god)
  async delete(@Param('email') email: string): Promise<UserDto> {
    await this._userService.delete(email);
    const user = await this._userService.findOne(email);
    return plainToInstance(UserDto, camelcaseKeys(user, {deep: true}));
  }

  @Delete(':email/permanent')
  @ApiOperation({summary: 'Suppression d\'un utilisateur'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.admin, UserRole.god)
  async deletePermanent(@Param('email') email: string): Promise<void> {
    return this._userService.deletePermanent(email);
  }

  @Post('populateTestData')
  @Dev()
  @ApiOperation({summary: 'Génération des données nécessaires pour les tests'})
  async populateTestData(): Promise<void> {
    return this._userService.populateTestData();
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._userService.removeTestData();
  }
}
