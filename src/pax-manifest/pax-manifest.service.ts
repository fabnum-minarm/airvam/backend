import { Injectable } from '@nestjs/common';
import { PaxManifest } from "./entities/pax-manifest.entity";
import { AirvamLogger } from "../logger/airvam.logger";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Pax } from "../pax/entities/pax.entity";
import { Leg } from "../leg/entities/leg.entity";


@Injectable()
export class PaxManifestService {
  private readonly _logger = new AirvamLogger('PaxManifestService');

  constructor(@InjectRepository(PaxManifest)
              private readonly _paxManifestRepository: Repository<PaxManifest>) {
  }

  /**
   * Suppression par leg
   * @param legId
   */
  deleteByLeg(legId: number) {
    return this._paxManifestRepository.delete({leg: {id: legId}});
  }

  /**
   * Flag des PAXs en LMC
   * Peut être filtré par leg
   * @param paxs
   * @param leg
   */
  lmc(paxs: Pax[], leg?: Leg) {
    const promises = [];
    paxs.forEach(p => {
      const criteria = !!leg ? {pax: {id: p.id}, leg: leg} : {pax: {id: p.id}};
      // @ts-ignore
      promises.push(this._paxManifestRepository.update(criteria, {lmc: true}));
    });
    return Promise.all(promises);
  }

  /**
   * Reset du flag LMC des PAXs
   * Peut être filtré par leg
   * @param paxs
   * @param leg
   */
  resetLmc(paxs: Pax[], leg: Leg) {
    const promises = [];
    paxs.forEach(p => {
      // @ts-ignore
      promises.push(this._paxManifestRepository.update({pax: p, leg: leg}, {lmc: false}));
    });
    return Promise.all(promises);
  }

  /**
   * Mise à jour des numéros de manifeste des PAXs d'un leg
   * @param paxs
   * @param leg
   */
  updateManifestNumbers(paxs: Pax[], leg: Leg) {
    return this._paxManifestRepository.save(paxs.map(p => {
      const toReturn = p.pax_manifest[0];
      if (!toReturn.id) {
        toReturn.pax = p;
        toReturn.leg = leg;
      }
      return toReturn;
    }));
  }
}
