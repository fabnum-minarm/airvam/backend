import { Module } from '@nestjs/common';
import { PaxManifestService } from './pax-manifest.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { PaxManifest } from "./entities/pax-manifest.entity";

@Module({
  imports: [
    TypeOrmModule.forFeature([PaxManifest]),
  ],
  controllers: [],
  providers: [PaxManifestService],
  exports: [PaxManifestService]
})
export class PaxManifestModule {}
