import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn, Unique
} from "typeorm";
import { Leg } from "../../leg/entities/leg.entity";
import { Pax } from "../../pax/entities/pax.entity";

@Entity('pax_manifest')
@Unique(["leg", "pax"])
export class PaxManifest {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Leg, leg => leg.pax_manifest, {onDelete: 'CASCADE'})
  leg: Leg;

  @ManyToOne(type => Pax, pax => pax.pax_manifest, {onDelete: 'CASCADE'})
  pax: Pax;

  @Column({nullable: true, type: 'integer'})
  manifest_number: number;

  @Column({default: false})
  lmc: boolean;
}
