import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Like, Repository, UpdateResult } from "typeorm";
import { Rank } from "./entities/rank.entity";

@Injectable()
export class RankService {

  constructor(@InjectRepository(Rank)
              private readonly _rankRepository: Repository<Rank>) {
  }

  /**
   * Création d'un grade
   * @param rank
   */
  create(rank: Rank): Promise<Rank> {
    return this._rankRepository.save(rank);
  }

  /**
   * Retourne tous les grades
   */
  findAll(): Promise<Rank[]> {
    return this._rankRepository.find({
      order: {
        disabled: "ASC",
        rank: "ASC",
        name: "ASC"
      }
    });
  }

  /**
   * Récupération d'un grade
   * @param id
   */
  findOne(id: number): Promise<Rank> {
    return this._rankRepository.findOneBy({id: id});
  }

  /**
   * Récupération d'un grade suivant une clause WHERE
   * @param whereClause
   */
  findOneWithClause(whereClause: any): Promise<Rank> {
    return this._rankRepository.findOne({
      where: whereClause
    });
  }

  /**
   * Mise à jour d'un grade
   * @param id
   * @param rank
   */
  update(id: number, rank: Rank): Promise<UpdateResult> {
    return this._rankRepository.update({id: id}, rank);
  }

  /**
   * Activation d'un grade si celui-ci avait été désactivé
   * @param id
   */
  async enable(id: number): Promise<void> {
    await this._rankRepository.update({id: id}, {disabled: false});
  }

  /**
   * Désactivation d'un grade
   * @param id
   */
  async remove(id: number): Promise<void> {
    await this._rankRepository.update({id: id}, {disabled: true});
  }

  /************************************************************************************ TEST FUNCTIONS ************************************************************************************/

  /**
   * Suppression des données générées par les tests E2E
   * Par convention les données générées par les tests E2E sont préfixées par CYTEST
   */
  removeTestData(): Promise<DeleteResult> {
    return this._rankRepository.delete({name: Like('CYTEST%')});
  }
}
