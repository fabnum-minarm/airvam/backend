import { IsInt, IsNotEmpty, IsString, Max, Min } from "class-validator";
import { RankCategory } from "../entities/rank.entity";

export class CreateRankDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  category: RankCategory;

  @IsInt()
  @IsNotEmpty()
  @Min(1)
  @Max(100)
  rank: number;
}
