import { IsOptional } from "class-validator";
import { CreateRankDto } from "./create-rank.dto";

export class RankDto extends CreateRankDto {
  @IsOptional()
  id: number;

  @IsOptional()
  disabled: boolean;
}
