import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { RankService } from './rank.service';
import { RankDto } from './dto/rank.dto';
import { Rank } from "./entities/rank.entity";
import { plainToInstance } from "class-transformer";
import camelcaseKeys = require("camelcase-keys");
import { ApiOperation } from "@nestjs/swagger";
import { CreateRankDto } from "./dto/create-rank.dto";
import { AuthGuard } from "@nestjs/passport";
import { RolesGuard } from "../core/guards/role.guard";
import { Roles } from "../core/decorators/role.decorator";
import { UserRole } from "../user/entities/user.entity";
import { DeleteResult } from "typeorm";
import { Dev } from "../core/decorators/dev.decorator";
const snakecaseKeys = require('snakecase-keys');

@Controller('rank')
export class RankController {
  constructor(private readonly _rankService: RankService) {}

  @Post()
  @ApiOperation({summary: 'Créer un grade'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async create(@Body() createRankDto: CreateRankDto): Promise<RankDto> {
    const rankToReturn: Rank = await this._rankService.create(plainToInstance(Rank, snakecaseKeys(createRankDto)));
    return plainToInstance(RankDto, camelcaseKeys(rankToReturn, {deep: true}));
  }

  @Get()
  @ApiOperation({summary: 'Retourne tout les grades'})
  @UseGuards(AuthGuard('jwt'))
  async findAll(): Promise<RankDto[]> {
    const ranks: Rank[] = await this._rankService.findAll();
    return plainToInstance(RankDto, camelcaseKeys(ranks, {deep: true}));
  }

  @Patch(':id')
  @ApiOperation({summary: 'Edition d\'un grade'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async update(@Param('id') id: string, @Body() updateRankDto: CreateRankDto): Promise<RankDto> {
    await this._rankService.update(+id, plainToInstance(Rank, snakecaseKeys(updateRankDto)));
    const rank = await this._rankService.findOne(+id);
    return plainToInstance(RankDto, camelcaseKeys(rank, {deep: true}));
  }

  @Patch(':id/activate')
  @ApiOperation({summary: 'Ré-activation d\'un grade'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async activate(@Param('id') id: string): Promise<RankDto> {
    await this._rankService.enable(+id);
    const rank = await this._rankService.findOne(+id);
    return plainToInstance(RankDto, camelcaseKeys(rank, {deep: true}));
  }

  @Delete(':id')
  @ApiOperation({summary: 'Désactivation d\'un grade'})
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles(UserRole.god)
  async remove(@Param('id') id: string): Promise<RankDto> {
    await this._rankService.remove(+id);
    const rank = await this._rankService.findOne(+id);
    return plainToInstance(RankDto, camelcaseKeys(rank, {deep: true}));
  }

  @Post('clearTestData')
  @Dev()
  @ApiOperation({summary: 'Suppression des données générées par les tests'})
  async clearTestData(): Promise<DeleteResult> {
    return this._rankService.removeTestData();
  }
}
