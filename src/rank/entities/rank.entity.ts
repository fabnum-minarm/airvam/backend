import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { User } from "../../user/entities/user.entity";
import { Pax } from "../../pax/entities/pax.entity";
import { Animal } from "../../animal/entities/animal.entity";

export enum RankCategory {
  'Air et Espace' = 'Air et Espace',
  'Terre' = 'Terre',
  'Marine' = 'Marine',
  'OTAN' = 'OTAN',
  'Gendarmerie' = 'Gendarmerie',
  'SSA' = 'SSA',
  'Commissaire' = 'Commissaire',
  'Autre' = 'Autre',
}

@Entity('rank')
@Unique(["name", "rank", "category"])
export class Rank {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: false, length: 10})
  name: string;

  @Column({
    nullable: false,
    type: "enum",
    enum: RankCategory,
  })
  category: RankCategory;

  @Column({nullable: false})
  rank: number;

  @Column({default: false})
  disabled: boolean;

  @OneToMany(type => User, user => user.rank)
  users: User[];

  @OneToMany(type => Pax, pax => pax.rank)
  paxs: Pax[];

  @OneToMany(type => Animal, animal => animal.owner_rank)
  animals: Animal[];
}
