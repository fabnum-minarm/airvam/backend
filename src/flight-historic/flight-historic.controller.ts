import { Controller, UseGuards } from '@nestjs/common';
import { FlightHistoricService } from './flight-historic.service';
import { AuthGuard } from "@nestjs/passport";

@Controller('flight-historic')
@UseGuards(AuthGuard('jwt'))
export class FlightHistoricController {
  constructor(private readonly flightHistoricService: FlightHistoricService) {}
}
