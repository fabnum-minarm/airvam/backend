import { forwardRef, Module } from '@nestjs/common';
import { FlightHistoricService } from './flight-historic.service';
import { FlightHistoricController } from './flight-historic.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { FlightHistoric } from "./entities/flight-historic.entity";
import { LegModule } from "../leg/leg.module";
import { FlightModule } from "../flight/flight.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([FlightHistoric]),
    forwardRef(() => LegModule),
    forwardRef(() => FlightModule)
  ],
  controllers: [FlightHistoricController],
  providers: [FlightHistoricService],
  exports: [FlightHistoricService]
})
export class FlightHistoricModule {
}
