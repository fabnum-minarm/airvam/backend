import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FlightHistoric } from "./entities/flight-historic.entity";
import { Flight } from "../flight/entities/flight.entity";
import { LegService } from "../leg/leg.service";
import { FlightService } from "../flight/flight.service";

@Injectable()
export class FlightHistoricService {
  constructor(@InjectRepository(FlightHistoric)
              private readonly _flightHistoricRepository: Repository<FlightHistoric>,
              @Inject(forwardRef(() => LegService))
              private _legService: LegService,
              @Inject(forwardRef(() => FlightService))
              private _flightService: FlightService) {
  }

  /**
   * Insertion d'une ligne d'historique
   * @param flight
   * @param userEmail
   * @param action
   */
  async insert(flight: Flight, userEmail: string, action: string): Promise<FlightHistoric> {
    if (!flight) {
      return;
    }
    const flightHistoric: FlightHistoric = {
      // @ts-ignore
      flight: {id: flight.id},
      // @ts-ignore
      user: {email: userEmail},
      action: `${action} ${flight.id}, ${flight.cotam_number}, ${flight.mission_name}, ${flight.mission_number}`
    };
    return this._flightHistoricRepository.save(flightHistoric);
  }

  /**
   * Insertion d'une ligne d'historique via le flightId
   * @param flightId
   * @param userEmail
   * @param action
   */
  async insertByFlightId(flightId: number, userEmail: string, action: string): Promise<FlightHistoric> {
    if (!flightId) {
      return;
    }
    const flight = await this._flightService.findOneLight(flightId);
    return this.insert(flight, userEmail, action);
  }

  /**
   * Insertion d'une ligne d'historique via le legId
   * @param legId
   * @param userEmail
   * @param action
   */
  async insertByLegId(legId: number, userEmail: string, action: string): Promise<FlightHistoric> {
    if (!legId) {
      return;
    }
    const leg = await this._legService.findOneLight(legId);
    const flightHistoric: FlightHistoric = {
      // @ts-ignore
      flight: {id: leg.flight.id},
      // @ts-ignore
      user: {email: userEmail},
      action: `${action} ${leg.id}, ${leg.departure?.oaci} - ${leg.arrival?.oaci}`
    };
    return this._flightHistoricRepository.save(flightHistoric);
  }
}
