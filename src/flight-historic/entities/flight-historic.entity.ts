import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Flight } from "../../flight/entities/flight.entity";
import { User } from "../../user/entities/user.entity";

@Entity('flight_historic')
export class FlightHistoric {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Flight, flight => flight.historic, {onDelete: 'CASCADE'})
  flight: Flight;

  @ManyToOne(type => User, user => user.historic, {onUpdate: 'CASCADE'})
  user: User;

  @CreateDateColumn({type: "timestamp"})
  time: Date;

  @Column({nullable: false, length: 2000})
  action: string;
}
