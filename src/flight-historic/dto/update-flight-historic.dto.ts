import { PartialType } from '@nestjs/swagger';
import { CreateFlightHistoricDto } from './create-flight-historic.dto';

export class UpdateFlightHistoricDto extends PartialType(CreateFlightHistoricDto) {}
