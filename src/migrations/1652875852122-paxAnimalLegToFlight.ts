import {MigrationInterface, QueryRunner} from "typeorm";

export class paxAnimalLegToFlight1652875852122 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM seat;`);
        await queryRunner.query(`DELETE FROM pax;`);
        await queryRunner.query(`DELETE FROM animal;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
