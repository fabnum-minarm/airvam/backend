import { MigrationInterface, QueryRunner } from "typeorm"

export class aircraftSiegesBloques1676536724347 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'A' AND number = 3;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'A' AND number = 4;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'K' AND number = 3;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'K' AND number = 4;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'A' AND number = 26;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'C' AND number = 26;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'J' AND number = 26;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCS' AND row = 'K' AND number = 26;

UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCT' AND row = 'A' AND number = 24;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCT' AND row = 'C' AND number = 24;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCT' AND row = 'J' AND number = 24;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCT' AND row = 'K' AND number = 24;

UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'A' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'A' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'C' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'C' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'H' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'H' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'K' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 ESTEREL - Plan cabine F-UJCU' AND row = 'K' AND number = 2;

UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'A' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'A' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'C' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'C' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'H' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'H' AND number = 2;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'K' AND number = 1;
UPDATE "seat_template" SET blocked = true WHERE "aircraftName" = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88' AND row = 'K' AND number = 2;
`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
