import { MigrationInterface, QueryRunner } from "typeorm"

export class AirportTicketMention1679645447314 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        UPDATE "airport" SET ticket_mention = 'N''attendez pas ! Après l''enregistrement de vos bagages de soute, rendez-vous en salle d''embarquement !';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
