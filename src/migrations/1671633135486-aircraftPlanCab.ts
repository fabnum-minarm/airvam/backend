import { MigrationInterface, QueryRunner } from "typeorm"

export class aircraftPlanCab1671633135486 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        // ADD DATA
        await queryRunner.query(`
        UPDATE "aircraft" SET plan_cab = '16-16-96-102' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCT';
        UPDATE "aircraft" SET plan_cab = '16-148-114' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCS';
        UPDATE "aircraft" SET plan_cab = '48-144-188' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCU';
        UPDATE "aircraft" SET plan_cab = '40-144-88' WHERE name = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88';
        UPDATE "aircraft" SET plan_cab = '88' WHERE name = 'A330.200 PHENIX MRTT - Plan cabine 88Y';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
