import {MigrationInterface, QueryRunner} from "typeorm";

export class UJCUPlanCabine1655307016690 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO "aircraft" (name, short_name) VALUES
('A330.200 ESTEREL - Plan cabine F-UJCU', 'A330 F-UJCU')`);

        await queryRunner.query(`INSERT INTO "seat_template" ("aircraftName", zone, row, number, blocked) VALUES
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'A', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'A', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 26, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 27, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'A', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'C', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'C', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 26, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 27, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'C', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'D', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'D', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'D', 38, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'E', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'E', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'E', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'F', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'F', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'F', 38, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'G', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'G', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'G', 38, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'H', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'H', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 26, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 27, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'H', 37, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 1, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 2, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 3, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 4, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 5, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OA', 'K', 6, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 7, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 8, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 9, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 10, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 11, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 12, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 14, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 15, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 16, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 17, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 18, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 19, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 20, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 21, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 22, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 23, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 24, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OB', 'K', 25, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 26, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 27, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 28, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 29, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 30, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 31, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 32, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 33, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 34, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 35, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 36, false),
('A330.200 ESTEREL - Plan cabine F-UJCU', 'OC', 'K', 37, false)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
