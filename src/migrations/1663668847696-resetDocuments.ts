import {MigrationInterface, QueryRunner} from "typeorm";

export class resetDocuments1663668847696 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        DELETE FROM document;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
