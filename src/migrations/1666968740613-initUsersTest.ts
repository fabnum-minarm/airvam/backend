import { MigrationInterface, QueryRunner } from "typeorm";

export class initUsersTest1666968740613 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    if (!['test'].includes(process.env.APP_ENV)) {
      return;
    }
    await queryRunner.query(`INSERT INTO "airvam_user" (email, first_name, last_name, role) VALUES
('dev-user@email.com', 'Utilisateur', 'Test', 'user'),
('dev-admin@email.com', 'Administrateur', 'Test', 'admin'),
('dev-super-admin@email.com', 'Super admin', 'Test', 'god')`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
