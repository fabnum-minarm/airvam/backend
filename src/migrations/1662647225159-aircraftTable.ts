import {MigrationInterface, QueryRunner} from "typeorm";

export class aircraftTable1662647225159 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        // ADD DATA
        await queryRunner.query(`
        UPDATE "aircraft" SET type = 'A330.200', registration = 'F-UJCT' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCT';
        UPDATE "aircraft" SET type = 'A330.200', registration = 'F-UJCS' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCS';
        UPDATE "aircraft" SET type = 'A330.200', registration = 'F-UJCU' WHERE name = 'A330.200 ESTEREL - Plan cabine F-UJCU';
        UPDATE "aircraft" SET type = 'A330.200', registration = '40-144-88' WHERE name = 'A330.200 PHENIX MRTT - Plan cabine 40-144-88';
        UPDATE "aircraft" SET type = 'A330.200', registration = '88Y' WHERE name = 'A330.200 PHENIX MRTT - Plan cabine 88Y';
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
