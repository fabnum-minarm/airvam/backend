import { MigrationInterface, QueryRunner } from "typeorm";

export class resetDocuments1663254399056 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        DELETE FROM leg_document;
        DELETE FROM pax_manifest;
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
