import { MigrationInterface, QueryRunner } from "typeorm";

export class AircraftData1640250072960 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO "aircraft" (name, short_name) VALUES
('A330.200 ESTEREL - Plan cabine F-UJCT', 'A330 F-UJCT'),
('A330.200 ESTEREL - Plan cabine F-UJCS', 'A330 F-UJCS'),
('A330.200 PHENIX MRTT - Plan cabine 40-144-88', 'A330 40-144-88'),
('A330.200 PHENIX MRTT - Plan cabine 88Y', 'A330 88Y')`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
