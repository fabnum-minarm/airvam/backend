import {MigrationInterface, QueryRunner} from "typeorm";

export class UnitData1645432931763 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      queryRunner.query(`INSERT INTO unit (code, name) VALUES
    ('ETAA 1D.105 - EVX', 'EVREUX'),
    ('ETAA 1D.107 - VLY ', 'VILLACOUBLAY'),
    ('ETAA 1D.110 - CRL', 'CREIL'),
    ('ETAA 1D.110 - CRL', 'CREIL'),
    ('ETAA 1D.123 - OAN', 'ORLÉANS'),
    ('ETAA 1D.125 - ITR', 'ISTRES'),
    ('ETAAS 1D.118 - MDM', 'MONT DE MARSAN'),
    ('ETAAS 1D.126 - SZA', 'SOLENZARA'),
    ('ETAAS 1D.702 - AVD', 'AVORD'),
    ('AT : 1er RTP - DiTIA TLS & PAU', 'DiTIA TOULOUSE & DiTIA PAU'),
    ('MN : BAN LANDIVISIAU', 'LANDIVISIAU'),
    ('MN : BAN LANN-BIHOUE', 'LANN-BIHOUE'),
    ('MN : BAN HYERES', 'HYERES'),
    ('EAM 1D.104 - DHF (FFEAU)', 'AL DHAFRA'),
    ('EAM 1D.168 - ABJ (FFCI)', 'ABIDJAN'),
    ('EAM 1D.188 - DJI (FFDj)', 'DJIBOUTI'),
    ('EAM 1D.470 - LBV (EFG)', 'LIBREVILLE'),
    ('EA 12.266 - DKR (EFS)', 'DAKAR'),
    ('EAM 1D.181 - RUN (FAZSOI)', 'LA RÉUNION'),
    ('EAM 1D.186 - NOU (FANC)', 'NOUMÉA'),
    ('EAM 1D.190 - PPT (FAPF)', 'PAPEETE'),
    ('EAM 1D.366 - FDF (FAA)', 'FORT DE France'),
    ('EAM 1D.367 - CAY (FAG)', 'CAYENNE'),
    ('DéTIA NDJ (BKN)', 'N’DJAMENA'),
    ('DéTIA NMY (BKN)', 'NIAMEY'),
    ('DéTIA BAMAKO (BKN)', 'BAMAKO'),
    ('DéTIA MENAKA (BKN)', 'MENAKA'),
    ('DéTIA GAO (BKN)', 'GAO'),
    ('DéTIA TST (BKN)', 'TESSALIT'),
    ('DéTIA TBU (BKN)', 'TOMBOUCTOU'),
    ('DéTIA OUA (SBR)', 'OUAGADOUGOU'),
    ('DéTIA BGI (RCA)', 'BANGUI'),
    ('DéTIA BAP H5 (CHA)', '"Prince Hassan" d’As Safawi'),
    ('DéTIA BGD (CHA)', 'Baghdad'),
    ('DéTIA EBL (CHA)', 'Erbil'),
    ('CCITTM DEYR KIFA (DMN)', 'Beyrouth')`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
