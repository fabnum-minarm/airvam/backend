import { Injectable } from '@nestjs/common';
import { Aircraft } from "./entities/aircraft.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class AircraftService {

  constructor(@InjectRepository(Aircraft)
              private readonly _aircraftRepository: Repository<Aircraft>) {
  }

  /**
   * Récupération de tous les avions
   */
  findAll(): Promise<Aircraft[]> {
    return this._aircraftRepository.find({
      select: ['name', 'short_name'],
      relations: ['seats_template'],
      order: {
        name: "ASC"
      }
    });
  }

  /**
   * Récupération d'un avion
   * @param name
   */
  findOne(name: string): Promise<Aircraft> {
    return this._aircraftRepository.findOne({
      where: {name: name},
      relations: ['seats_template'],
    });
  }
}
