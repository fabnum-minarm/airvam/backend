import { Controller, Get, UseGuards } from '@nestjs/common';
import { AircraftService } from "./aircraft.service";
import { AircraftDto } from "./dto/aircraft.dto";
import { Aircraft } from "./entities/aircraft.entity";
import { plainToInstance } from "class-transformer";
import camelcaseKeys = require("camelcase-keys");
import { AuthGuard } from "@nestjs/passport";
import { ApiOperation } from "@nestjs/swagger";

@Controller('aircraft')
@UseGuards(AuthGuard('jwt'))
export class AircraftController {
  constructor(private readonly _aircraftService: AircraftService) {}

  @Get()
  @ApiOperation({summary: 'Retourne tous les avions'})
  async findAll(): Promise<AircraftDto[]> {
    const aircrafts: Aircraft[] = await this._aircraftService.findAll();
    return plainToInstance(AircraftDto, camelcaseKeys(aircrafts, {deep: true}));
  }
}
