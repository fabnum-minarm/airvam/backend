import { Column, Entity, OneToMany, PrimaryColumn, } from "typeorm";
import { Flight } from "../../flight/entities/flight.entity";
import { SeatTemplateEntity } from "../../seat-template/entities/seat-template.entity";

@Entity('aircraft')
export class Aircraft {
  @PrimaryColumn()
  name: string;

  @Column({length: 20, nullable: true})
  short_name: string;

  @Column({length: 20, nullable: true})
  type: string;

  @Column({length: 50, nullable: true})
  registration: string;

  @Column({length: 50, nullable: true})
  plan_cab: string;

  @OneToMany(type => SeatTemplateEntity, seatTemplate => seatTemplate.aircraft)
  seats_template: SeatTemplateEntity[];

  @OneToMany(type => Flight, flight => flight.aircraft)
  flights: Flight[];
}
