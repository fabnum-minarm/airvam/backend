import { IsArray } from "class-validator";
import { SeatTemplateDto } from "../../seat-template/dto/seat-template.dto";
import { CreateAircraftDto } from "./create-aircraft.dto";

export class AircraftDto extends (CreateAircraftDto){

  @IsArray()
  seats_template: SeatTemplateDto[];
}
