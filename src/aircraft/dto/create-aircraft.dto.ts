import { IsString } from "class-validator";

export class CreateAircraftDto {

  @IsString()
  name: string;

  @IsString()
  shortName: string;
}
